import { Transform, TransformCallback } from 'stream'

import { v4 as uuid } from 'uuid'

import { FileUpload, GraphQLUpload, Upload as UploadType } from 'graphql-upload'

import traverse from '../../util/traverseMapArgs'
import env from '../../util/env'
import s3 from '../../s3'
import AppError, { AppErrorType } from '../../AppError'
import { builder } from '../../builder'

export interface Upload {
  upload(uploadFunction?: UploadFunction): Promise<string>
  file?: Promise<FileUpload>
}

export type UploadFunction = (file: FileUpload) => Promise<string>

const maxFileSize = Number(env.ASSET_MAX_SIZE_BYTES)

class SizeCounter extends Transform {
  length: number

  constructor() {
    super()
    this.length = 0
  }

  _transform(chunk: any, encoding: BufferEncoding, callback: TransformCallback) {
    this.length += chunk.length

    if (this.length > maxFileSize) {
      this.destroy(new AppError(AppErrorType.FILE_TOO_LARGE))
      return
    }

    this.push(chunk)
    callback()
  }
}

export class UploadResultImpl implements Upload {
  _fileOrUrl: string | Promise<any>

  constructor(fileOrUrl: string | Promise<any>) {
    this._fileOrUrl = fileOrUrl
  }

  get file() {
    if (typeof this._fileOrUrl === 'string') {
      return undefined
    }

    return GraphQLUpload.parseValue(this._fileOrUrl) as Promise<FileUpload>
  }

  async upload(uploadFunction: UploadFunction = defaultUploadFn) {
    if (typeof this._fileOrUrl === 'string') {
      return Promise.resolve(this._fileOrUrl)
    }

    const file = await GraphQLUpload.parseValue(this._fileOrUrl)

    return uploadFunction(file as FileUpload)
  }
}

async function defaultUploadFn(file: FileUpload) {
  const { filename, createReadStream, mimetype } = file

  const key = uuid()
  const extension = filename.split('.').reverse()[0]
  const s3fileName = `${key}.${extension}`
  const stream = createReadStream()

  const sizeValidator = new SizeCounter()
  sizeValidator.on('error', (error) => {
    stream.destroy(error)
  })

  stream.pipe(sizeValidator)

  try {
    const uploadResult = await s3
      .upload({
        Body: stream,
        Key: s3fileName,
        ACL: env.AWS_GLOBAL_ACL || 'public-read',
        ContentType: mimetype,
        Bucket: env.SPACES_BUCKET
      })
      .promise()

    let url = uploadResult.Location

    if (!url.startsWith('http')) {
      url = 'https://' + url
    }

    return url
  } catch (err) {
    console.error(err)

    throw err
  } finally {
    stream.destroy()
  }
}

function uploadProcessor(value: string | Promise<any>): Upload {
  return new UploadResultImpl(value)
}

export async function handleUploads(args: any): Promise<any> {
  return traverse(args, (leaf) => {
    if (leaf) {
      if (leaf instanceof UploadResultImpl) {
        return leaf.upload()
      }

      return leaf
    }
  })
}

builder.scalarType('Upload', {
  description: GraphQLUpload.description || '',
  serialize: (value) => {
    return value
  },
  // @ts-ignore
  parseValue: uploadProcessor
})
