import { getS3FileUrl } from '../../util/s3cdnUrl'

import { builder } from '../../builder'
import {
  RecordingPublishStatusFieldObject,
  RecordingSongFieldObject,
  RecordingTypeFieldObject,
  RecordingUserFieldObject
} from '../../generated/Recording'
import {
  ScoreObject,
  ScorePublishStatusFieldObject,
  ScoreSongFieldObject,
  ScoreUserFieldObject
} from '../../generated/Score'

builder.prismaObject('Score', {
  ...ScoreObject,
  fields: (t) => {
    const publishStatusField = ScorePublishStatusFieldObject
    const songField = ScoreSongFieldObject
    const userField = ScoreUserFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      url: t.string({
        resolve(parent) {
          return getS3FileUrl(parent.url)
        }
      }),
      description: t.expose('description', { type: 'String', nullable: true }),
      publishStatus: t.expose('publishStatus', publishStatusField),
      song: t.relation('song', songField),
      user: t.relation('user', userField)
    }
  }
})

builder.prismaObject('Recording', {
  ...ScoreObject,
  fields: (t) => {
    const publishStatusField = RecordingPublishStatusFieldObject
    const songField = RecordingSongFieldObject
    const userField = RecordingUserFieldObject
    const typeField = RecordingTypeFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      url: t.string({
        resolve(parent) {
          return getS3FileUrl(parent.url)
        }
      }),
      description: t.expose('description', { type: 'String', nullable: true }),
      publishStatus: t.expose('publishStatus', publishStatusField),
      type: t.expose('type', typeField),
      song: t.relation('song', songField),
      user: t.relation('user', userField)
    }
  }
})
