import { getS3FileUrl } from '../../util/s3cdnUrl'
import { builder } from '../../builder'
import { UserObject } from '../../generated/User'

builder.prismaObject('User', {
  ...UserObject,
  fields: (t) => {
    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      profileImage: t.field({
        type: 'String',
        nullable: true,
        resolve(parent) {
          return getS3FileUrl(parent.profileImage)
        }
      }),
      email: t.exposeString('email'),
      name: t.exposeString('name')
    }
  }
})
