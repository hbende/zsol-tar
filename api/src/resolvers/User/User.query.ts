import { hash } from 'bcrypt'

import { Prisma } from '@prisma/client'

import env from '../../util/env'
import { handleUploads, UploadResultImpl } from '../scalars/Upload.scalar'
import { builder } from '../../builder'
import {
  createOneUserMutationObject,
  findUniqueUserQueryObject,
  updateOneUserMutationObject
} from '../../generated/objects'
import AppError, { AppErrorType } from '../../AppError'
import { UserUpdateInputFields } from '../../generated/inputs'

export const UserUpdateInputWithFile = builder
  .inputRef<Prisma.UserUpdateInput & { profileImage: UploadResultImpl }>('UserUpdateInputWithFile')
  .implement({
    fields: (t) => ({
      ...UserUpdateInputFields(t),
      // @ts-expect-error
      profileImage: t.field({ type: 'Upload' })
    })
  })

builder.queryFields((t) => {
  return { user: t.prismaField(findUniqueUserQueryObject(t)) }
})

builder.mutationFields((t) => {
  const createOneUser = createOneUserMutationObject(t)
  const updateOneUser = updateOneUserMutationObject(t)

  return {
    createOneUser: t.prismaField({
      // Inherit all the generated properties
      ...createOneUser,
      async resolve(mutation, root, args, ctx, info) {
        args.data = {
          ...args.data,
          password: await hash(args.data.password, env.SALT_ROUNDS)
        }
        return createOneUser.resolve(mutation, root, args, ctx, info)
      }
    }),
    updateOneUser: t.prismaField({
      ...updateOneUser,
      args: {
        ...updateOneUser.args,
        data: t.arg({ type: UserUpdateInputWithFile, required: true })
      },
      async resolve(query, root, args, context, info) {
        if (context.user?.id !== args.where.id) {
          throw new AppError(AppErrorType.AUTH_INVALID)
        }

        if (args.data.password) {
          args.data = {
            ...args.data,
            // @ts-expect-error todo
            password: { set: await hash(args.data.password.set, env.SALT_ROUNDS) }
          }
        }

        const argsAfterUpload: typeof args = await handleUploads(args)

        // @ts-expect-error todo
        return updateOneUser.resolve(query, root, argsAfterUpload, context, info)
      }
    })
  }
})
