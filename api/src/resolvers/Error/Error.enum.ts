import { AppErrorType } from '../../AppError'
import { builder } from '../../builder'

builder.enumType(AppErrorType, { name: 'AppError' })
