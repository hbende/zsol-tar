import { builder } from '../../builder'

import {
  SongScoresFieldObject,
  SongCreatorFieldObject,
  SongRecordingsFieldObject,
  SongCommentsFieldObject,
  SongObject,
  SongLyricsFieldObject,
  SongPublishStatusFieldObject
} from '../../generated/Song'

import {
  LyricsPublishStatusFieldObject,
  LyricsObject,
  LyricsSongFieldObject,
  LyricsVersesFieldObject,
  LyricsCreatorFieldObject
} from '../../generated/Lyrics'

import { VerseLyricsFieldObject, VerseObject, VerseTypeFieldObject } from '../../generated/Verse'

import {
  SongCommentObject,
  SongCommentPublishStatusFieldObject,
  SongCommentSongFieldObject,
  SongCommentUserFieldObject
} from '../../generated/SongComment'

builder.prismaObject('Song', {
  ...SongObject,
  fields: (t) => {
    const creatorField = SongCreatorFieldObject
    const lyricsField = SongLyricsFieldObject(t)
    const publishStatusField = SongPublishStatusFieldObject
    const scoresField = SongScoresFieldObject(t)
    const recordingsField = SongRecordingsFieldObject(t)
    const commentsField = SongCommentsFieldObject(t)

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      name: t.exposeString('name'),
      writer: t.expose('writer', { type: 'String', nullable: true }),
      translator: t.expose('translator', { type: 'String', nullable: true }),
      publishStatus: t.expose('publishStatus', publishStatusField),
      userTags: t.prismaField({
        type: ['Tag'],
        resolve: async (query, root, _args, ctx) => {
          if (!ctx.user) {
            return []
          }
          const tags = await ctx.prisma.tag.findMany({
            where: {
              AND: [{ user: { id: ctx.user?.id } }, { songs: { some: { id: root.id } } }]
            }
          })

          return tags || []
        }
      }),
      creator: t.relation('creator', creatorField),
      lyrics: t.relation('lyrics', lyricsField),
      scores: t.relation('scores', scoresField),
      recordings: t.relation('recordings', recordingsField),
      comments: t.relation('comments', commentsField)
    }
  }
})

builder.prismaObject('Lyrics', {
  ...LyricsObject,
  fields: (t) => {
    const songField = LyricsSongFieldObject
    const versesField = LyricsVersesFieldObject(t)
    const creatorField = LyricsCreatorFieldObject
    const publishStatusField = LyricsPublishStatusFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      publishStatus: t.expose('publishStatus', publishStatusField),
      song: t.relation('song', songField),
      verses: t.relation('verses', versesField),
      creator: t.relation('creator', creatorField)
    }
  }
})

builder.prismaObject('Verse', {
  ...VerseObject,
  fields: (t) => {
    const lyricsField = VerseLyricsFieldObject
    const typeField = VerseTypeFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      text: t.exposeString('text'),
      order: t.expose('order', { type: 'Int', nullable: true }),
      type: t.expose('type', typeField),
      lyrics: t.relation('lyrics', lyricsField)
    }
  }
})

builder.prismaObject('SongComment', {
  ...SongCommentObject,
  fields: (t) => {
    const publishStatusField = SongCommentPublishStatusFieldObject
    const songField = SongCommentSongFieldObject
    const userField = SongCommentUserFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      text: t.exposeString('text'),
      publishStatus: t.expose('publishStatus', publishStatusField),
      song: t.relation('song', songField),
      user: t.relation('user', userField)
    }
  }
})
