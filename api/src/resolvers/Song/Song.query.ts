import { Prisma } from '@prisma/client'

import * as R from 'remeda'

import AppError, { AppErrorType } from '../../AppError'

import { builder } from '../../builder'
import {
  countLyricsQueryObject,
  createOneLyricsMutationObject,
  findManyLyricsQueryObject,
  findUniqueLyricsQueryObject
} from '../../generated/Lyrics'
import {
  createOneSongMutationObject,
  findManySongQueryObject,
  findUniqueSongQueryObject,
  countSongQueryObject
} from '../../generated/Song'
import {
  countSongCommentQueryObject,
  createOneSongCommentMutationObject,
  findManySongCommentQueryObject
} from '../../generated/SongComment'

import canonizeString from '../../util/canonizeString'
import { authorizeAdmin } from '../Publish/Publish.query'

export function canonizeStringFilters(filters: string | Prisma.StringFilter | undefined) {
  if (R.isObject(filters)) {
    // @ts-ignore
    filters = R.mapValues(filters, (value) => {
      if (value) {
        return R.isArray(value)
          ? R.map(value, (v) => canonizeString(v))
          : canonizeString(value as string)
      }
      return undefined
    })
  }

  return filters
}

// ==================================================== Song ====================================================

// Use the QueryObject exports to override or add to the generated code
builder.queryFields((t) => {
  const findUniqueSong = findUniqueSongQueryObject(t)
  const songs = findManySongQueryObject(t)
  const songsCount = countSongQueryObject(t)

  return {
    song: t.prismaField({
      // Inherit all the generated properties
      ...findUniqueSong,
      // Modify the resolver
      resolve: async (query, root, args, ctx) => {
        const song = await ctx.prisma.song.findFirst({
          ...args,
          include: {
            lyrics: {
              where: {
                OR: [
                  { publishStatus: { equals: 'Published' } },
                  { creatorId: { equals: ctx.user?.id } }
                ]
              }
            },
            scores: {
              where: {
                OR: [
                  { publishStatus: { equals: 'Published' } },
                  { userId: { equals: ctx.user?.id } }
                ]
              }
            },
            recordings: {
              where: {
                OR: [
                  { publishStatus: { equals: 'Published' } },
                  { userId: { equals: ctx.user?.id } }
                ]
              }
            },
            comments: {
              where: {
                OR: [
                  { publishStatus: { equals: 'Published' } },
                  { userId: { equals: ctx.user?.id } }
                ]
              }
            }
          }
        })

        return song
      }
    }),

    publishedSongs: t.prismaField({
      ...songs,
      resolve: async (query, root, args, context, info) => {
        if (args.where?.name) {
          args.where.name = canonizeStringFilters(args.where?.name)
          args.where.canonicalName = args.where.name
          args.where.name = undefined
        }

        // todo filter OR from inputs..
        if (!context.user || !context.user.isAdmin) {
          args.where = args.where || {}
          args.where.OR = [{ publishStatus: 'Published' }, { creator: { id: context.user?.id } }]
        }

        return songs.resolve(query, root, args, context, info)
      }
    }),
    publishedSongsCount: t.field({
      args: songsCount.args,
      type: 'Int',
      resolve: async (root, args, context, info) => {
        if (args.where?.name) {
          args.where.name = canonizeStringFilters(args.where.name)
          args.where.canonicalName = args.where.name
          args.where.name = undefined
        }

        // todo filter OR from inputs..
        if (!context.user || !context.user.isAdmin) {
          args.where = args.where || {}
          args.where.OR = [{ publishStatus: 'Published' }, { creator: { id: context.user?.id } }]
        }

        return songsCount.resolve(root, args, context, info)
      }
    })
  }
})

// Use the QueryObject exports to override or add to the generated code
builder.mutationFields((t) => {
  const createOneSong = createOneSongMutationObject(t)
  return {
    createOneSong: t.prismaField({
      // Inherit all the generated properties
      ...createOneSong,
      resolve(mutation, root, args, ctx, info) {
        args.data = {
          ...args.data,
          canonicalName: canonizeString(args.data.name)!,
          creator: { connect: { id: ctx.user?.id || undefined } }
        }
        return createOneSong.resolve(mutation, root, args, ctx, info)
      }
    }),
    addTagToSong: t.prismaField({
      type: 'Song',
      nullable: true,
      args: {
        tagId: t.arg({ type: 'String' }),
        songId: t.arg({ type: 'String', required: true }),
        text: t.arg({ type: 'String', required: true })
      },
      async resolve(query, root, { songId, text, tagId }, ctx, info) {
        if (!ctx.user?.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        if (!tagId) {
          const existingUserTag = await ctx.prisma.tag.findFirst({
            where: { text, userId: ctx.user?.id }
          })

          if (existingUserTag) {
            await ctx.prisma.tag.update({
              where: { id: existingUserTag.id },
              data: {
                songs: { connect: { id: songId } }
              }
            })
          } else {
            await ctx.prisma.tag.create({
              data: {
                user: { connect: { id: ctx.user?.id } },
                songs: { connect: { id: songId } },
                text,
                canonicalText: canonizeString(text) as string
              }
            })
          }
        } else {
          await ctx.prisma.tag.update({
            where: { id: tagId },
            data: {
              songs: { connect: { id: songId } }
            }
          })
        }

        return ctx.prisma.song.findFirst({ ...query, where: { id: songId } })
      }
    }),
    deleteTagFromSong: t.prismaField({
      type: 'Song',
      args: {
        tagId: t.arg({ type: 'String', required: true }),
        songId: t.arg({ type: 'String', required: true })
      },
      async resolve(query, root, { songId, tagId }, ctx, info) {
        if (!ctx.user?.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }
        const tag = await ctx.prisma.tag.findFirst({ where: { id: tagId } })
        if (tag?.userId !== ctx.user.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const updatedSong = await ctx.prisma.song.update({
          where: { id: songId },
          data: { labels: { disconnect: { id: tagId } } },
          include: { labels: true }
        })

        const numberOfRemainingTags = await ctx.prisma.tag.count({
          where: { id: { equals: tagId }, songs: { some: { id: { contains: '' } } } }
        })

        if (numberOfRemainingTags === 0) {
          await ctx.prisma.tag.delete({ where: { id: tagId } })
        }

        return updatedSong
      }
    })
  }
})

// ==================================================== Lyrics ====================================================

builder.mutationFields((t) => {
  const createOneLyrics = createOneLyricsMutationObject(t)
  return {
    createOneLyrics: t.prismaField({
      // Inherit all the generated properties
      ...createOneLyrics,
      resolve(mutation, root, args, ctx, info) {
        args.data = {
          ...args.data,
          creator: {
            connect: { id: ctx.user?.id }
          }
        }
        return createOneLyrics.resolve(mutation, root, args, ctx, info)
      }
    })
  }
})

builder.queryFields((t) => {
  const findManyLyrics = findManyLyricsQueryObject(t)
  const lyricsCount = countLyricsQueryObject(t)
  const findUniqueLyrics = findUniqueLyricsQueryObject(t)

  return {
    searchLyrics: t.prismaField({
      ...findManyLyrics,
      async resolve(query, root, args, ctx, info) {
        await authorizeAdmin(ctx)

        return findManyLyrics.resolve(query, root, args, ctx, info)
      }
    }),
    songlyricsCount: t.field({
      args: lyricsCount.args,
      type: 'Int',
      resolve: async (root, args, context, info) => {
        return lyricsCount.resolve(
          root,
          { ...args, where: { ...args.where, creatorId: context.user?.id } },
          context,
          info
        )
      }
    }),
    lyrics: t.prismaField(findUniqueLyrics)
  }
})

// ==================================================== Comments ====================================================

builder.mutationFields((t) => {
  const createOneSongComment = createOneSongCommentMutationObject(t)
  return {
    createOneSongComment: t.prismaField({
      ...createOneSongComment,
      resolve(mutation, root, args, ctx, info) {
        args.data = {
          ...args.data,
          publishStatus: 'Unpublished',
          user: { connect: { id: ctx.user?.id } }
        }

        return createOneSongComment.resolve(mutation, root, args, ctx, info)
      }
    })
  }
})

builder.queryFields((t) => {
  const findManyLyrics = findManySongCommentQueryObject(t)
  const commentsCount = countSongCommentQueryObject(t)

  return {
    searchComments: t.prismaField({
      ...findManyLyrics,
      async resolve(query, root, args, ctx, info) {
        await authorizeAdmin(ctx)

        return findManyLyrics.resolve(query, root, args, ctx, info)
      }
    }),
    commentsCount: t.field({
      args: commentsCount.args,
      type: 'Int',
      resolve: async (root, args, context, info) => {
        await authorizeAdmin(context)

        return commentsCount.resolve(root, args, context, info)
      }
    })
  }
})
