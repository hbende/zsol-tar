import { countTagQueryObject, findManyTagQueryObject } from '../../generated/Tag'
import { builder } from '../../builder'
import { canonizeStringFilters } from '../Song/Song.query'

builder.queryFields((t) => {
  const tags = findManyTagQueryObject(t)
  const tagCounts = countTagQueryObject(t)

  return {
    userTags: t.prismaField({
      ...tags,
      resolve: async (query, root, args, ctx, info) => {
        args.where = args.where || {}
        const textFilter = canonizeStringFilters(args.where.text)
        args.where.text = undefined

        args.where = {
          ...args.where,
          ...(textFilter ? { canonicalText: textFilter } : {}),
          userId: ctx.user?.id
        }
        return tags.resolve(query, root, args, ctx, info)
      }
    }),
    userTagsCount: t.field({
      ...tagCounts,
      type: 'Int',
      resolve: async (root, args, context, info) => {
        return tagCounts.resolve(
          root,
          { ...args, where: { ...args.where, userId: context.user?.id } },
          context,
          info
        )
      }
    })
  }
})
