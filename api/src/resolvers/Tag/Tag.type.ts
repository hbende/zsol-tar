import { builder } from '../../builder'
import { TagObject, TagSongsFieldObject, TagUserFieldObject } from '../../generated/Tag'

builder.prismaObject('Tag', {
  ...TagObject,
  fields: (t) => {
    const songsField = TagSongsFieldObject(t)
    const userField = TagUserFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      text: t.exposeString('text'),
      scores: t.relation('songs', songsField),
      user: t.relation('user', userField)
    }
  }
})
