// eslint-disable @typescript-eslint/ban-ts-comment

import AppError, { AppErrorType } from '../../AppError'
import { builder } from '../../builder'
import {
  createOneSongOrderMutationObject,
  updateOneSongOrderMutationObject,
  findUniqueSongOrderQuery,
  findManySongOrderQueryObject,
  countSongOrderQueryObject,
  findUniqueSongOrderQueryObject
} from '../../generated/SongOrder'
import {
  countSongOrderTemplateQueryObject,
  createOneSongOrderTemplateMutationObject,
  findManySongOrderTemplateQueryObject,
  findUniqueSongOrderTemplateQuery,
  findUniqueSongOrderTemplateQueryObject
} from '../../generated/SongOrderTemplate'

// ==================================================== SONG ORDER ====================================================

builder.queryFields(findUniqueSongOrderQuery)
builder.queryFields((t) => {
  const findManySongOrder = findManySongOrderQueryObject(t)
  const songOrderCount = countSongOrderQueryObject(t)
  const findUniqueSongOrder = findUniqueSongOrderQueryObject(t)

  return {
    userSongOrders: t.prismaField({
      ...findManySongOrder,
      async resolve(query, root, args, ctx, info) {
        return findManySongOrder.resolve(
          query,
          root,
          { ...args, where: { ...args.where, creatorId: ctx.user?.id } },
          ctx,
          info
        )
      }
    }),
    userSongOrdersCount: t.field({
      args: songOrderCount.args,
      type: 'Int',
      resolve: async (root, args, context, info) => {
        return songOrderCount.resolve(
          root,
          { ...args, where: { ...args.where, creatorId: context.user?.id } },
          context,
          info
        )
      }
    }),
    songOrder: t.prismaField(findUniqueSongOrder)
  }
})

builder.mutationFields((t) => {
  const createOneSongOrder = createOneSongOrderMutationObject(t)
  const updateOneSongOrder = updateOneSongOrderMutationObject(t)

  return {
    createOneSongOrder: t.prismaField({
      // Inherit all the generated properties
      ...createOneSongOrder,
      resolve(mutation, root, args, ctx, info) {
        if (!ctx.user?.id) {
          throw new AppError(AppErrorType.AUTH_INVALID)
        }

        args.data = {
          ...args.data,
          creator: { connect: { id: ctx.user?.id || undefined } }
        }
        return createOneSongOrder.resolve(mutation, root, args, ctx, info)
      }
    }),
    updateOneSongOrder: t.prismaField({
      // Inherit all the generated properties
      ...updateOneSongOrder,
      async resolve(mutation, root, args, ctx, info) {
        const songOrder = await ctx.prisma.songOrder.findFirst({
          where: { id: args.where.id || undefined }
        })

        if (!ctx.user?.id || songOrder?.creatorId !== ctx.user?.id) {
          throw new AppError(AppErrorType.AUTH_INVALID)
        }

        args.data = {
          ...args.data,
          creator: { connect: { id: ctx.user?.id || undefined } }
        }
        return updateOneSongOrder.resolve(mutation, root, args, ctx, info)
      }
    })
  }
})

// ==================================================== SONG ORDER TEMPLATE ====================================================

builder.queryFields(findUniqueSongOrderTemplateQuery)
builder.queryFields((t) => {
  const findManySongOrderTemplate = findManySongOrderTemplateQueryObject(t)
  const songOrderTemplateCount = countSongOrderTemplateQueryObject(t)
  const findUniqueSongOrderTemplate = findUniqueSongOrderTemplateQueryObject(t)

  return {
    userTemplates: t.prismaField({
      ...findManySongOrderTemplate,
      async resolve(query, root, args, ctx, info) {
        return findManySongOrderTemplate.resolve(
          query,
          root,
          {
            ...args,
            where: {
              ...args.where,
              OR: [{ publishStatus: 'Published' }, { creatorId: ctx.user?.id }]
            }
          },
          ctx,
          info
        )
      }
    }),
    userTemplatesCount: t.field({
      args: songOrderTemplateCount.args,
      type: 'Int',
      resolve: async (root, args, context, info) => {
        return songOrderTemplateCount.resolve(
          root,
          { ...args, where: { ...args.where, creatorId: context.user?.id } },
          context,
          info
        )
      }
    }),
    songOrderTemplate: t.prismaField(findUniqueSongOrderTemplate)
  }
})

builder.mutationFields((t) => {
  const createOneSongOrderTemplate = createOneSongOrderTemplateMutationObject(t)

  return {
    createOneSongOrderTemplate: t.prismaField({
      ...createOneSongOrderTemplate,
      resolve: (mutation, root, args, ctx, info) => {
        if (!ctx.user?.id) {
          throw new AppError(AppErrorType.AUTH_INVALID)
        }

        args.data = {
          ...args.data,
          creator: { connect: { id: ctx.user?.id || undefined } }
        }
        return createOneSongOrderTemplate.resolve(mutation, root, args, ctx, info)
      }
    })
  }
})
