import { builder } from '../../builder'
import {
  SongOrderObject,
  SongOrderPresentationConfigFieldObject,
  SongOrderGroupsFieldObject
} from '../../generated/SongOrder'
import {
  SongOrderTemplateCreatorFieldObject,
  SongOrderTemplateItemsFieldObject,
  SongOrderTemplateObject,
  SongOrderTemplatePublishStatusFieldObject
} from '../../generated/SongOrderTemplate'
import {
  SongOrderTemplateItemObject,
  SongOrderTemplateItemTemplateFieldObject
} from '../../generated/SongOrderTemplateItem'

builder.prismaObject('SongOrder', {
  ...SongOrderObject,
  fields: (t) => {
    const presentationConfigField = SongOrderPresentationConfigFieldObject
    const groupsField = SongOrderGroupsFieldObject(t)

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      title: t.expose('title', { type: 'String', nullable: true }),
      presentationConfig: t.expose('presentationConfig', presentationConfigField),
      note: t.expose('note', { type: 'String', nullable: true }),
      groups: t.relation('groups', groupsField)
    }
  }
})

builder.prismaObject('SongOrderTemplate', {
  ...SongOrderTemplateObject,
  fields: (t) => {
    const publishStatusField = SongOrderTemplatePublishStatusFieldObject
    const creatorField = SongOrderTemplateCreatorFieldObject
    const itemsField = SongOrderTemplateItemsFieldObject(t)

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      title: t.exposeString('title'),
      publishStatus: t.expose('publishStatus', publishStatusField),
      creator: t.relation('creator', creatorField),
      items: t.relation('items', itemsField)
    }
  }
})

builder.prismaObject('SongOrderTemplateItem', {
  ...SongOrderTemplateItemObject,
  fields: (t) => {
    const templateField = SongOrderTemplateItemTemplateFieldObject
    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      title: t.expose('title', { type: 'String', nullable: true }),
      template: t.relation('template', templateField),
      order: t.expose('order', { type: 'Int' })
    }
  }
})
