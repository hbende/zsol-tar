import { builder } from '../../builder'
import { SlideGroupFieldObject, SlideObject } from '../../generated/Slide'
import {
  SlideGroupObject,
  SlideGroupSlidesFieldObject,
  SlideGroupSongFieldObject,
  SlideGroupSongOrderFieldObject
} from '../../generated/SlideGroup'

builder.prismaObject('SlideGroup', {
  ...SlideGroupObject,
  fields: (t) => {
    const slidesField = SlideGroupSlidesFieldObject(t)
    const songField = SlideGroupSongFieldObject
    const songOrderField = SlideGroupSongOrderFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      order: t.exposeInt('order'),
      title: t.expose('title', { type: 'String', nullable: true }),
      isSongOrderItem: t.expose('isSongOrderItem', { type: 'Boolean', nullable: true }),
      slides: t.relation('slides', slidesField),
      song: t.relation('song', songField),
      songOrder: t.relation('songOrder', songOrderField)
    }
  }
})

builder.prismaObject('Slide', {
  ...SlideObject,
  fields: (t) => {
    const groupField = SlideGroupFieldObject

    return {
      id: t.exposeID('id'),
      createdAt: t.expose('createdAt', { type: 'DateTime' }),
      updatedAt: t.expose('updatedAt', { type: 'DateTime' }),
      order: t.exposeInt('order'),
      title: t.expose('title', { type: 'String', nullable: true }),
      content: t.exposeString('content'),
      group: t.relation('group', groupField)
    }
  }
})
