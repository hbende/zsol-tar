import AppError, { AppErrorType } from '../../AppError'

import { builder } from '../../builder'
import { SlideGroupCreateInput, SongOrderWhereUniqueInput } from '../../generated/inputs'
import {
  createOneSlideGroupMutationObject,
  deleteOneSlideGroupMutationObject,
  updateOneSlideGroupMutationObject
} from '../../generated/SlideGroup'

builder.mutationFields((t) => {
  const createOneSlideGroup = createOneSlideGroupMutationObject(t)
  const deleteOneSlideGroup = deleteOneSlideGroupMutationObject(t)
  const updateOneSlideGroup = updateOneSlideGroupMutationObject(t)

  return {
    createOneSlideGroup: t.prismaField({
      ...createOneSlideGroup,
      async resolve(query, root, args, context, info) {
        const songOrder = await context.prisma.songOrder.findFirst({
          where: { id: args.data.songOrder.connect?.id || undefined }
        })

        if (songOrder?.creatorId !== context.user?.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        return createOneSlideGroup.resolve(query, root, args, context, info)
      }
    }),
    deleteOneSlideGroup: t.prismaField({
      ...deleteOneSlideGroup,
      async resolve(query, root, args, context, info) {
        const songOrder = (
          await context.prisma.slideGroup.findFirst({
            where: { id: args.where.id || undefined },
            include: { songOrder: true }
          })
        )?.songOrder

        if (songOrder?.creatorId !== context.user?.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const slideGroup = await context.prisma.slideGroup.findFirst({
          where: { id: args.where.id || undefined },
          include: { songOrder: { include: { groups: true } } }
        })

        const updateSongOrder = slideGroup?.songOrder
        if (updateSongOrder) {
          const slideGroupsToDecrement = updateSongOrder.groups
            .filter((otherGroup) => otherGroup.order >= slideGroup.order)
            .map((slideGroup) => slideGroup.id)

          if (slideGroupsToDecrement?.length) {
            await context.prisma.slideGroup.updateMany({
              where: { id: { in: slideGroupsToDecrement } },
              data: { order: { decrement: 1 } }
            })
          }
        }

        return deleteOneSlideGroup.resolve(query, root, args, context, info)
      }
    }),
    updateOneSlideGroup: t.prismaField({
      ...updateOneSlideGroup,
      async resolve(query, root, args, context, info) {
        const songOrder = (
          await context.prisma.slideGroup.findFirst({
            where: { id: args.where.id || undefined },
            include: { songOrder: true }
          })
        )?.songOrder

        if (songOrder?.creatorId !== context.user?.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }
        return updateOneSlideGroup.resolve(query, root, args, context, info)
      }
    })
  }
})

// ==================================================== Custom actions ====================================================

builder.mutationFields((t) => {
  return {
    insertSlideGroup: t.prismaField({
      type: 'SlideGroup',
      args: {
        data: t.arg({ type: SlideGroupCreateInput, required: true }),
        whereSongOrder: t.arg({ type: SongOrderWhereUniqueInput, required: true })
      },
      async resolve(query, root, { data, whereSongOrder }, context, _info) {
        if (!context.user) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const songOrder = await context.prisma.songOrder.findFirst({
          where: { id: whereSongOrder?.id || undefined },
          include: { groups: true }
        })

        if (songOrder?.creatorId !== context.user.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const slideGroupsToIncrement = songOrder?.groups
          .filter((slideGroup) => slideGroup.order >= (data?.order || 0))
          .map((slideGroup) => slideGroup.id)

        if (slideGroupsToIncrement?.length) {
          await context.prisma.slideGroup.updateMany({
            where: { id: { in: slideGroupsToIncrement } },
            data: { order: { increment: 1 } }
          })
        }

        return context.prisma.slideGroup.create({ ...query, data })
      }
    }),
    swapSlideGroups: t.prismaField({
      type: ['SlideGroup'],
      args: {
        groupId1: t.arg({ type: 'String', required: true }),
        groupId2: t.arg({ type: 'String', required: true })
      },
      async resolve(query, root, { groupId1, groupId2 }, ctx, _info) {
        if (!ctx.user) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const songOrder = await ctx.prisma.slideGroup
          .findFirst({
            where: { id: groupId1 || undefined },
            select: { songOrder: true }
          })
          .songOrder()

        if (songOrder?.creatorId !== ctx.user.id) {
          throw new AppError(AppErrorType.AUTH_NO_PERMISSION)
        }

        const firstSideGroup = await ctx.prisma.slideGroup.findFirst({
          where: { id: groupId1 || undefined },
          select: { order: true }
        })
        const secondSlideGroup = await ctx.prisma.slideGroup.findFirst({
          where: { id: groupId2 || undefined },
          select: { order: true }
        })

        const slideGroup1 = await ctx.prisma.slideGroup.update({
          where: { id: groupId1 },
          data: { order: secondSlideGroup?.order },
          ...query
        })

        const slideGroup2 = await ctx.prisma.slideGroup.update({
          where: { id: groupId2 },
          data: { order: firstSideGroup?.order },
          ...query
        })

        return [slideGroup1, slideGroup2]
      }
    })
  }
})
