import express from 'express'
import { applyMiddleware } from 'graphql-middleware'
import { graphqlUploadExpress } from 'graphql-upload'

import { ApolloServer } from '@apollo/server'
import { ApolloServerErrorCode } from '@apollo/server/errors'
import { expressMiddleware } from '@apollo/server/express4'
import bodyParser from 'body-parser'
import cors from 'cors'

import sentryPlugin from './sentryPlugin'

import env from './util/env'
import shield from './shield'
import { schema } from './schema'
import { createContext } from './context'

async function startApolloServer() {
  const app = express()
  const server = new ApolloServer({
    schema: applyMiddleware(schema, shield),
    introspection: true,
    plugins: [
      ...(env.SENTRY_DSN
        ? [
            sentryPlugin({
              dsn: env.SENTRY_DSN,

              release: env.SENTRY_APP + '@' + env.SENTRY_RELEASE,

              environment: env.NODE_ENV || 'production',

              sampleRate: 1
            })
          ]
        : [])
    ],
    formatError(err) {
      // The handled errors should be sent to the client as is

      if (Object.values(ApolloServerErrorCode).includes(err.extensions?.code as any)) {
        return err
      }

      console.error(err)

      // Otherwise we must hide the informations

      return new Error('Internal server error')
    }
  })

  await server.start()

  app.use(
    '/gql',
    cors<cors.CorsRequest>({
      origin: ['https://akkordo.hu', 'https://www.akkordo.hu']
    }),
    graphqlUploadExpress(),
    bodyParser.json(),
    expressMiddleware(server, { context: createContext })
  )

  const host = env.HOST
  const port = env.PORT

  await new Promise<void>((resolve) => app.listen({ port, host }, resolve))

  console.log(`Server ready at ${host}:${port}`)
}

startApolloServer()
