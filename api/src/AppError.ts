import { ApolloServerErrorCode } from '@apollo/server/errors'
import { GraphQLError } from 'graphql'

export enum AppErrorType {
  AUTH_INVALID = 'AUTH_INVALID',
  AUTH_LOCKED = 'AUTH_LOCKED',
  AUTH_NO_PERMISSION = 'AUTH_NO_PERMISSION',
  NO_SUCH_RECORD = 'NO_SUCH_RECORD',
  SERVER_ERROR = 'SERVER_ERRROR',
  FILE_TOO_LARGE = 'FILE_TOO_LARGE'
}

export default class AppError extends GraphQLError {
  constructor(type: ApolloServerErrorCode | AppErrorType) {
    super(type.toString())

    Object.defineProperty(this, 'name', { value: 'AppError' })
  }
}
