import { Scalars } from 'prisma-generator-pothos-codegen'
import SchemaBuilder from '@pothos/core'

import PrismaPlugin from '@pothos/plugin-prisma'
import type PrismaTypes from '@pothos/plugin-prisma/generated'

import { Upload } from 'graphql-upload'

import { Prisma, User } from '@prisma/client'

import { Context, prisma } from './context'

// todo move to extra types
type AuthPayload = {
  token: string
  user?: User
}

export const builder = new SchemaBuilder<{
  Scalars: Scalars<Prisma.Decimal, Prisma.InputJsonValue | null, Prisma.InputJsonValue> & {
    Upload: { Input: Upload; Output: Upload }
  }
  PrismaTypes: PrismaTypes
  Objects: { AuthPayload: AuthPayload }
  Context: Context
}>({
  plugins: [PrismaPlugin],
  prisma: {
    client: prisma
  }
})
