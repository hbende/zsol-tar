import AWS from 'aws-sdk'

import env from './util/env'

export const isStorjStorage = env.SPACES_ENDPOINT.includes('gateway.storjshare.io')

const spacesEndpoint = isStorjStorage ? env.SPACES_ENDPOINT : new AWS.Endpoint(env.SPACES_ENDPOINT)

const options: AWS.S3.ClientConfiguration = {
  params: { Bucket: env.SPACES_BUCKET },
  accessKeyId: env.SPACES_KEY,
  secretAccessKey: env.SPACES_SECRET,
  endpoint: spacesEndpoint,
  region: env.SPACES_REGION,
  logger: env.APP_DEBUG ? console : undefined
}

const s3 = new AWS.S3(options)

export default s3
