import { isStorjStorage } from '../s3'

import env from './env'

export function getS3FileUrl(url: string): string
export function getS3FileUrl(url: string | null): string | null
export function getS3FileUrl(url: string | null) {
  if (!isStorjStorage || !env.SPACES_CDN) {
    return url
  }

  if (!url) {
    return null
  }

  const replacedUrl = url?.replace(
    `${env.SPACES_BUCKET}.${env.SPACES_ENDPOINT.replace('https://', '')}`,
    `${env.SPACES_CDN}`
  )

  return replacedUrl
}
