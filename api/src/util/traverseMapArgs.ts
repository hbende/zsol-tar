export default async function traverse(
  args: any,
  mapFn: (p: any) => any,
  path = 'root'
): Promise<any> {
  if (args.constructor === Array) {
    return Promise.all(args.map((item, index) => traverse(item, mapFn, `${path}[${index}]`)))
  }

  if (args.constructor === Object) {
    return Object.entries(args).reduce(async (acc, [key, value]) => {
      const prev = await acc

      return {
        ...prev,
        [key]: await traverse(value, mapFn, `${path}.${key}`)
      }
    }, Promise.resolve({}))
  }

  return await mapFn(await args)
}
