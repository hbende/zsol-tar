import { lstatSync, readdirSync } from 'fs'
import { join } from 'path'

const isDirectory = (source: string) => lstatSync(source).isDirectory()

const requireAll = (source: string): any[] =>
  readdirSync(source)
    .map((name) => join(source, name))
    .filter(isDirectory)
    .reduce((acc, curr) => {
      const modules = readdirSync(curr)
        .filter((name) => name.match(/.*(j|t)s$/))
        .map((name) => join(curr, name))
        .map((file) => {
          if (isDirectory(file)) {
            return [...acc, ...requireAll(file)]
          }
          return require(file)
        })
      return [...acc, ...modules]
    }, [] as any[])

export default requireAll
