import path from 'path'

import * as dotenv from 'dotenv'
import { bool, email, envsafe, num, str } from 'envsafe'

const PROJECT_ROOT = process.cwd()

dotenv.config({
  path: path.join(PROJECT_ROOT, '.env')
})

/** on coolify dashboard saving an empty env variable is not supported */
function deleteCoolifyEmptyEnvVars(env: NodeJS.ProcessEnv) {
  const cleanedUpEnv: NodeJS.ProcessEnv = {}

  for (const key in env) {
    const element = env[key]
    if (element === '""') {
      cleanedUpEnv[key] = ''
      continue
    }
    cleanedUpEnv[key] = element
  }

  return cleanedUpEnv
}

const env = envsafe(
  {
    SALT_ROUNDS: num(),
    APP_SECRET: str(),
    HOST: str({ allowEmpty: true, devDefault: 'localhost' }),
    PORT: num({ allowEmpty: true, devDefault: 4000 }),
    AWS_GLOBAL_ACL: str({ allowEmpty: true, default: 'public-read' }),
    SPACES_ENDPOINT: str(),
    SPACES_KEY: str(),
    SPACES_SECRET: str(),
    SPACES_BUCKET: str(),
    SPACES_REGION: str({ allowEmpty: true }),
    SPACES_CDN: str({ allowEmpty: true }),
    ASSET_MAX_SIZE_BYTES: num(),
    ADMIN_USER_EMAIL: email(),
    ADMIN_USER_PASSWORD: str(),
    GRAPHQL_TRACING: bool({ allowEmpty: true, devDefault: false }),
    SENTRY_DSN: str({ allowEmpty: true }),
    SENTRY_APP: str({ allowEmpty: true }),
    SENTRY_RELEASE: str({ allowEmpty: true }),
    NODE_ENV: str({
      devDefault: 'development',
      choices: ['development', 'production']
    }),
    APP_DEBUG: bool({ allowEmpty: true, devDefault: true })
  },
  {
    env: deleteCoolifyEmptyEnvVars(process.env)
  }
)

if (env.APP_DEBUG) {
  console.log('API ENV', env)
}

export default env
