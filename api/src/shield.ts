import { rule, shield, not, allow } from 'graphql-shield'

import AppError, { AppErrorType } from './AppError'

// Rules

const isAuthenticated = rule({ cache: 'contextual' })(async (parent, args, ctx, info) => {
  return !!ctx.user
})

// Permissions

export default shield(
  {
    Query: {
      '*': allow
    },
    Mutation: {
      login: not(isAuthenticated),
      signup: not(isAuthenticated),
      '*': isAuthenticated
    }
  },
  {
    allowExternalErrors: true,
    fallbackError: new AppError(AppErrorType.AUTH_NO_PERMISSION)
  }
)
