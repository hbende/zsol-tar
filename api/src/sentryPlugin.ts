import { Transaction } from '@sentry/types/esm/transaction.d'

import * as Sentry from '@sentry/node'
import * as Integrations from '@sentry/integrations'
// import * as Tracing from '@sentry/tracing'

import { ApolloServerErrorCode } from '@apollo/server/errors'

import { ApolloServerPlugin, GraphQLRequestContext } from '@apollo/server'

import { Span } from '@sentry/tracing'

import { Integration } from './../node_modules/@sentry/types/esm/integration.d'

import { Context } from './context'

type Options = Sentry.NodeOptions

// This allows TypeScript to detect our global value
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace NodeJS {
    interface Global {
      __rootdir__: string
    }
  }
}

global.__rootdir__ = __dirname || process.cwd()

const sentryPlugin: (opts?: Options) => ApolloServerPlugin<Context> = (opts) => {
  if (opts) {
    Sentry.init({
      ...opts,
      integrations: [
        ...((opts.integrations || []) as Integration[]),
        new Integrations.RewriteFrames({
          root: global.__rootdir__
        })
      ]
    })
  }

  return {
    async requestDidStart() {
      let transaction: Transaction | null = null

      return {
        async didResolveOperation(ctx: GraphQLRequestContext<Context>) {
          transaction = Sentry.startTransaction({
            op: ctx.operation?.operation,
            name: ctx.operationName ?? 'unnamed-operation'
          })
        },
        async didEncounterErrors(ctx: GraphQLRequestContext<Context>) {
          // If we couldn't parse the operation, don't
          // do anything here
          if (!ctx.operation) {
            return
          }

          for (const err of ctx.errors || []) {
            // Only report internal server errors,
            // all errors extending ApolloError should be user-facing
            if (Object.values(ApolloServerErrorCode).includes(err.extensions.code as any)) {
              continue
            }

            // Add scoped report details and send to Sentry
            Sentry.withScope((scope) => {
              scope.setSpan(transaction as Span)

              const apolloContext = ctx.contextValue

              if (apolloContext.user) {
                scope.setUser(apolloContext.user)
              }

              // Annotate whether failing operation was query/mutation/subscription
              if (ctx.operation) {
                scope.setTag('kind', ctx.operation.operation)
              }

              // Log query and variables as extras (make sure to strip out sensitive data!)
              scope.setExtra('query', ctx.request.query)
              scope.setExtra('variables', ctx.request.variables)

              if (err.path) {
                // We can also add the path as breadcrumb
                scope.addBreadcrumb({
                  category: 'query-path',
                  message: err.path.join(' > '),
                  level: Sentry.Severity.Debug
                })
              }

              Sentry.captureException(err)
            })
          }
        },
        async willSendResponse() {
          transaction?.finish()
        }
      }
    }
  }
}

export default sentryPlugin
