import { builder } from './builder'
import './resolvers/User/User.auth'
import './resolvers/User/User.type'
import './resolvers/User/User.query'
import './resolvers/Tag/Tag.type'
import './resolvers/Tag/Tag.query'
import './resolvers/Song/Song.type'
import './resolvers/Song/Song.query'
import './resolvers/Publish/Publish.query'
import './resolvers/Assets/Assets.type'
import './resolvers/Assets/Asset.query'
import './resolvers/Presentation/Presentation.type'
import './resolvers/Presentation/Presentation.query'
import './resolvers/SongOrder/SongOrder.type'
import './resolvers/SongOrder/SongOrder.query'
import './resolvers/Error/Error.enum'

builder.queryType({})
builder.mutationType({})

export const schema = builder.toSchema({})
