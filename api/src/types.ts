export interface JWTPayload {
  id: string
  isAdmin?: boolean
}
