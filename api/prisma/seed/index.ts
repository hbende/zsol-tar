import { PrismaClient } from '@prisma/client'
import { hash } from 'bcrypt'

import canonizeString from '../../src/util/canonizeString'

import { superAdminEmail, superAdminPassword } from './constants'
import songs from './initial_songs'
import templates from './initial_templates'

const prisma = new PrismaClient()

async function createSuperAdmin() {
  try {
    await prisma.user.create({
      data: {
        email: superAdminEmail,
        name: 'Admin',
        password: await hash(superAdminPassword, 10),
        isAdmin: true
      }
    })
  } catch (err) {
    console.error(err)
  }
}

async function createSongs() {
  try {
    await Promise.all(
      songs.map(
        async (song) =>
          await prisma.song.create({
            data: {
              lyrics: {
                create: song.lyrics.map((verses) => ({
                  verses: { create: verses },
                  creator: { connect: { email: superAdminEmail } },
                  publishStatus: 'Published'
                }))
              },
              name: song.name,
              canonicalName: canonizeString(song.name) as string,
              publishStatus: 'Published',
              creator: { connect: { email: superAdminEmail } }
            }
          })
      )
    )
  } catch (err) {
    console.error(err)
  }
}

async function createTemplates() {
  try {
    await Promise.all(
      templates.map(
        async (template) =>
          await prisma.songOrderTemplate.create({
            data: {
              title: template.title,
              items: {
                createMany: {
                  data: template.items.map((item) => ({
                    title: item.title,
                    order: item.order
                  }))
                }
              },
              publishStatus: 'Published',
              creator: { connect: { email: superAdminEmail } }
            }
          })
      )
    )
  } catch (err) {
    console.error(err)
  }
}

async function main() {
  await createSuperAdmin()
  await createSongs()
  await createTemplates()
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
    process.exit(0)
  })
