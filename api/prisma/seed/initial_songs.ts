import { VerseType } from '.prisma/client'

type VerseData = {
  order?: number
  text: string
  type: VerseType
}

type LyricsData = VerseData[]

type SongData = {
  name: string
  lyrics: LyricsData[]
}

const songs: SongData[] = [
  {
    name: 'Mária, Hozzád száll imánk',
    lyrics: [
      [
        {
          text: `Mária, Hozzád száll imánk! Tiszta virága az égnek!
Mária, gyönyörű liliomszál! Szűzanyánk, kérünk Téged!`,
          order: 1,
          type: 'Verse'
        },
        {
          text: `Könyörögj értünk Jézusnál! Könyörögj értünk, Szűzanyánk!
Követni vágyunk Szent Fiad! Vezess az úton tovább!`,
          type: 'Refrain'
        },
        {
          text: `Mária, ragyogó ékes ág! Egyetlen igen a földön.
Egyedül tiszta és egyedül jó, Istenhez egyedül méltó.`,
          order: 2,
          type: 'Verse'
        }
      ]
    ]
  },
  {
    name: 'Nem vagyok rá méltó',
    lyrics: [
      [
        {
          text: `Nem vagyok rá méltó, hogy a lelkembe szállj
Csak egy szóval mondjad, Jézus, Te nagy király`,
          order: 1,
          type: 'Verse'
        },
        {
          text: `Meggyógyul a lelkem, (meggyógyul a lelkem)
Szent érintésedben (szent érintésedben)`,
          type: 'Refrain'
        },
        {
          text: `Magamhoz veszem az életnek kenyerét
Megváltóm szent testét s hívom az Úr nevét!`,
          order: 2,
          type: 'Verse'
        },
        {
          text: `Uram, Jézus Krisztus, tested vétele
Méltatlanságomban lelkem étele.`,
          order: 3,
          type: 'Verse'
        }
      ]
    ]
  },
  {
    name: 'Ó, uram',
    lyrics: [
      [
        {
          text: `//: Ó, Uram néked zeng szavam, Téged áldalak, ó, Uram! ://`,
          type: 'Refrain'
        },
        {
          text: `Akárhová a kezed ér, ott gyümölcs sarjad, 
Amit megérintesz, életre fakad`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Közelséged felüdít, és megvigasztal engem,
Szent igéd megújítja lelkem.`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Te vagy a fény',
    lyrics: [
      [
        {
          text: `Te vagy a fény a szívemben, Jézus, add, hogy ne szólhasson bennem a sötét! 
Te vagy a fény a szívemben, Jézus, Krisztus jöjj, Te vezess utamon!`,
          type: 'Refrain'
        }
      ]
    ]
  },
  {
    name: 'Tökéletes Jézus',
    lyrics: [
      [
        {
          text: `Életem ereje, hűséges szövetségesem.
Uram és Megváltóm, közelséged elég nekem. Tökéletes Jézus!
Reményem, örömöm, forrásom Belőled fakad.
Ékszerem, koronám, minden gazdagságom Te vagy! Tökéletes Jézus!
Szerelmed féltőn átkarol, hozzád bújva nyugszik meg lelkem.
Békességed szárnyakra emel. Nem rémít a háborgó tenger.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Reménységem, amíg élek el nem hagy.
Az út végén színről-színre látlak majd!`,
          order: 1,
          type: 'Chorus'
        }
      ]
    ]
  },
  {
    name: 'Tart még a kegyelem',
    lyrics: [
      [
        {
          text: `Tart még a kegyelem, és a szívem örül,
Jézus eljött hozzám, rajtam megkönyörült.
Kinyújtotta karját, szelíden felemelt,
Amíg élek, azt a percet nem feledem.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Nyitva áll az ajtó, ma is bejöhetek,
A trónteremben várnak, nekem ott a helyem.
Jézus mellett élni, ez a legjobb dolog,
Ugrálok és táncolok, mert szabad vagyok.`,
          type: 'Verse',
          order: 2
        },
        {
          text: `És velem tapsol és táncol a Menny, Jézus győzelméről zengek új éneket.
És velem tapsol és táncol a Menny, a Bárány dicsőségét hirdetem!`,
          type: 'Refrain'
        }
      ]
    ]
  },
  {
    name: 'A világnak Krisztus kell',
    lyrics: [
      [
        {
          text: `||: A világnak Krisztus kell!
A világnak Krisztus kell!
A világnak kellesz te is,
Mivel Te Krisztushoz tartozol! :|`,
          type: 'Verse',
          order: 1
        },
        {
          text: `||: Emlékezz! Emlékezz!
Te Krisztushoz tartozol! :||`,
          type: 'Refrain'
        }
      ]
    ]
  },
  {
    name: 'Adjunk hálát az Úrnak',
    lyrics: [
      [
        {
          text: `Adjunk hálát az Úrnak,
Aki meghalt és feltámadt!
Ő a mennybe ment, és a trónon ül,
Onnan várjuk, míg eljő.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Alleluja, az Úr az Isten!
Alleluja, a Megváltó!
Alleluja, az Úr az Isten!
Zengj alleluját, amen!`,
          type: 'Refrain'
        },
        {
          text: `Az az égi fény, amit Ő ma küld,
Újraformál minden embert.
Vele minden új, ami régi volt,
Öröm Lelke, áradj ránk!`,
          type: 'Verse',
          order: 2
        }
      ],
      [
        {
          text: `Adjunk hálát az Úrnak,
Aki meghalt és feltámadt!
Ő a mennybe ment, és a trónon ül,
Onnan várjuk, míg eljő.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Alleluja, az Úr az Isten!
Alleluja, a Megváltó!
Alleluja, az Úr az Isten!
Zengj alleluját! Amen!`,
          type: 'Refrain'
        },
        {
          text: `Az ő teste és vére
éltet minket itt a földön.
Gyere és örülj velünk, ünnepelj,
tiszta szívből áldjuk őt!`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Aki benne hisz, sose szomjazik,
ő az élő víz forrása.
Szentlélek, jöjj és szállj le ránk,
tölts be minket szeretet!`,
          type: 'Verse',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'Ahol szeretet',
    lyrics: [
      [
        {
          text: `Ahol szeretet és jóság, 
ahol szeretet, ott van Istenünk.`,
          type: 'Refrain'
        }
      ]
    ]
  },
  {
    name: 'A hegyek s tengerek felett',
    lyrics: [
      [
        {
          text: `A hegyek s tengerek felett folyóid zúgják, hogy szeretsz
S én megnyitom szívem, hogy gyógyításod szabaddá tegyen
Örvendek igazságodon, magasztallak minden napon
Örökké éneklek, és szereteted áldom`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Mindig csak Rólad énekelnék, mindig csak Rólad énekelnék
Mindig csak Rólad énekelnék, mindig csak Rólad énekelnék`,
          type: 'Refrain'
        },
        {
          text: `Szívem ujjong, s táncol, nem érti a világ
De amikor meglátnak, nagy öröm lesz majd, és minden ember táncol.`,
          type: 'Chorus'
        }
      ]
    ]
  },
  {
    name: 'Áldásoddal megyünk',
    lyrics: [
      [
        {
          text: `Áldásoddal megyünk, megyünk innen el.
Néked énekelünk boldog éneket.
Te vagy mindig velünk, ha útra kelünk. 
Őrizd életünk minden nap!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Minden nap dicsérünk Téged, jó Uram.
Néked énekelünk vígan, boldogan.
Maradj mindig velünk, ha útra kelünk! 
Őrizd életünk minden nap!`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Áldjátok az Urat',
    lyrics: [
      [
        {
          text: `//: Áldjátok az Urat, 
áldjátok szent nevét
Kik házában álltok napról-napra, 
dicsérjétek szüntelenül őt ://`,
          type: 'Verse',
          order: 1
        },
        {
          text: `//: Tárjátok kezeitek az élő Isten felé,
     Áldjon meg Téged Sionból az Úr, ragyogtassa arcát reád  ://`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Áldott Istenünk',
    lyrics: [
      [
        {
          text: `Áldott Istenünk, a szívünket fogadd el,
Felajánljuk munkánk a kenyérrel.
Kérünk fogadd el ezt a fehér kenyeret,
Hisz ezáltal leszünk eggyé veled.
A verejtékes munka áldott gyümölcse, és sokmillió ember élete,
Te egyszerű és köztünk élő valóság, így jössz közénk te is, Jézusunk!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Áldott Istenünk, a kehelybe bort töltünk, 
Felajánljuk néked minden örömünk.
Kérünk, fogadd el ezt a kehely fehér bort, 
Hisz a véred egykor miértünk folyt.
Átnyújtjuk most neked szívünk, mindenünk, fogadd el hát egész életünk.
Te nagyszerű és üdvözítő valóság, kérünk, jöjj el közénk, Jézusunk!`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Áldott légy, Uram',
    lyrics: [
      [
        {
          text: `//: Áldott légy, Uram, szent neved áldja lelkem! 
Áldott légy, Uram, mert megváltottál már. ://`,
          type: 'Verse',
          order: 1
        }
      ]
    ]
  },
  {
    name: 'Alleluja – Dicsérjétek az Urat',
    lyrics: [
      [
        {
          text: `//:Alleluja, alleluja, alleluja, alleluja! ://`,
          type: 'Refrain'
        },
        {
          text: `Dicsérjétek az Urat,  áldjátok a magasságban, 
dicsérjétek angyalok, égnek seregei.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Dicsérjétek Nap és Hold, dicsérjétek csillagai, 
dicsérjétek szent nevét, mert megtartja ígéretét!`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Dicsérjétek tűz és jég, áldjátok vihar és szél, 
gyümölcsfák és cédrusok, havas magaslatok!`,
          type: 'Verse',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'Alleluja – Krisztus az Úr',
    lyrics: [
      [
        {
          text: `Alleluja, allelu-alleluja.    
Alleluja, alleluja!
Alleluja, allelu-alleluja,   
alleluja, alleluja!`,
          type: 'Refrain'
        },
        {
          text: `Krisztus az Úr, erő s hatalom övé, 
bátran bízz Benne, hisz mondta:
„Ki újra kopog, annak ajtót nyitnak majd”, 
alleluja, alleluja!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Föltámadt Krisztus, legyőzte a halált, 
mindenre új hajnal virrad.
Indulj az úton, és soha meg ne állj!
Alleluja, alleluja!`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Keressétek előbb Isten országát,
és az ő igazságát!
És mindenek megadatnak néktek,
Alleluja, alleluja!`,
          type: 'Verse',
          order: 3
        }
      ],
      [
        {
          text: `Alleluja, allelu-alleluja.    
Alleluja, alleluja!
Alleluja, allelu-alleluja,   
alleluja, alleluja!`,
          type: 'Refrain'
        },
        {
          text: `Föltámadt Krisztus, legyőzte a halált, 
          mindenre új hajnal virrad.
          Indulj az úton, és soha meg ne állj!
          Alleluja, alleluja!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Velünk jön Jézus a hűséges barát,
          Ő hívott erre az útra.
          Hirdessük népének szerető szavát,
          Alleluja, alleluja!`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Alleluja – Dicsérjétek az Urat',
    lyrics: [
      [
        {
          text: `//:Alleluja, alleluja, alleluja, alleluja! ://`,
          type: 'Refrain'
        },
        {
          text: `Dicsérjétek az Urat,  áldjátok a magasságban, 
dicsérjétek angyalok, égnek seregei.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Dicsérjétek Nap és Hold, dicsérjétek csillagai, 
dicsérjétek szent nevét, mert megtartja ígéretét!`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Dicsérjétek tűz és jég, áldjátok vihar és szél, 
gyümölcsfák és cédrusok, havas magaslatok!`,
          type: 'Verse',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'A mélyből kiáltok, Uram, hozzád',
    lyrics: [
      [
        {
          text: `A mélyből kiáltok Uram hozzád, ne hagyj el, szabadíts meg!
Mutasd meg magad, végtelen Isten, ne hagyj elveszni bűneimben!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `//: Uram irgalmazz, Uram irgalmazz! ://`,
          type: 'Refrain',
          order: 1
        },
        {
          text: `Te elküldted hozzánk Fiadat, Jézust, ki kereszten szenvedett értünk
De közénk jön most a kenyérben, borban, a búcsúvacsorán ígérte nékünk.`,
          type: 'Verse',
          order: 2
        },
        {
          text: `//: Krisztus kegyelmezz, Krisztus kegyelmezz! ://`,
          type: 'Refrain',
          order: 2
        },
        {
          text: `A mélyből kiáltok Uram hozzád, hol zarándokútját járja néped.
A kereszt jelével a homlokunkon Fiad nevében kérünk téged.`,
          type: 'Verse',
          order: 3
        },
        {
          text: `//: Uram irgalmazz, Uram irgalmazz! ://`,
          type: 'Refrain',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'Gyönyörű vagy, én Uram',
    lyrics: [
      [
        {
          text: `Gyönyörű vagy, én Uram, szívem mélyéből áldalak,
szívem mélyéből áldalak.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Alleluja`,
          type: 'Refrain'
        },
        {
          text: `Megigézted szívemet, //: megérintetted bensőmet. ://
Táncot járva ámulok, //: megváltásomra gondolok. ://`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Lelked betölti életem, //: Benned ujjong, dalol szívem. ://
Tegyél szívedre pecsétül, //: örök életet adj végül! ://`,
          type: 'Verse',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'Hadd jöjjek én',
    lyrics: [
      [
        {
          text: `Hadd jöjjek én forrásodhoz, Uram! Hadd jöjjek én forrásodhoz, Uram!
Hadd jöjjek én forrásodhoz, Uram!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Hadd jöjjek én! 
Hadd igyak én! 
Hadd éljek én!`,
          type: 'Refrain'
        },
        {
          text: `//: Hadd igyak én forrásodból, Uram! :// (3x)`,
          type: 'Verse',
          order: 2
        },
        {
          text: `//: Hadd éljek én forrásodnál, Uram! :// (3x)`,
          type: 'Verse',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'Hálát adok, az esti órán',
    lyrics: [
      [
        {
          text: `Hálát adok az esti órán, hálát adok, hogy itt az éj.
Hálát adok, hogy szívem mélyén hála dala kél.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Hálát adok a csillagfényért, hálát a sűrű éjjelért.
Hálát adok, hogy nem vagy messze, most is küldesz fényt.`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Hálát adok, hogy küldtél testvért, társként, aki ma mellém állt
Hálát adok, hogy megengedted azt is, ami fájt.`,
          type: 'Verse',
          order: 3
        },
        {
          text: `Hálát adok, hogy Tested táplált, hálát, hogy ma is szólt szavad.
Hálát adok, hogy lelkem biztos cél felé halad.`,
          type: 'Verse',
          order: 4
        },
        {
          text: `Hálát adok, hogy mellém álltál, hálát adok, hogy szép az otthonom. 
Hálát, hogy munkám elvégeztem, s békén alhatom.`,
          type: 'Verse',
          order: 5
        },
        {
          text: `Hálát adok a sok-sok jóért, hálát adok, hogy megbocsátsz.
Hálát adok, hogy el nem hagytál, s újabb útra vársz.`,
          type: 'Verse',
          order: 6
        },
        {
          text: `Hálát adok, hogy szemed rajtam, hálát adok, hogy reményt adsz.
Hálát adok, hogy biztos eljössz, mint a virradat.`,
          type: 'Verse',
          order: 7
        }
      ]
    ]
  },
  {
    name: 'Hálát adok, hogy itt a reggel',
    lyrics: [
      [
        {
          text: `Hálát adok, hogy itt a reggel, hálát adok az új napon.
Hálát adok, hogy minden percem néked adhatom.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Hálát nem csak a jó testvérért, hálát mindenkiért adok.
Hálát adok, hogy minden sértést megbocsáthatok.`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Hálát adok, hogy munkát küldesz, hálát adok, hogy fény ragyog.
Hálát adok, hogy lelkem fényes, és boldog vagyok.`,
          type: 'Verse',
          order: 3
        },
        {
          text: `Hálát adok a vidám percért, hálát, ha szomorú leszek.
Hálát adok, hogy adsz elém célt és kezed vezet.`,
          type: 'Verse',
          order: 4
        },
        {
          text: ` Hálát adok, hogy hallom hangod, hálát adok a jó hírért.
Hálát adok, hogy Tested adtad minden emberért.`,
          type: 'Verse',
          order: 5
        },
        {
          text: `Hálát adok az üdvösségért, hálát adok, hogy várni fogsz.
Hálát adok, hogy ma reggel még hálát adhatok.`,
          type: 'Verse',
          order: 6
        }
      ]
    ]
  },
  {
    name: 'Halló szívet adj nekem',
    lyrics: [
      [
        {
          text: `Halló szívet adj nekem, add szavadat értenem,
Istenem, Istenem, halló szívet adj nekem.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Csendességben, harsogásban, hétköznapi tompaságban,
Istenem, Istenem, halló szívet adj nekem.`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Hozzád száll az énekem',
    lyrics: [
      [
        {
          text: `Hozzád száll az énekem,      
olyan jó Téged dicsérni,
Bűnös volt az életem,     
Te eljöttél, hogy megments engem.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `A mennyből jöttél közénk, hogy út legyél,
Feláldoztad magad a bűnömért.
A Golgotán meghaltál, harmadnap feltámadtál,
S uralkodsz mindörökké.`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Hús-vér templom',
    lyrics: [
      [
        {
          text: `Nekem nincs más Rajtad kívül Jézus, életem forrása vagy
Kézzel, lábbal, szívvel, szájjal dicsérlek Uram.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Ez a hús-vér templom Érted épült, neked ég a tűz, benn oltárainál.
Ez a hús-vér templom Téged dicsér, szívem minden húrja rólad muzsikál.`,
          type: 'Refrain'
        }
      ]
    ]
  },
  {
    name: 'Ím, eléd ünnepelni jöttünk',
    lyrics: [
      [
        {
          text: `Ím, eléd ünnepelni jöttünk, mint egy nagy család
Szívünkből kiáltunk most Hozzád, Isten, nagy király,           
zengjük`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Abba, Atyánk, áldott szeretet! Abba, Atyánk, szent a Te neved!`,
          type: 'Refrain'
        }
      ]
    ]
  },
  {
    name: 'Immár a nap leáldozott',
    lyrics: [
      [
        {
          text: `Immár a nap leáldozott, Teremtőnk kérünk Tégedet,
Légy kegyes és maradj velünk, őrizzed, óvjad népedet!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Álmodjék Rólad a szívünk, álmunkban is Te légy velünk,
Téged dicsérjen énekünk, midőn új napra ébredünk.`,
          type: 'Refrain'
        },
        {
          text: `Adj nékünk üdvös életet, szítsd fel a szív fáradt tüzét:
A ránk törő rút éjhomályt világosságod rontsa szét!`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Kérünk mindenható Atyánk, úr Jézus Krisztus érdemén,
Ki Szentlélekkel és veled uralkodik, s örökkön él.`,
          type: 'Verse',
          order: 3
        },
        {
          text: `Mikor lecsukjuk két szemünk, a szívünk virrasszon Veled,
Őrködjék mindig jobb kezed hűséges híveid felett!`,
          type: 'Verse',
          order: 4
        }
      ]
    ]
  },
  {
    name: 'Indulj és menj',
    lyrics: [
      [
        {
          text: `Indulj és menj, hirdesd szavam, népemhez küldelek én.
Tövis és gaz, vér és panasz, meddig hallgassam még én.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `//: Küldelek én s megáldlak én, csak menj és hirdesd szavam! ://`,
          type: 'Refrain'
        },
        {
          text: `Tüzessé teszem ajkaidat, gyémánttá homlokodat,          
Népemnek őrévé rendellek én, Lelkemet adom melléd.`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Irgalmas Istenünk',
    lyrics: [
      [
        {
          text: `Irgalmas Istenünk jóságát mindörökké éneklem.`,
          type: 'Verse',
          order: 1
        }
      ]
    ]
  },
  {
    name: 'Isten báránya (Sillye)',
    lyrics: [
      [
        {
          text: `Isten báránya, te elveszed a világ bűneit,
Irgalmazz, irgalmazz nekünk!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Isten báránya, te elveszed a világ bűneit,
Irgalmazz, irgalmazz nekünk!`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Isten báránya, te elveszed a világ bűneit,
Adj nekünk, adj nekünk békét!`,
          type: 'Verse',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'Isten báránya (Sillye IV.)',
    lyrics: [
      [
        {
          text: `Isten báránya, te elveszed a világ bűneit,
Irgalmazz nekünk, irgalmazz nekünk, irgalmazz nekünk!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Isten báránya, te elveszed a világ bűneit,
Irgalmazz nekünk, irgalmazz nekünk, irgalmazz nekünk!`,
          type: 'Verse',
          order: 2
        },
        {
          text: `Isten báránya, te elveszed a világ bűneit,
Adj nekünk békét, adj nekünk békét, adj nekünk békét!`,
          type: 'Verse',
          order: 3
        }
      ]
    ]
  },
  {
    name: 'Isteni béke',
    lyrics: [
      [
        {
          text: `Isteni béke, boldog öröm. Szállj le szívembe, isteni csönd!`,
          type: 'Verse',
          order: 1
        }
      ]
    ]
  },
  {
    name: 'Ízleld és lásd',
    lyrics: [
      [
        {
          text: `Ízleld és lásd, hogy jó az Úr, 
ó ízleld és lásd, hogy az Úr jó!
Boldog ember az, ki benne bízik, 
boldog ember az, ki benne bízik.`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Ő jó, Ő jó, Ő jó, Ő jó.`,
          type: 'Verse',
          order: 2
        }
      ]
    ]
  },
  {
    name: 'Jézus bennem él',
    lyrics: [
      [
        {
          text: `Jézus bennem él, 
békéje már az enyém
Fénye ragyog szívemen át,
boldogan mondok imát.`,
          type: 'Verse',
          order: 1
        }
      ]
    ]
  },
  {
    name: 'Jézus, életem',
    lyrics: [
      [
        {
          text: `Jézus életem, erőm, békém, Jézus társam, örömöm.
Benned bízom, Te vagy az Úr,
Már nincs mit félnem, mert bennem élsz. 
Már nincs mit félnem, mert bennem élsz.`,
          type: 'Verse',
          order: 1
        }
      ]
    ]
  },
  {
    name: 'Jézus, én Téged választottalak',
    lyrics: [
      [
        {
          text: `Jézus én Téged választottalak, 
hol halál jár és fojtogat,
Ott Te vagy nekem az élet!
Feléd tárom két kezem, 
engem küldjél Istenem, 
Jelként, ahova kell!`,
          type: 'Verse',
          order: 1
        },
        {
          text: `Hát jöjj és élj velem,
egy egész életen át!
Nem győz a félelem,
ha látja szívem Urát!`,
          type: 'Refrain'
        }
      ]
    ]
  }
]

export default songs
