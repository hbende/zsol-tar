export const superAdminEmail = process.env.ADMIN_USER_EMAIL ?? 'admin@site.com'
export const superAdminPassword =
  process.env.ADMIN_USER_PASSWORD ??
  "I'm not kidding, please select a secure password or you'll have to type this into the login form"
