import React, { ReactElement } from 'react'

import clsx from 'clsx'
import UserIcon from 'components/UserIcon'
import { CommentFragment } from 'generated/apollo-components'
import { transformDate } from 'util/formatDate'

interface CommentProps {
  className?: string
  comment: CommentFragment
}

export default function Comment({ className, comment }: CommentProps): ReactElement {
  return (
    <div className={clsx('max-w-md space-y-2 p-2', className)}>
      <p className="border border-purple-500 rounded-md p-2 bg-indigo-100">{comment.text}</p>
      <div className="flex justify-between items-center">
        <UserIcon user={comment.user} />
        <span>{transformDate(comment.createdAt)}</span>
      </div>
    </div>
  )
}
