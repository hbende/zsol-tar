import { mockUser } from 'components/Profile/mocks'
import { CommentFragment, PublishStatus } from 'generated/apollo-components'

export const mockComment: CommentFragment = {
  user: mockUser,
  createdAt: new Date('2020'),
  id: '1',
  song: { id: '1', name: 'ének' },
  text: 'This is my comment',
  publishStatus: PublishStatus.Published
}
