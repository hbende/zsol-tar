import React, { ReactElement } from 'react'
import Head from 'next/head'

interface HeadProps {
  pageName?: string
}

export default function CommonHead({ pageName }: HeadProps): ReactElement {
  return (
    <Head>
      <title>{pageName ? `${pageName} - Akkordo` : 'Akkordo'}</title>
    </Head>
  )
}
