import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './SongForm'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongForm'
}
export default meta

const template = storyTemplate(Component)

export const Page = template({ onSubmit: action('submit') })
