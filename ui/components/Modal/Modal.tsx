import { useEffect, useState } from 'react'
import { TiDeleteOutline } from 'react-icons/ti'
import ReactModal from 'react-modal'

import clsx from 'clsx'

type ModalProps = {
  children: React.ReactNode
  isOpen: boolean
  onClose?: () => void
}

function Modal({ children, isOpen, onClose = () => {} }: ModalProps) {
  const [isBrowser, setIsBrowser] = useState(false)

  useEffect(() => {
    setIsBrowser(true)
  }, [])

  function handleClose() {
    onClose()
  }

  useEffect(() => {
    if (isOpen) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = ''
    }
    return () => {
      document.body.style.overflow = ''
    }
  }, [isOpen])

  if (isBrowser) {
    return (
      <ReactModal
        isOpen={isOpen}
        onRequestClose={handleClose}
        parentSelector={() => document.getElementById('modal-root') as HTMLElement}
        overlayElement={({ className, ...props }, children) => (
          <div className={clsx('z-[1000]', className)} {...props}>
            {children}
          </div>
        )}
        contentElement={({ className, ...props }, children) => (
          <div className={clsx('z-[1000]', className)} {...props}>
            {children}
          </div>
        )}
      >
        <div className="relative">
          <div className="absolute top-1 right-1 cursor-pointer p-2 group" onClick={handleClose}>
            <span className="sr-only">Bezárás</span>
            <TiDeleteOutline className="w-8 h-8 text-indigo-600 group-hover:text-indigo-800" />
          </div>
          {children}
        </div>
      </ReactModal>
    )
  } else {
    return null
  }
}

export default Modal
