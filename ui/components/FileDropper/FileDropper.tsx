import React, { useState } from 'react'
import { DropzoneOptions, DropzoneState, useDropzone } from 'react-dropzone'
import { TiDelete } from 'react-icons/ti'

import clsx from 'clsx'
import HelperText from 'components/ui/HelperText'

type FileDropperProps = DropzoneOptions & { onChange?: (e: any) => void }

function FileDropper({ onChange, ...props }: FileDropperProps) {
  const { getRootProps, getInputProps, isDragActive, acceptedFiles } = useDropzone({
    ...props
  })

  const [key, setKey] = useState(0)

  const removeFile = (file: File) => {
    acceptedFiles.splice(acceptedFiles.indexOf(file), 1)
    onChange?.({ target: { files: acceptedFiles } })
    setKey((key) => key + 1)
  }

  const inputProps = getInputProps({
    onChange
  })

  return (
    <div
      {...getRootProps()}
      className={clsx(
        'border rounded border-purple-300 focus:border-purple-700 focus:border-2 bg-gold-100 w-full h-full flex justify-center items-center min-h-[6rem] cursor-pointer flex-col space-y-2',
        props.disabled && 'border-gray-200'
      )}
    >
      <input {...inputProps} />
      {props.disabled ? (
        <div>
          <HelperText className="text-gray-500">Jelenleg nem tudsz fájlt hozzáadni</HelperText>
        </div>
      ) : (
        <div>
          {isDragActive ? (
            <HelperText>Húzd ide a fájlokat</HelperText>
          ) : (
            <HelperText>Húzd ide a fájlokat vagy kattints a mezőre</HelperText>
          )}
        </div>
      )}
      {!!acceptedFiles.length && (
        <ul className="text-sm text-indigo-600" key={key}>
          Fájlok:
          {acceptedFiles.map((file) => (
            <li key={file.name} className="flex space-x-1 items-center relative">
              {file.name} - {(file.size / 1024 / 1024).toFixed(2)} megabájt
              <span>
                <span className="sr-only">Törlés</span>
                <TiDelete
                  className="text-red-800 h-4 w-4"
                  onClick={(e) => {
                    e.stopPropagation()
                    removeFile(file)
                  }}
                />
              </span>
            </li>
          ))}
        </ul>
      )}
    </div>
  )
}

export default FileDropper
