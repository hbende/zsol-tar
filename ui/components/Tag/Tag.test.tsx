import renderer from 'react-test-renderer'

import Tag from './Tag'

it('renders Tag without crashing', () => {
  const tree = renderer.create(<Tag>Any text</Tag>).toJSON()
  expect(tree).toMatchSnapshot()
})
