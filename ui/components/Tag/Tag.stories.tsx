import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './Tag'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'UI/Tag'
}
export default meta

const template = storyTemplate(Component)

export const Tag = template({ children: 'Tag#1' })
export const Long = template({
  children: 'Tag#1234 and it continues like this',
  deletable: true,
  onDelete: action('delete')
})
export const Deletable = template({
  children: 'Tag#1',
  deletable: true,
  onDelete: action('delete')
})
