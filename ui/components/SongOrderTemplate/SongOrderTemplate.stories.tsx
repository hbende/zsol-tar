import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import { mockSongOrderTemplate } from './mocks'
import Component from './SongOrderTemplate'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongOrderTemplate'
}
export default meta

const template = storyTemplate(Component)

export const SongOrderTemplate = template({
  template: mockSongOrderTemplate,
  editable: true,
  onEdit: action('onEdit')
})

export const UnEditable = template({
  template: mockSongOrderTemplate,
  onEdit: action('onEdit'),
  editable: false
})
