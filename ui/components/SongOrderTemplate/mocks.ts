import { mockUser } from 'components/Profile/mocks'
import { TemplateFragment } from 'generated/apollo-components'
export const mockSongOrderTemplate: TemplateFragment = {
  id: 'a',
  title: 'Sablon',
  createdAt: new Date(),
  creator: mockUser,
  items: [
    { id: '1', order: 0, title: 'Bevonulás' },
    { id: '2', order: 1, title: 'Kyrie' },
    { id: '3', order: 2, title: 'Gloria' },
    { id: '4', order: 3, title: 'Olvasmányközi ének' },
    { id: '5', order: 4, title: 'Alleluja' },
    { id: '6', order: 5, title: 'Felajánlás' },
    { id: '7', order: 6, title: 'Sanctus' }
  ]
}
