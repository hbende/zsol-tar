import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './Header'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'Header'
}
export default meta

const template = storyTemplate(Component)

export const LoggedIn = template({ loggedIn: true, onLogout: action('onLogout') })
export const LoggedOut = template({ loggedIn: false, onLogout: action('onLogout') })
export const Admin = template({ loggedIn: false, isAdmin: true, onLogout: action('onLogout') })
