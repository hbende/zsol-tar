import React, { useEffect, useRef, useState } from 'react'
import dynamic from 'next/dynamic'

const { RevealJS } = {
  RevealJS: dynamic(() => import('@gregcello/revealjs-react').then((mod) => mod.RevealJS), {
    ssr: false
  })
}

import Reveal, { MightBeRevealPlugin } from '@gregcello/revealjs-react/types/reveal.js'
import ActionBar from 'components/SongOrderEditor/ActionBar'
import { SongOrderWithGroupsFragment, TemplateFragment } from 'generated/apollo-components'
import screenfull from 'screenfull'

import { LocalSlideGroup } from './PresentationEditor/PresentationEditor'
import SideBar, { PresentationEditorMenu } from './SideBar/SideBar'
import Slides from './Slides'
import { LocalSongOrderItem } from './SongOrderEditor/SongOrderEditor'

import RevealJsStyles from './RevealJsStyles'
import { RevealJsTheme } from './RevealJsStyles/RevealJsStyles'

function useInnerHTML() {
  const container = useRef<HTMLDivElement>(null)
  const [innerHTML, setInnerHTML] = useState('')

  useEffect(() => {
    if (container && container.current) {
      setInnerHTML(container.current.innerHTML)
    }
  }, [container])

  return { container, innerHTML }
}

type SongOrderEditorProrps = {
  songOrder?: SongOrderWithGroupsFragment | null
  onSongOrderChange?: (items: LocalSongOrderItem[], title?: string) => void
  onSaveAsTemplate?: (items: Pick<LocalSongOrderItem, 'title' | 'order'>[]) => void
  initialMenu?: PresentationEditorMenu
  initialItems?: TemplateFragment['items']
  onSetTheme?: (newTheme: RevealJsTheme) => void
}

export function useExposedDeck() {
  const [deck, setDeck] = useState<Reveal<MightBeRevealPlugin[]> | null>(null)

  return { deck, setDeck }
}

export function isPresentationEmpty(presentation: SongOrderWithGroupsFragment): boolean {
  const numberOfSlides = presentation.groups.reduce((acc, curr) => {
    return acc + curr.slides.length
  }, 0)
  return numberOfSlides === 0
}

function getStartingSlide(presentation: SongOrderWithGroupsFragment, group: LocalSlideGroup) {
  return presentation.groups.reduce((acc, curr) => {
    if (curr.order >= group.order) {
      return acc
    }
    return acc + curr.slides.length
  }, 0)
}

export function SongOrderEditor({
  songOrder,
  onSongOrderChange,
  onSaveAsTemplate,
  initialMenu = 'songOrder',
  onSetTheme
}: SongOrderEditorProrps) {
  const { container } = useInnerHTML()
  const { setDeck, deck } = useExposedDeck()
  const [theme, setTheme] = useState<RevealJsTheme>(songOrder?.presentationConfig?.theme || 'black')

  return (
    <div className="flex flex-col lg:flex-row lg:h-full">
      <aside className="lg:w-1/3 lg:h-[calc(100vh-6rem)]">
        <SideBar
          songOrder={songOrder}
          initialMenu={initialMenu}
          onSongOrderChange={(...args) => {
            onSongOrderChange?.(...args)
            deck?.sync()
          }}
          onSaveAsTemplate={onSaveAsTemplate}
          onJumpToSlideGroup={(group) => {
            if (!songOrder?.groups.length) {
              return
            }
            deck?.slide(getStartingSlide(songOrder, group))
          }}
        />
      </aside>
      <div className="lg:w-2/3">
        <div className="min-h-[300px] lg:h-5/6 w-auto">
          <div className="h-80 md:h-96 lg:h-full w-full relative" ref={container}>
            <RevealJsStyles theme={theme} />
            <RevealJS
              {...songOrder?.presentationConfig}
              // @ts-expect-error bad typing
              controlsLayout="edges"
              keyboardCondition="focused"
              embedded={true}
              autoSlide={0}
              className="relative"
              onDeckReady={(deck: Reveal<MightBeRevealPlugin[]>) => {
                setDeck(deck)
                // timeout shouldn't be used
                setTimeout(() => deck.layout(), 100)
              }}
            >
              <Slides songOrder={songOrder} />
            </RevealJS>
          </div>
          <ActionBar
            onFullScreen={() => {
              const revealContainer = document.getElementsByClassName('reveal')[0]
              if (screenfull.isEnabled) {
                screenfull.request(revealContainer)
                return
              } else {
                alert(
                  'A böngésződ nem támogatja a teljes képernyős mód megkezdését. Kattints a prezentációra, majd nyomj "F" gombot'
                )
              }
            }}
            theme={theme}
            setTheme={(newTheme) => {
              onSetTheme?.(newTheme)
              setTheme(newTheme)
            }}
            onExport={() => {
              window.open(`${window.location.origin}/presentation/${songOrder?.id}?print-pdf`)
            }}
            presentationId={songOrder?.id}
            className="mt-2"
          />
        </div>
      </div>
    </div>
  )
}

export default SongOrderEditor
