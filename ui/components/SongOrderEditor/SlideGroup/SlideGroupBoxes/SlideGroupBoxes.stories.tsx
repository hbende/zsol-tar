import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './SlideGroupBoxes'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'PresentationEditor/SlideGroupBoxes'
}
export default meta

const template = storyTemplate(Component)

export const SlideGroupBoxes = template({
  slides: [
    { children: 'R1', onClick: action('r1') },
    { children: 'V1', onClick: action('v1') },
    { children: 'R', onClick: action('r1') }
  ]
})
