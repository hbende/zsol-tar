import React from 'react'

import clsx from 'clsx'
import SlideBox, { SlideBoxProps } from 'components/SongOrderEditor/SlideGroup/SlideBox'

type SlideGroupBoxesProps = {
  slides?: SlideBoxProps[]
  className?: string
}

export default function SlideGroupBoxes({ slides = [], className }: SlideGroupBoxesProps) {
  return (
    <div className={clsx('space-y-4', className)}>
      {slides.map((slide, i) => (
        <SlideBox key={i} {...slide} />
      ))}
    </div>
  )
}
