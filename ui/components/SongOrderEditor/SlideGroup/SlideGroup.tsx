import React from 'react'
import { TiArrowDown, TiArrowUp, TiDelete, TiEdit, TiLocation, TiNotes } from 'react-icons/ti'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import { ButtonProps } from 'components/ui/Button/Button'
import Tooltip from 'components/ui/Tooltip'

import { LocalSlideGroup } from '../PresentationEditor/PresentationEditor'
import SlideGroupBoxes from './SlideGroupBoxes'

type SlideGroupProps = {
  group: LocalSlideGroup
  className?: string
  onDelete?: () => void
  onEdit?: () => void
  onJump?: () => void
  onGoToSong?: () => void
  moveUpButtonProps?: Partial<ButtonProps>
  moveDownButtonProps?: Partial<ButtonProps>
}

export default function SlideGroup({
  group,
  className,
  onDelete = () => {},
  onEdit = () => {},
  onJump = () => {},
  onGoToSong = () => {},
  moveDownButtonProps = {},
  moveUpButtonProps = {}
}: SlideGroupProps) {
  return (
    <div className={clsx('flex', className)}>
      <div className="p-2 w-2/5">
        <h3>
          {!!group.title && 'Cím:'} {group.title}
        </h3>
        <SlideGroupBoxes
          slides={
            group.slides.length
              ? group.slides.map(() => ({ children: group.song?.name }))
              : [{ children: `Üres` }]
          }
        />
      </div>
      <div className="flex flex-col justify-center items-center space-y-2 ml-4">
        {/* <Tooltip label="Mozgatás előre"> */}
        <Button color="indigo" type="button" size="small" {...moveUpButtonProps}>
          <TiArrowUp />
        </Button>
        {/* </Tooltip> */}
        {/* <Tooltip label="Törlés"> */}
        <Button color="indigo" size="small" onClick={onDelete} disabled={!!group.song}>
          <TiDelete />
        </Button>
        {/* </Tooltip> */}
        {/* <Tooltip label="Szerkesztés"> */}
        <Button color="indigo" size="small" onClick={onEdit}>
          <TiEdit />
        </Button>
        {/* </Tooltip> */}
        {/* <Tooltip label="Ugrás a diához"> */}
        <Button color="indigo" size="small" onClick={onJump}>
          <TiLocation />
        </Button>
        {/* </Tooltip> */}
        {!!group.song && (
          // <Tooltip label="Ugrás az énekhez">
          <Button color="indigo" size="small" onClick={onGoToSong}>
            <TiNotes />
          </Button>
          // </Tooltip>
        )}
        {/* <Tooltip label="Mozgatás hátra"> */}
        <Button color="indigo" type="button" size="small" {...moveDownButtonProps}>
          <TiArrowDown />
        </Button>
        {/* </Tooltip> */}
      </div>
    </div>
  )
}
