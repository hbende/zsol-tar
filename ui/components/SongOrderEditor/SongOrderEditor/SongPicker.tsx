import React, { useState } from 'react'
import { Controller, ControllerProps } from 'react-hook-form'
import { TiArrowDown, TiArrowUp, TiDelete } from 'react-icons/ti'
import { InputProps } from 'react-select'

import clsx from 'clsx'
import AsyncSelect from 'components/ui/AsyncSelect'
import Button from 'components/ui/Button'
import { ButtonProps } from 'components/ui/Button/Button'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import TagSelect from 'components/ui/TagSelect'
import Tooltip from 'components/ui/Tooltip'
import { useSearchSongsLazyQuery, useUserTagsLazyQuery } from 'generated/apollo-components'
import debounce from 'util/debounce'

type SongPickerProps = {
  className?: string
  titleInputProps: Partial<InputProps>
  moveUpButtonProps: Partial<ButtonProps>
  moveDownButtonProps: Partial<ButtonProps>
  removeButtonProps: Partial<ButtonProps>
  songControllerProps: Partial<ControllerProps> & Pick<ControllerProps, 'name' | 'control'>
}

export default function SongPicker({
  className,
  titleInputProps,
  moveDownButtonProps,
  moveUpButtonProps,
  removeButtonProps,
  songControllerProps
}: SongPickerProps) {
  const [, { refetch: refetchTags, loading: tagsLoading }] = useUserTagsLazyQuery()
  const [, { refetch: refetchSongs, loading: songsLoading }] = useSearchSongsLazyQuery()
  const [tags, setTags] = useState<{ label: string; value: string }[]>([])

  const loadTagOptions = debounce(200, (input: string) => {
    return refetchTags(input ? { where: { text: { contains: input } } } : {}).then((userTags) =>
      (userTags?.data?.userTags || []).map((tag) => ({
        label: tag.text,
        value: tag.id
      }))
    )
  })

  const loadSongOptions = debounce(200, (input: string) => {
    return refetchSongs(
      input
        ? {
            where: {
              name: { contains: input },
              ...(tags.length
                ? {
                    labels: {
                      some: { text: { in: tags.map((tag) => tag.label) } }
                    }
                  }
                : {})
            }
          }
        : {}
    ).then((songs) =>
      (songs?.data?.publishedSongs || []).map((song) => ({
        label: song.name,
        value: song.id
      }))
    )
  })

  return (
    <div className={clsx('flex items-center space-y-4', className)}>
      <div className="w-2/3 space-y-2">
        <Label>Elnevezés</Label>
        <Input
          {...titleInputProps}
          type="text"
          placeholder="Elem (pl. bevonulás)"
          className="w-full h-10"
        />
        <Label>Ének</Label>
        <TagSelect
          isSearchable
          pageSize={20}
          value={tags}
          onChange={(values) => {
            setTags(values as any)
          }}
          placeholder="Keress a cimkéid között"
          isLoading={tagsLoading}
          loadOptions={loadTagOptions}
          className="h-10"
        />
        <Controller
          {...songControllerProps}
          render={({ field }) => {
            return (
              <AsyncSelect
                {...field}
                isSearchable
                pageSize={20}
                placeholder="Keress egy éneket"
                isLoading={songsLoading}
                defaultOptions
                loadOptions={loadSongOptions}
                className="w-full h-10"
              />
            )
          }}
        />
      </div>
      <div className="w-1/3 h-full">
        <div className="pl-2 flex items-center h-full flex-col space-y-2 justify-center">
          <Tooltip label="Mozgatás előre">
            <Button color="indigo" type="button" size="small" {...moveUpButtonProps}>
              <TiArrowUp />
            </Button>
          </Tooltip>
          <Tooltip label="Törlés">
            <Button color="indigo" type="button" size="small" {...removeButtonProps}>
              <TiDelete />
            </Button>
          </Tooltip>
          <Tooltip label="Mozgatás hátra">
            <Button color="indigo" type="button" size="small" {...moveDownButtonProps}>
              <TiArrowDown />
            </Button>
          </Tooltip>
        </div>
      </div>
    </div>
  )
}
