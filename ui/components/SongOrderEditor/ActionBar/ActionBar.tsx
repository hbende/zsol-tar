import React from 'react'
import { TiDeviceDesktop, TiExport, TiSocialGithub } from 'react-icons/ti'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import HelperText from 'components/ui/HelperText'
import Tooltip from 'components/ui/Tooltip'
import { noop } from 'lodash'

import { RevealJsTheme } from '../RevealJsStyles/RevealJsStyles'

type ActionBarProps = {
  className?: string
  onExport?: () => void
  onFullScreen?: () => void
  theme?: RevealJsTheme
  setTheme?: (theme: RevealJsTheme) => void
  presentationId?: string
}

const themeOptions: { value: RevealJsTheme; label: string }[] = [
  {
    label: 'Black',
    value: 'black'
  },
  { label: 'Beige', value: 'beige' },
  { label: 'Blood', value: 'blood' },
  { label: 'League', value: 'league' },
  { label: 'Moon', value: 'moon' },
  { label: 'Night', value: 'night' },
  { label: 'Serif', value: 'serif' },
  { label: 'Simple', value: 'simple' },
  { label: 'Sky', value: 'sky' },
  { label: 'Solarized', value: 'solarized' },
  { label: 'White', value: 'white' }
]

export default function ActionBar({
  className,
  onExport = noop,
  onFullScreen = noop,
  theme,
  setTheme = noop,
  presentationId
}: ActionBarProps) {
  return (
    <div
      className={clsx(
        'lg:h-1/6 flex flex-wrap justify-end items-center space-x-2 space-y-2 px-2',
        className
      )}
    >
      <Tooltip label="Vetítés címének vágólapra másolása">
        <Button
          color="indigo"
          className="relative"
          onClick={() =>
            navigator.clipboard.writeText(
              `${window.location.origin}/presentation/${presentationId}`
            )
          }
        >
          <TiExport />
          <span className="sr-only">Vetítés címének vágólapra másolása</span>
        </Button>
      </Tooltip>
      <Tooltip label="Vetítés">
        <Button color="indigo" className="relative" onClick={onFullScreen}>
          <TiDeviceDesktop />
          <span className="sr-only">Vetítés teljes képernyőn</span>
        </Button>
      </Tooltip>
      <div>
        <Button color="indigo" onClick={onExport}>
          Exportálás
        </Button>
      </div>
      <select
        className="h-10"
        value={theme}
        onChange={(e) => {
          setTheme(e.target.value as RevealJsTheme)
        }}
      >
        {themeOptions.map((option) => {
          return (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          )
        })}
      </select>
    </div>
  )
}
