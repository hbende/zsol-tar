import { useEffect, useState } from 'react'

import Modal from 'components/Modal'
import PresentationConfig from 'components/PresentationConfig'
import { PresentationConfiguration } from 'components/PresentationConfig/PresentationConfig'
import ManualTextEditor from 'components/SongDetails/ManualTextEditor'
import {
  SongOrderDocument,
  SongOrderWithGroupsFragment,
  useCreateSlideGroupMutation,
  useDeleteSlideGroupMutation,
  useInsertSlideGroupMutation,
  useSongWithAllLyricsLazyQuery,
  useUpdatePresentationConfigMutation,
  useUpdateSlideGroupMutation,
  useUpdateSongOrderNoteMutation
} from 'generated/apollo-components'
import { noop } from 'lodash'

import PresentationEditor from '../PresentationEditor'
import { LocalSlideGroup } from '../PresentationEditor/PresentationEditor'
import SideBarHeader from '../SideBarHeader/SideBarHeader'
import SongOrderEditor from '../SongOrderEditor/index'
import { LocalSongOrderItem } from '../SongOrderEditor/SongOrderEditor'
import SongOrderNoteEditor from '../SongOrderNoteEditor'
import VersePicker from '../VersePicker'

export type PresentationEditorMenu = 'songOrder' | 'slides' | 'slidegroup' | 'notes'

type SlideBarProps = {
  songOrder?: SongOrderWithGroupsFragment | null
  initialMenu: PresentationEditorMenu
  onSongOrderChange?: (items: LocalSongOrderItem[], title?: string) => void
  onSaveAsTemplate?: (items: Pick<LocalSongOrderItem, 'title' | 'order'>[]) => void
  onJumpToSlideGroup?: (slideGroup: LocalSlideGroup) => void
}

const menuTitles: Record<PresentationEditorMenu, string> = {
  slides: 'Diasor',
  songOrder: 'Énekrend',
  slidegroup: 'Dia csoport',
  notes: 'Jegyzetek'
}

export default function SideBar({
  initialMenu,
  songOrder,
  onSaveAsTemplate = noop,
  onSongOrderChange = noop,
  onJumpToSlideGroup = noop
}: SlideBarProps) {
  const [openMenu, setOpenMenu] = useState<PresentationEditorMenu>(initialMenu)
  const [presentationConfigModalOpen, setPresentationConfigModalOpen] = useState(false)

  const [selectedGroup, setSelectedGroup] = useState<LocalSlideGroup | null>(null)

  const [createSlideGroup] = useCreateSlideGroupMutation({
    refetchQueries: [{ query: SongOrderDocument, variables: { where: { id: songOrder?.id } } }]
  })
  const [updateSlideGroup] = useUpdateSlideGroupMutation({
    refetchQueries: [{ query: SongOrderDocument, variables: { where: { id: songOrder?.id } } }]
  })
  const [deleteSlideGroup] = useDeleteSlideGroupMutation({
    refetchQueries: [{ query: SongOrderDocument, variables: { where: { id: songOrder?.id } } }]
  })
  const [insertSlideGroup] = useInsertSlideGroupMutation({
    refetchQueries: [{ query: SongOrderDocument, variables: { where: { id: songOrder?.id } } }]
  })
  const [updateNote] = useUpdateSongOrderNoteMutation({
    refetchQueries: [{ query: SongOrderDocument, variables: { where: { id: songOrder?.id } } }]
  })
  const [updatePresentationConfig] = useUpdatePresentationConfigMutation({
    refetchQueries: [{ query: SongOrderDocument, variables: { where: { id: songOrder?.id } } }]
  })

  const [getSongWithAllLyrics, { data }] = useSongWithAllLyricsLazyQuery()

  useEffect(() => {
    if (openMenu === 'slidegroup' && selectedGroup?.song) {
      getSongWithAllLyrics({ variables: { where: { id: selectedGroup.song.id } } })
    }
  }, [getSongWithAllLyrics, openMenu, selectedGroup])

  const slideGroups = songOrder?.groups

  const backButtonLabel = !!songOrder && openMenu === 'songOrder' ? 'Diasor' : 'Énekrend'

  return (
    <>
      <SideBarHeader
        className="p-2 h-1/6"
        showBackButton={backButtonLabel}
        showNoteButton={!!songOrder}
        showSettingsButton={!!songOrder}
        onBackClick={() => {
          if (openMenu === 'songOrder' || openMenu === 'slidegroup') {
            setOpenMenu('slides')
            return
          }
          setOpenMenu('songOrder')
        }}
        title={menuTitles[openMenu]}
        onNotesButtonClick={() => {
          setOpenMenu('notes')
        }}
        onEditButtonClick={() => setPresentationConfigModalOpen(true)}
      />

      <div className="h-5/6 overflow-y-auto">
        {openMenu === 'slides' && (
          <PresentationEditor
            slideGroups={slideGroups}
            className="p-2"
            onEditSlideGroup={(group) => {
              setSelectedGroup(group)
              setOpenMenu('slidegroup')
            }}
            onDeleteSlideGroup={(id) => {
              deleteSlideGroup({ variables: { where: { id } } })
            }}
            onJumpToSlideGroup={(slideGroup) => {
              onJumpToSlideGroup(slideGroup)
            }}
          />
        )}
        {openMenu === 'songOrder' && (
          <SongOrderEditor
            songOrder={songOrder}
            className="p-2"
            onSubmit={(items, title) => {
              onSongOrderChange(items, title)
            }}
            onSaveAsTemplate={onSaveAsTemplate}
          />
        )}
        {openMenu === 'notes' && (
          <SongOrderNoteEditor
            note={songOrder?.note || undefined}
            onSubmit={(note) => {
              updateNote({
                variables: {
                  where: { id: songOrder?.id },
                  note: note || null
                }
              })
            }}
          />
        )}

        {openMenu === 'slidegroup' && selectedGroup && selectedGroup?.song && (
          <VersePicker
            className="p-2"
            lyrics={data?.song?.lyrics}
            songId={selectedGroup.song.id}
            onSave={async (text) => {
              const slideTexts = text.split(/\r?\n\r?\n/)

              if (!selectedGroup?.id) {
                const createSlideGroupResult = await createSlideGroup({
                  variables: {
                    data: {
                      songOrder: { connect: { id: songOrder?.id } },
                      slides: {
                        createMany: {
                          data: slideTexts.map((text, i) => ({
                            content: text,
                            order: i,
                            title: selectedGroup?.title
                          }))
                        }
                      },
                      order: selectedGroup.order,
                      song: { connect: { id: selectedGroup.song?.id || undefined } },
                      title: selectedGroup?.title
                    }
                  }
                })
                setSelectedGroup(createSlideGroupResult.data?.createOneSlideGroup || null)
                onJumpToSlideGroup(selectedGroup)
                return
              }

              if (slideTexts.length < selectedGroup.slides.length) {
                const slidesToDelete = selectedGroup.slides.slice(
                  selectedGroup.slides.length - slideTexts.length
                )
                await updateSlideGroup({
                  variables: {
                    where: { id: selectedGroup.id },
                    data: {
                      slides: {
                        delete: slidesToDelete.map((slide) => ({ id: slide.id }))
                      }
                    }
                  }
                })
              }

              const updateSlideGroupResult = await updateSlideGroup({
                variables: {
                  where: { id: selectedGroup.id },
                  data: {
                    song: { connect: { id: selectedGroup.song?.id || undefined } },
                    slides: {
                      upsert: (slideTexts || []).map((slideText, i) => ({
                        where: { id: selectedGroup.slides[i]?.id ?? 'undefined' },
                        create: {
                          order: i,
                          content: slideText,
                          title: selectedGroup.song?.name || selectedGroup.title
                        },
                        update: {
                          order: { set: i },
                          content: { set: slideText },
                          title: { set: selectedGroup.song?.name || 'Új dia' }
                        }
                      }))
                    }
                  }
                }
              })
              setSelectedGroup(updateSlideGroupResult.data?.updateOneSlideGroup || null)
              onJumpToSlideGroup(selectedGroup)
            }}
          />
        )}

        {openMenu === 'slidegroup' && selectedGroup && !selectedGroup?.song && (
          <ManualTextEditor
            onSubmit={async (data) => {
              const slideTexts = data.text.split(/\r?\n\r?\n/)
              if (!selectedGroup.id) {
                const createSlideGroupResult = await insertSlideGroup({
                  variables: {
                    data: {
                      songOrder: { connect: { id: songOrder?.id } },
                      slides: {
                        createMany: {
                          data: slideTexts.map((text, i) => ({
                            content: text,
                            order: i,
                            title: selectedGroup?.title
                          }))
                        }
                      },
                      order: selectedGroup.order,
                      title: data.title || selectedGroup?.title
                    },
                    whereSongOrder: { id: songOrder?.id }
                  }
                })
                setSelectedGroup(createSlideGroupResult.data?.insertSlideGroup || null)
                onJumpToSlideGroup(selectedGroup)
                return
              }

              const updateSlideGroupResult = await updateSlideGroup({
                variables: {
                  where: { id: selectedGroup.id },
                  data: {
                    title: { set: data.title },
                    slides: {
                      upsert: (slideTexts || []).map((slideText, i) => ({
                        where: { id: selectedGroup.slides[i]?.id ?? 'undefined' },
                        create: {
                          order: i,
                          content: slideText,
                          title: selectedGroup.title
                        },
                        update: {
                          order: { set: i },
                          content: { set: slideText },
                          title: { set: selectedGroup.title || 'Új dia' }
                        }
                      }))
                    }
                  }
                }
              })
              setSelectedGroup(updateSlideGroupResult.data?.updateOneSlideGroup || null)
              onJumpToSlideGroup(selectedGroup)
            }}
            slideGroup={selectedGroup}
          />
        )}
      </div>
      <Modal
        isOpen={presentationConfigModalOpen && !!songOrder?.id}
        onClose={() => setPresentationConfigModalOpen(false)}
      >
        <PresentationConfig
          onSubmit={(config) =>
            updatePresentationConfig({ variables: { where: { id: songOrder?.id }, config } })
          }
          config={songOrder?.presentationConfig as PresentationConfiguration}
        />
      </Modal>
    </>
  )
}
