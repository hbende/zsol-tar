import React from 'react'

import clsx from 'clsx'
import { SongOrderWithGroupsFragment } from 'generated/apollo-components'

import SlideGroup from '../SlideGroup'
import { mockSlideGroup } from '../SlideGroup/mocks'

type SlidesProps = {
  className?: string
  songOrder?: SongOrderWithGroupsFragment
}

export default function Slides({ className, songOrder }: SlidesProps) {
  return (
    <div className={clsx('space-y-8 divide-indigo-500 divide-y-2 flex flex-col', className)}>
      <SlideGroup group={mockSlideGroup} />
      {songOrder?.groups.map((group) => (
        <SlideGroup key={group.id} group={group} />
      ))}
    </div>
  )
}
