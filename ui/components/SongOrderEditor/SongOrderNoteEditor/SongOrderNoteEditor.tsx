import React from 'react'
import { useForm } from 'react-hook-form'

import Button from 'components/ui/Button'
import HelperText from 'components/ui/HelperText'
import TextArea from 'components/ui/TextArea'

type SongOrderNoteEditorProps = {
  note?: string
  onSubmit?: (newNote?: string) => void
}

type FormData = {
  note?: string
}

export default function SongOrderNoteEditor({
  note,
  onSubmit = () => {}
}: SongOrderNoteEditorProps) {
  const { handleSubmit, register } = useForm<FormData>({ defaultValues: { note } })

  const onSubmitCallback = handleSubmit(async (data) => {
    onSubmit(data.note)
  })

  return (
    <form className="space-y-2 p-2" onSubmit={onSubmitCallback}>
      <HelperText>Írj bármilyen megjegyzést az énekrendhez</HelperText>
      <TextArea {...register('note')} />
      <Button color="indigo" type="submit">
        Mentés
      </Button>
    </form>
  )
}
