import React from 'react'
import dynamic from 'next/dynamic'

const { H3, Header, Slide } = {
  H3: dynamic(() => import('@gregcello/revealjs-react').then((mod) => mod.H3), { ssr: false }),
  Header: dynamic(() => import('@gregcello/revealjs-react').then((mod) => mod.Header), {
    ssr: false
  }),
  Slide: dynamic(() => import('@gregcello/revealjs-react').then((mod) => mod.Slide), {
    ssr: false
  })
}

import { PresentationConfiguration } from 'components/PresentationConfig/PresentationConfig'
import { SongOrderWithGroupsFragment } from 'generated/apollo-components'

type SlidesProps = {
  songOrder?: SongOrderWithGroupsFragment | null
}

export default function Slides({ songOrder }: SlidesProps) {
  const shouldAddEmptySlidesBetweenGroups = (
    songOrder?.presentationConfig as PresentationConfiguration
  )?.emptySlides

  return (
    <>
      {songOrder?.groups.map((slideGroup, i) => {
        return (
          <React.Fragment key={slideGroup.id}>
            {slideGroup.slides.map((slide) => (
              <Slide key={slide.id}>
                {songOrder.presentationConfig.showTitleOfSlides && slideGroup.title && (
                  <Header className="text-left">{slideGroup.title}</Header>
                )}
                {slide.content
                  .replace(/\r/g, '')
                  .split(/\n/)
                  .map((line, i) => (
                    <H3 key={`${slideGroup.id}-${i}`}>{line}</H3>
                  ))}
              </Slide>
            ))}
            {shouldAddEmptySlidesBetweenGroups &&
              !!slideGroup.slides.length &&
              !!songOrder.groups[i + 1]?.slides.length && <Slide>{null}</Slide>}
          </React.Fragment>
        )
      })}
    </>
  )
}
