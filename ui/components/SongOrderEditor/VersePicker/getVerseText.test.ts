import { mockSong } from 'components/SongDetails/mocks'
import { VerseFragment, VerseType } from 'generated/apollo-components'

import { getVerseText } from './VersePicker'

const firstVerse: VerseFragment & { order: number } = {
  text: 'First verse',
  order: 0,
  id: '0',
  type: VerseType.Verse
}
const firstRefrain: VerseFragment & { order: number } = {
  text: 'First refrain',
  order: 0,
  id: '01',
  type: VerseType.Refrain
}

describe('getVerseText', () => {
  it('should retrieve a specific verse from a lyrics', () => {
    expect(
      getVerseText(
        {
          ...mockSong.lyrics[0],
          verses: [firstVerse, firstRefrain]
        },
        { order: firstVerse.order, type: VerseType.Verse }
      )
    ).toBe(firstVerse.text)
  })

  it('should retrieve a specific refrain from a lyrics', () => {
    expect(
      getVerseText(
        {
          ...mockSong.lyrics[0],
          verses: [firstVerse, firstRefrain]
        },
        { order: firstRefrain.order, type: VerseType.Refrain }
      )
    ).toBe(firstRefrain.text)
  })

  it('should return undefined string for nonexistent verse', () => {
    expect(
      getVerseText(
        {
          ...mockSong.lyrics[0],
          verses: [firstRefrain, firstVerse]
        },
        { order: 23, type: VerseType.Chorus }
      )
    ).toBeUndefined
  })
})
