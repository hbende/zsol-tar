import React, { useState } from 'react'
import { TiArrowLeft, TiArrowRight } from 'react-icons/ti'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import HelperText from 'components/ui/HelperText'
import Link from 'components/ui/Link'
import TextArea from 'components/ui/TextArea'
import Tooltip from 'components/ui/Tooltip'
import { LyricsFragment } from 'generated/apollo-components'
import useArrayIndex from 'hooks/useArrayIndex'

import SongTextPreview from '../../SongDetails/SongTextPreview'
import VersePickerInput, { VerseOrderAndType } from './VersePickerInput'

type VersePickerProps = {
  className?: string
  lyrics?: LyricsFragment[]
  onSave?: (text: string) => void
  songId?: string
}

export function getVerseText(lyrics: LyricsFragment, verse: VerseOrderAndType) {
  return lyrics.verses.find(
    (part) => part.type === verse.type && (part.order === verse.order || !part.order)
  )?.text
}

export default function VersePicker({
  songId,
  className,
  lyrics,
  onSave = () => {}
}: VersePickerProps) {
  const [verses, setVerses] = useState<VerseOrderAndType[]>([])
  const [manualValue, setManualValue] = useState('')

  const { selectedItem, next, previous, index } = useArrayIndex(lyrics)

  if (!lyrics || !selectedItem) {
    if (!lyrics?.length && songId) {
      return (
        <>
          <HelperText>
            Sajnos egyetlen publikus szöveg sem elérhető az általad választott énekhez. -
            <Link href={`/song/${songId}/lyrics/new`}>Szöveg hozzáadása</Link>
          </HelperText>
        </>
      )
    }
    return null
  }

  const editedManually = Boolean(manualValue)

  const text = !editedManually
    ? (verses.map((verse) => getVerseText(selectedItem, verse)) as string[])
        .filter(Boolean)
        .join('\r\n\r\n')
    : manualValue

  return (
    <div className={clsx('space-y-4', className)}>
      <div className="flex">
        <VersePickerInput
          onChange={(verses) => setVerses(verses)}
          className="w-full"
          disabled={editedManually}
        />
        <Button color="indigo" onClick={() => onSave(text)}>
          Mentés
        </Button>
      </div>
      <div>
        <label>Előnézet</label>
        <TextArea
          className=""
          value={text}
          onChange={(event) => {
            setManualValue(event.target.value)
          }}
        />
        <HelperText>
          Ha kézzel szerkeszted ezt a mezőt, utána nem fogod tudni a fenti sorrend-választót
          alkalmazni.
        </HelperText>
        <HelperText>
          Két sortörés beillesztésével lehetséges több diára tördelni a szöveget.
        </HelperText>
      </div>
      {!editedManually && lyrics && (
        <div className="space-y-4">
          <div>
            {lyrics.length === 1
              ? 'Sajnos csak egy szöveg elérhető ehhez az énekhez'
              : `${index + 1}/${lyrics.length}`}
          </div>
          {lyrics.length > 1 && (
            <div className="flex gap-x-2">
              <Tooltip label="Előző szöveg">
                <Button color="gold" onClick={previous}>
                  <TiArrowLeft />
                </Button>
              </Tooltip>
              <Tooltip label="Következő szöveg">
                <Button color="gold" onClick={next}>
                  <TiArrowRight />
                </Button>
              </Tooltip>
            </div>
          )}
          <SongTextPreview lyrics={selectedItem} />
        </div>
      )}
    </div>
  )
}
