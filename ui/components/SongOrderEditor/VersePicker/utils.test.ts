import { VerseType } from 'generated/apollo-components'

import {
  getOrder,
  getTypeString,
  getVerseType,
  isVerseTypeAbbreviated,
  isVerseTypeLabel
} from './VersePickerInput'

describe('isVerseTypeAbbreviated', () => {
  it('should tell whether verse type is abbreviated in a string', () => {
    expect(isVerseTypeAbbreviated('b')).toBe(true)
    expect(isVerseTypeAbbreviated('v')).toBe(true)
    expect(isVerseTypeAbbreviated('c')).toBe(true)
    expect(isVerseTypeAbbreviated('r')).toBe(true)
  })
  it('should be false when the string is not an abbreviation', () => {
    expect(isVerseTypeAbbreviated('g')).toBe(false)
    expect(isVerseTypeAbbreviated('va')).toBe(false)
    expect(isVerseTypeAbbreviated('dfgc')).toBe(false)
    expect(isVerseTypeAbbreviated('d')).toBe(false)
  })
})

describe('isVerseTypeLabel', () => {
  it('should tell whether verse type is typed in a string in hungarian', () => {
    expect(isVerseTypeLabel('versszak')).toBe(true)
    expect(isVerseTypeLabel('refrén')).toBe(true)
  })
  it('should be false when the string is not an hungarian verse type', () => {
    expect(isVerseTypeLabel('g')).toBe(false)
    expect(isVerseTypeLabel('va')).toBe(false)
    expect(isVerseTypeLabel('dfgc')).toBe(false)
    expect(isVerseTypeLabel('d')).toBe(false)
    expect(isVerseTypeLabel('dasdasddg')).toBe(false)
    expect(isVerseTypeLabel('refrem')).toBe(false)
  })
})

describe('getVerseType', () => {
  it('should retrieve the correct verse type', () => {
    expect(getVerseType('versszak')).toBe(VerseType.Verse)
    expect(getVerseType('v')).toBe(VerseType.Verse)
    expect(getVerseType('refrén')).toBe(VerseType.Refrain)
    expect(getVerseType('r')).toBe(VerseType.Refrain)
    // rest is handled by typescript types
  })
})

describe('getOrder', () => {
  it('should retrieve the first number from a string', () => {
    expect(getOrder('asdasd0a')).toBe(0)
    expect(getOrder('asdasd123123')).toBe(123123)
    expect(getOrder('321asdasd123123')).toBe(321)
    expect(getOrder('versszak54123123')).toBe(54123123)
  })
  it('should retrieve the order when capital letter is used', () => {
    expect(getOrder('V1')).toBe(1)
    expect(getOrder('R2')).toBe(2)
    expect(getOrder('R2')).toBe(2)
  })
  it('should retrieve 0 when no number is specified', () => {
    expect(getOrder('R')).toBe(0)
  })
})

describe('getTypeString', () => {
  it('should retrieve the correct verse type from a reference string', () => {
    expect(getTypeString('versszak23')).toBe('versszak')
    expect(getTypeString('R12')).toBe('R')
    expect(getTypeString('af23R12')).toBe('af')
  })
  it('should return empty string when no character is provided before numbers', () => {
    expect(getTypeString('23')).toBe('')
  })
})
