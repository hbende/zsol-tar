import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './VersePickerInput'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'PresentationEditor/VersePicker'
}
export default meta

const template = storyTemplate(Component)

export const VersePicker = template({
  onChange: action('change')
})
