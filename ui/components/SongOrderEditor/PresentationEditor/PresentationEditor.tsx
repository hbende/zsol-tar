import React, { useEffect } from 'react'
import { NestedValue, useFieldArray, useForm } from 'react-hook-form'
import { TiPlus } from 'react-icons/ti'
import { useRouter } from 'next/router'

import Button from 'components/ui/Button'
import Tooltip from 'components/ui/Tooltip'
import {
  SlideFragment,
  SlideGroupFragment,
  useSwapSlideGroupsMutation
} from 'generated/apollo-components'
import { PartialBy } from 'util/utilTypes'

import SlideGroup from '../SlideGroup/SlideGroup'

export type LocalSlide = PartialBy<SlideFragment, 'id'>

export type LocalSlideGroup = PartialBy<
  Omit<SlideGroupFragment, 'slides'> & { slides: LocalSlide[] },
  'id'
>

type PresentationEditorProps = {
  slideGroups?: LocalSlideGroup[]
  className?: string
  onJumpToSlideGroup?: (slideGroup: LocalSlideGroup) => void
  onEditSlideGroup?: (slideGroup: LocalSlideGroup) => void
  onDeleteSlideGroup?: (groupId: string) => void
}

export default function PresentationEditor({
  slideGroups = [],
  className,
  onEditSlideGroup = () => {},
  onJumpToSlideGroup = () => {},
  onDeleteSlideGroup = () => {}
}: PresentationEditorProps) {
  const router = useRouter()
  const { control, setValue, getValues } = useForm<{
    slideGroups: NestedValue<LocalSlideGroup[]>
  }>({
    defaultValues: {
      slideGroups
    }
  })
  useEffect(() => {
    // this is rather hacky, but couldn't find documentation on why the useFieldArray doesn't get updated with a new defaultValue
    const values = getValues()
    if (values.slideGroups.length === 0 && !!slideGroups) {
      setValue('slideGroups', slideGroups)
    }
  }, [slideGroups, setValue, getValues])
  const { fields, remove, prepend, insert, move } = useFieldArray({
    control,
    name: `slideGroups`,
    keyName: 'formKey'
  })
  const [swapSlideGroups] = useSwapSlideGroupsMutation({
    refetchQueries: ['songOrder']
  })
  return (
    <div className={className}>
      <Tooltip label="Beszúrás">
        <Button
          color="indigo"
          className="relative"
          onClick={() => {
            prepend({ slides: [{ order: 0, title: 'Új dia', content: '' }] })
          }}
        >
          <TiPlus />
          <span className="sr-only">Új dia-blokk beszúrása legelőre</span>
        </Button>
      </Tooltip>
      <div className="space-y-6">
        {fields.map((group, index) => {
          return (
            <div key={group.id || group.formKey}>
              <SlideGroup
                key={index}
                group={group}
                onDelete={() => {
                  remove(index)
                  if (group.id) {
                    onDeleteSlideGroup(group.id)
                    return
                  }
                }}
                onEdit={() => onEditSlideGroup({ ...group, order: index })}
                onGoToSong={() => {
                  router.push(`/song/${group.song?.id}`)
                }}
                onJump={() => onJumpToSlideGroup(group)}
                moveDownButtonProps={{
                  disabled: index === fields.length - 1,
                  onClick: () => {
                    if (index === fields.length - 1) {
                      return
                    }
                    move(index, index + 1)
                    if (fields[index].id && fields[index + 1].id) {
                      swapSlideGroups({
                        variables: {
                          groupId1: fields[index].id as string,
                          groupId2: fields[index + 1].id as string
                        }
                      })
                    }
                  }
                }}
                moveUpButtonProps={{
                  disabled: index === 0,
                  onClick: () => {
                    if (index === 0) {
                      return
                    }
                    move(index, index - 1)
                    if (fields[index].id && fields[index - 1].id) {
                      swapSlideGroups({
                        variables: {
                          groupId1: fields[index].id as string,
                          groupId2: fields[index - 1].id as string
                        }
                      })
                    }
                  }
                }}
              />
              <Tooltip label="Új beszúrása">
                <Button
                  color="indigo"
                  className="relative"
                  onClick={() => {
                    insert(index + 1, { slides: [{ order: 0, title: 'Új dia', content: '' }] })
                  }}
                >
                  <TiPlus />
                  <span className="sr-only">Új dia-blokk hozzáadása</span>
                </Button>
              </Tooltip>
            </div>
          )
        })}
      </div>
    </div>
  )
}
