import { Meta } from '@storybook/react'

import Component from './PresentationConfig'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'PresentationConfig'
}
export default meta

const template = storyTemplate(Component)

export const PresentationConfig = template({ config: { theme: 'black' } })
