import React from 'react'
import { Controller, useForm } from 'react-hook-form'

import type { RevealJSProps } from '@gregcello/revealjs-react/dist/index'
import { MightBeRevealPlugin } from '@gregcello/revealjs-react/types/reveal.js'
import Button from 'components/ui/Button'
import CheckBox from 'components/ui/CheckBox'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import RadioGroup from 'components/ui/RadioGroup'

import { RevealJsTheme } from 'components/SongOrderEditor/RevealJsStyles/RevealJsStyles'

type AkkordoPresentationConfiguration = {
  theme: RevealJsTheme
  emptySlides?: boolean
  showTitleOfSlides?: boolean
}

export type PresentationConfiguration = Pick<
  RevealJSProps<MightBeRevealPlugin[]>,
  | 'controlsLayout'
  | 'controlsBackArrows'
  | 'progress'
  | 'slideNumber'
  | 'transition'
  | 'transitionSpeed'
  | 'margin'
  | 'minScale'
  | 'maxScale'
> &
  AkkordoPresentationConfiguration

const defaultConfig: PresentationConfiguration = {
  theme: 'black',
  controlsBackArrows: 'faded',
  controlsLayout: 'bottom-right',
  margin: 0.04,
  maxScale: 2.0,
  minScale: 0.4,
  progress: true,
  slideNumber: false,
  transition: 'slide',
  transitionSpeed: 'default'
}

type PresentationConfigProps = {
  config?: PresentationConfiguration
  onSubmit?: (config: PresentationConfiguration) => void
}

export default function PresentationConfig({
  config,
  onSubmit = () => {}
}: PresentationConfigProps) {
  const { register, handleSubmit, control } = useForm<PresentationConfiguration>({
    defaultValues: { ...defaultConfig, ...config }
  })

  const onSubmitCallback = handleSubmit((data) => {
    onSubmit(data as PresentationConfiguration)
  })

  return (
    <form className="space-y-4 space-x-4" onSubmit={onSubmitCallback}>
      <div className="flex space-x-4 items-start w-full justify-between p-6">
        <div className="space-y-2">
          <h3 className="text-xl">Lapozási beállítások</h3>
          <div>
            <Label>Lapozó nyilak elhelyezkedése</Label>
            <RadioGroup
              register={register('controlsLayout')}
              options={[
                { label: 'Alul', value: 'bottom-right' },
                { label: 'Középen', value: 'edges' }
              ]}
            />
          </div>
          <div>
            <Label>Hátra lapozás láthatósága</Label>
            <RadioGroup
              register={register('controlsBackArrows')}
              options={[
                { label: 'Halvány', value: 'faded' },
                { label: 'Elrejtve', value: 'hidden' },
                { label: 'Mutat', value: 'visible' }
              ]}
            />
          </div>
          <div>
            <Label>Állapotsáv</Label>
            <CheckBox className="inline-block" {...register('progress')} />
          </div>
          <div>
            <Label>Oldalszám mutatása</Label>
            <CheckBox className="inline-block" {...register('slideNumber')} />
          </div>
        </div>
        <div className="space-y-2">
          <h3 className="text-xl">Animálási beállítások</h3>
          <div>
            <Label>Átmenet</Label>
            <RadioGroup
              register={register('transition')}
              // none/fade/slide/convex/concave/zoom
              options={[
                {
                  label: 'Csúszás',
                  value: 'slide'
                },
                { label: 'Átfedés', value: 'fade' },
                { label: 'Konvex', value: 'convex' },
                { label: 'Konkáv', value: 'concave' },
                { label: 'Zoom', value: 'Zoom' },
                { label: 'Kikapcsol', value: 'none' }
              ]}
            />
          </div>
          <div>
            <Label>Átmenet sebessége</Label>
            <RadioGroup
              register={register('transitionSpeed')}
              options={[
                {
                  label: 'Normál',
                  value: 'default'
                },
                { label: 'Gyors', value: 'fast' },
                { label: 'Lassú', value: 'slow' }
              ]}
            />
          </div>
        </div>

        <div className="space-y-2">
          <h3 className="text-xl">Méret beállítások</h3>
          <div>
            <Label>Margó</Label>
            <Input type="number" {...register('margin')} step="0.01" />
          </div>
          <div>
            <Label>Minimum méret</Label>
            <Input type="number" {...register('minScale')} step="0.1" />
          </div>
          <div>
            <Label>Maximum méret</Label>
            <Input type="number" {...register('maxScale')} step="0.1" />
          </div>
        </div>
      </div>

      <div className="space-y-2">
        <h3 className="text-xl">Dia beállítások</h3>
        <div>
          <Label>Üres dia beillesztése minden énekrend elem közé</Label>
          <Controller
            name="emptySlides"
            control={control}
            render={({ field: { value, ...field } }) => <CheckBox {...field} checked={value} />}
          />
        </div>
        <div>
          <Label>Dia címének megjelenítése (bal felső sarokban)</Label>
          <Controller
            name="showTitleOfSlides"
            control={control}
            render={({ field: { value, ...field } }) => <CheckBox {...field} checked={value} />}
          />
        </div>
      </div>

      <div className="flex w-full justify-center">
        <Button type="submit" color="indigo">
          Mentés
        </Button>
      </div>
    </form>
  )
}
