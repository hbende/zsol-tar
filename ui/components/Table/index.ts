export { perPageOptions } from './constants'
export { default } from './Table'

import type { PageSize } from './constants'
export type { PageSize }
