import React, { useCallback } from 'react'
import { Column, Row } from 'react-table'
import { useRouter } from 'next/router'

import Table, { FetchParameters } from 'components/Table/Table'
import Tag from 'components/Tag'
import Button from 'components/ui/Button'
import Link from 'components/ui/Link'
import { SongOrderMetaFragment, useSearchSongOrdersQuery } from 'generated/apollo-components'
import usePageSize from 'hooks/usePageSize'
import { transformToPrismaSort } from 'util/sort'

import { getDateColumn } from '../util'

export default function SongOrderTable() {
  const router = useRouter()
  const [pageSize, setPageSize] = usePageSize()
  const columns = React.useMemo<Column<SongOrderMetaFragment>[]>(
    () => [
      {
        Header: 'Cím',
        accessor: 'title'
      },
      getDateColumn<SongOrderMetaFragment>({
        Header: 'Utoljára szerkesztve',
        accessor: 'updatedAt'
      }),
      {
        Header: 'Vetítés',
        Cell: function PresentationButtonCell({ row }: { row: Row<SongOrderMetaFragment> }) {
          return (
            <div>
              <Link
                href={`/presentation/${row.original.id}`}
                passHref={false}
                onClick={(e) => e.stopPropagation()}
              >
                <Button color="indigo" className="">
                  Vetítés
                </Button>
              </Link>
            </div>
          )
        }
      }
    ],
    []
  )

  const { loading, data, refetch } = useSearchSongOrdersQuery({
    variables: { take: 10, skip: 0 }
  })

  const refetchData = useCallback(
    ({ pageSize, pageIndex, sortBy }: FetchParameters<SongOrderMetaFragment>) => {
      refetch({
        skip: pageSize * pageIndex,
        take: pageSize,
        orderBy: sortBy.length
          ? sortBy.reduce((sorts, curr) => {
              return { ...sorts, [curr.id]: transformToPrismaSort(curr) }
            }, {})
          : null
      })
    },
    [refetch]
  )

  return (
    <Table
      columns={columns}
      data={data?.userSongOrders || []}
      fetchData={refetchData}
      loading={loading}
      pageCount={Math.ceil((data?.userSongOrdersCount || 1) / pageSize)}
      onRowClick={(row) => {
        router.push(`/songorder/${row.original.id}`)
      }}
      pageSize={pageSize}
      setPageSize={setPageSize}
      dataCount={data?.userSongOrdersCount}
    />
  )
}
