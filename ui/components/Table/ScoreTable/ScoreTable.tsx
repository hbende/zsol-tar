import React, { useCallback } from 'react'
import { Column } from 'react-table'

import Table, { FetchParameters } from 'components/Table/Table'
import Link from 'components/ui/Link'
import UserIcon from 'components/UserIcon'
import {
  ScoreDetailsFragment,
  useChangeScorePublicityMutation,
  useSearchScoresQuery
} from 'generated/apollo-components'
import usePageSize from 'hooks/usePageSize'
import { transformToPrismaSort } from 'util/sort'

import { getDateColumn, getPublicityColumns } from '../util'

export default function ScoreTable() {
  const [pageSize, setPageSize] = usePageSize()
  const [changeScorePublicity] = useChangeScorePublicityMutation()

  const columns = React.useMemo<Column<ScoreDetailsFragment>[]>(
    () => [
      {
        Header: 'Ének',
        accessor: 'song',
        Cell: ({ row }) => {
          return (
            <Link
              href={`song/${row.original.song.id}`}
              onClick={(e) => {
                e.stopPropagation()
              }}
            >
              {(row.values as ScoreDetailsFragment).song.name}
            </Link>
          )
        }
      },
      {
        Header: 'Felhasználó',
        accessor: 'user',
        Cell: ({ row }) => {
          return (
            <div>
              <UserIcon user={row.original.user} />
            </div>
          )
        }
      },
      {
        Header: 'Leírás',
        accessor: 'description'
      },
      getDateColumn<ScoreDetailsFragment>({ Header: 'Hozzáadva', accessor: 'createdAt' }),
      ...getPublicityColumns<ScoreDetailsFragment>(changeScorePublicity)
    ],
    [changeScorePublicity]
  )

  const { loading, data, refetch } = useSearchScoresQuery({
    variables: { take: 10, skip: 0 }
  })

  const refetchData = useCallback(
    ({ pageSize, pageIndex, sortBy }: FetchParameters<ScoreDetailsFragment>) => {
      refetch({
        skip: pageSize * pageIndex,
        take: pageSize,
        orderBy: sortBy.length
          ? sortBy.reduce((sorts, curr) => {
              if (curr.id === 'song') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              if (curr.id === 'user') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              return { ...sorts, [curr.id]: transformToPrismaSort(curr) }
            }, {})
          : null
      })
    },
    [refetch]
  )

  return (
    <>
      <Table
        columns={columns}
        data={data?.scores || []}
        fetchData={refetchData}
        loading={loading}
        pageCount={Math.ceil((data?.scoresCount || 1) / pageSize)}
        pageSize={pageSize}
        setPageSize={setPageSize}
        dataCount={data?.scoresCount}
        onRowClick={(row) => window.open(row.original.url)}
      />
    </>
  )
}
