import React from 'react'
import { TiDelete } from 'react-icons/ti'
import { Column } from 'react-table'

import Button from 'components/ui/Button'
import CheckBox from 'components/ui/CheckBox'
import { Publicity, PublishStatus } from 'generated/apollo-components'
import { transformDate } from 'util/formatDate'
import { deserializeToken } from 'util/jwt'

type PublicityChangerMutationVariables = {
  where: { id: string }
  publicity: Publicity
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function getPublicityColumns<T extends { publishStatus?: PublishStatus | null; id: string }>(
  changePublicity: (params: { variables: PublicityChangerMutationVariables }) => void
): Column<T>[] {
  const { isAdmin } = deserializeToken()

  if (!isAdmin) {
    return []
  }

  return [
    {
      Header: 'Nyilvános',
      accessor: 'publishStatus',
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      Cell: ({ row }) => {
        const publishStatus = row.original.publishStatus
        return (
          <div>
            <CheckBox
              onClick={(e) => e.stopPropagation()}
              checked={publishStatus === 'Published'}
              onChange={() => {
                changePublicity({
                  variables: {
                    where: { id: row.original.id },
                    publicity: Publicity.Published
                  }
                })
              }}
              disabled={publishStatus === 'Published'}
            />
          </div>
        )
      }
    },
    {
      Header: 'Elutasítás',
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      Cell: ({ row }) => {
        const publishStatus = row.original.publishStatus
        return (
          <div>
            <Button
              color="red"
              size="small"
              onClick={(e) => {
                e.stopPropagation()
                changePublicity({
                  variables: {
                    where: { id: row.original.id },
                    publicity: Publicity.Rejected
                  }
                })
              }}
              disabled={publishStatus === Publicity.Rejected}
            >
              <TiDelete />
            </Button>
          </div>
        )
      }
    }
  ] as Column<T>[]
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function getDateColumn<T extends object = {}>(column: Column<T>): Column<T> {
  return {
    ...column,
    Cell: ({ value }: { value: any }) => transformDate(value)
  }
}
