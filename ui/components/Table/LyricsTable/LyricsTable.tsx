import React, { useCallback } from 'react'
import { Column } from 'react-table'
import router from 'next/router'

import Table, { FetchParameters } from 'components/Table/Table'
import Link from 'components/ui/Link'
import UserIcon from 'components/UserIcon'
import {
  LyricsMetaFragment,
  useChangeLyricsPublicityMutation,
  useSearchLyricsQuery
} from 'generated/apollo-components'
import usePageSize from 'hooks/usePageSize'
import { transformToPrismaSort } from 'util/sort'

import { getDateColumn, getPublicityColumns } from '../util'

export default function LyricsTable() {
  const [pageSize, setPageSize] = usePageSize()
  const [changeLyircsPublicity] = useChangeLyricsPublicityMutation()
  const columns = React.useMemo<Column<LyricsMetaFragment>[]>(
    () => [
      {
        Header: 'Ének',
        accessor: 'song',
        Cell: ({ row }) => {
          return (
            <Link
              href={`/song/${row.original.song?.id}`}
              onClick={(e) => {
                e.stopPropagation()
              }}
            >
              {row.original.song?.name}
            </Link>
          )
        }
      },
      {
        Header: 'Felhasználó',
        accessor: 'creator',
        Cell: ({ row }) => {
          return (
            <div>
              <UserIcon user={row.original.creator} />
            </div>
          )
        }
      },
      getDateColumn<LyricsMetaFragment>({ Header: 'Hozzáadva', accessor: 'createdAt' }),
      ...getPublicityColumns<LyricsMetaFragment>(changeLyircsPublicity)
    ],
    [changeLyircsPublicity]
  )

  const { loading, data, refetch } = useSearchLyricsQuery({
    variables: { take: 10, skip: 0 }
  })

  const refetchData = useCallback(
    ({ pageSize, pageIndex, sortBy }: FetchParameters<LyricsMetaFragment>) => {
      refetch({
        skip: pageSize * pageIndex,
        take: pageSize,
        orderBy: sortBy.length
          ? sortBy.reduce((sorts, curr) => {
              if (curr.id === 'song') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              if (curr.id === 'creator') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              return { ...sorts, [curr.id]: transformToPrismaSort(curr) }
            }, {})
          : null
      })
    },
    [refetch]
  )

  return (
    <>
      <Table
        columns={columns}
        data={data?.searchLyrics || []}
        fetchData={refetchData}
        loading={loading}
        pageCount={Math.ceil((data?.songlyricsCount || 1) / pageSize)}
        pageSize={pageSize}
        setPageSize={setPageSize}
        onRowClick={(row) => {
          router.push(`/song/${row.original.song?.id}/lyrics/${row.original.id}`)
        }}
        dataCount={data?.songlyricsCount}
      />
    </>
  )
}
