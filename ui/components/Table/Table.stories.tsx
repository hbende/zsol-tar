import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import Component from './Table'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'Table'
}
export default meta

const template = storyTemplate(Component)

export const Page = template<{ id: string; name: string }>({
  columns: [
    { Header: 'Id', accessor: 'id' },
    { Header: 'name', accessor: 'name' }
  ],
  pageSize: 10,
  setPageSize: () => action('setPageSize'),
  onRowClick: action('rowClick'),
  data: [
    { id: '1', name: '1st' },
    { id: '2', name: '2nd' }
  ],
  fetchData: action('fetch'),
  loading: false,
  pageCount: 5
})
