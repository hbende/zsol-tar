export type PageSize = 10 | 20 | 30

export const perPageOptions: PageSize[] = [10, 20, 30]
