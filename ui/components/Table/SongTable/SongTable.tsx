import React, { useCallback } from 'react'
import { TiDelete } from 'react-icons/ti'
import { Column } from 'react-table'
import { useRouter } from 'next/router'

import Table, { FetchParameters } from 'components/Table/Table'
import Tag from 'components/Tag'
import UserIcon from 'components/UserIcon'
import {
  SongFragment,
  useChangeSongPublicityMutation,
  useSearchSongsQuery
} from 'generated/apollo-components'
import usePageSize from 'hooks/usePageSize'
import { deserializeToken } from 'util/jwt'
import { transformToPrismaSort } from 'util/sort'

import { getDateColumn, getPublicityColumns } from '../util'
import Filters from './Filters'

export default function SongTable() {
  const router = useRouter()
  const { isLoggedIn } = deserializeToken()
  const [pageSize, setPageSize] = usePageSize()
  const [changeSongPublicity] = useChangeSongPublicityMutation()

  const columns = React.useMemo<Column<SongFragment>[]>(
    () => [
      {
        Header: 'Név',
        accessor: 'name'
      },
      { Header: 'Szerző', accessor: 'writer' },
      {
        Header: 'Cimkék',
        disableSortBy: true,
        accessor: 'userTags',
        Cell: function TagCell({ row }) {
          return (
            <div className="flex space-x-2">
              {(row.values as SongFragment).userTags.slice(0, 5).map((tag) => (
                <Tag className="" key={tag.id}>
                  {tag.text}
                </Tag>
              ))}
            </div>
          )
        }
      },
      {
        Header: 'Feltöltő',
        accessor: 'creator',
        Cell: function TagCell({ row }) {
          return <UserIcon user={row.original.creator} />
        }
      },
      getDateColumn<SongFragment>({ Header: 'Létrehozva', accessor: 'createdAt' }),
      ...getPublicityColumns<SongFragment>(changeSongPublicity)
    ],
    [changeSongPublicity]
  )

  const { loading, data, refetch, variables } = useSearchSongsQuery({
    variables: { take: 10, skip: 0 }
  })

  const refetchData = useCallback(
    ({ pageSize, pageIndex, sortBy }: FetchParameters<SongFragment>) => {
      refetch({
        skip: pageSize * pageIndex,
        take: pageSize,
        orderBy: sortBy.length
          ? sortBy.reduce((sorts, curr) => {
              if (curr.id === 'creator') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              return { ...sorts, [curr.id]: transformToPrismaSort(curr) }
            }, {})
          : null
      })
    },
    [refetch]
  )

  return (
    <>
      <Filters
        className="p-4"
        showOwnCheckbox={isLoggedIn}
        onSubmit={(filters) => {
          if (!variables) {
            return
          }
          refetch({
            ...variables,
            where: {
              name: { contains: filters.name || undefined },
              ...(filters.tagIds.length
                ? { labels: { some: { id: { in: filters.tagIds } } } }
                : {}),
              ...(filters.ownOnly ? { creatorId: { equals: deserializeToken().id } } : {})
            }
          })
        }}
      />
      <Table
        columns={columns}
        data={data?.publishedSongs || []}
        fetchData={refetchData}
        loading={loading}
        pageCount={Math.ceil((data?.publishedSongsCount || 1) / pageSize)}
        onRowClick={(row) => {
          router.push(`/song/${row.original.id}`)
        }}
        pageSize={pageSize}
        setPageSize={setPageSize}
        dataCount={data?.publishedSongsCount}
      />
    </>
  )
}
