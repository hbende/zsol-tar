import React, { useEffect } from 'react'
import { Controller, useForm } from 'react-hook-form'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import CheckBox from 'components/ui/CheckBox'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import TagSelect from 'components/ui/TagSelect'
import { useUserTagsLazyQuery } from 'generated/apollo-components'
import { noop } from 'lodash'

type Filters = {
  name?: string
  tagIds: string[]
  ownOnly: boolean
}

type FilterProps = {
  onSubmit?: (filters: Filters) => void
  className?: string
  showOwnCheckbox?: boolean
}

export default function Filters({ className, onSubmit = noop, showOwnCheckbox }: FilterProps) {
  const { register, handleSubmit, control } = useForm<Filters>({
    defaultValues: { tagIds: [], name: '', ownOnly: false }
  })

  const [searchTags, { loading, data }] = useUserTagsLazyQuery()

  const onSubmitCallback = handleSubmit(async (data) => {
    onSubmit({
      ...data,
      tagIds: data.tagIds.map(
        (option) => (option as unknown as { value: string; label: string }).value
      )
    })
  })

  useEffect(() => {
    searchTags({ variables: { take: 20 } })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <form className={clsx('space-y-4', className)} onSubmit={onSubmitCallback}>
      <div className="flex justify-evenly">
        <div className="w-5/12">
          <Label>Név</Label>
          <Input type="text" {...register('name')} />
        </div>
        <div className="w-5/12">
          <Label>Cimkék</Label>
          <Controller
            name="tagIds"
            control={control}
            render={({ field }) => {
              return (
                <TagSelect
                  {...field}
                  isSearchable
                  required
                  pageSize={20}
                  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                  // @ts-ignore
                  options={(data?.userTags || []).map((song) => ({
                    label: song.text,
                    value: song.id
                  }))}
                  placeholder="Keress a cimkéid között"
                  isLoading={loading}
                  defaultOptions={(data?.userTags || []).map((tag) => ({
                    label: tag.text,
                    value: tag.id
                  }))}
                  loadOptions={async (input: string) => {
                    return new Promise((resolve) => {
                      searchTags({
                        variables: input ? { where: { text: { contains: input } } } : {}
                      })
                      resolve(
                        (data?.userTags || []).map((tag) => ({
                          label: tag.text,
                          value: tag.id
                        }))
                      )
                    })
                  }}
                  className="h-10"
                />
              )
            }}
          />
          {showOwnCheckbox && (
            <>
              <Label>Csak saját dalok</Label>
              <CheckBox className="inline-block" {...register('ownOnly')} />
            </>
          )}
        </div>
      </div>
      <div className="flex justify-center items-center space-x-2">
        <Button type="submit" color="indigo" className="w-80">
          Szűrés
        </Button>
      </div>
    </form>
  )
}
