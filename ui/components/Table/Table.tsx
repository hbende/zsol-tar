import React from 'react'
import {
  FiArrowDown,
  FiArrowUp,
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight
} from 'react-icons/fi'
import { Column, Row, SortingRule, usePagination, useSortBy, useTable } from 'react-table'

import clsx from 'clsx'
import Input from 'components/ui/Input'
import { noop } from 'lodash'

import { PageSize, perPageOptions } from './constants'

export type FetchParameters<T, W = Record<string, unknown>> = {
  pageIndex: number
  pageSize: number
  sortBy: SortingRule<T>[]
  where?: W
}

const iconSize = clsx('h-5 w-5')
const iconColor = clsx('text-purple-500 hover:text-purple-700')

type TableProps<T extends Record<string, unknown>> = {
  columns: readonly Column<T>[]
  data: readonly T[]
  dataCount?: number | null
  fetchData: (paginationData: FetchParameters<T>) => void
  loading: boolean
  pageCount: number
  onRowClick?: (row: Row<T>) => void
  pageSize: PageSize
  setPageSize: (value: PageSize) => void
}

function Table<T extends Record<string, unknown>>({
  columns,
  data,
  dataCount,
  fetchData,
  loading,
  pageCount: controlledPageCount,
  onRowClick,
  setPageSize
}: TableProps<T>) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize: setInnerPageSize,
    state: { pageIndex, pageSize, sortBy }
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
      manualPagination: true,
      manualSortBy: true,
      pageCount: controlledPageCount
    },
    useSortBy,
    usePagination
  )

  // Listen for changes in pagination and use the state to fetch our new data
  React.useEffect(() => {
    fetchData({ pageIndex, pageSize, sortBy })
  }, [fetchData, pageIndex, pageSize, sortBy])

  return (
    <>
      <table {...getTableProps()} className={'w-full rounded-t-md'}>
        <thead className="rounded-t-md">
          {headerGroups.map((headerGroup) => (
            // eslint-disable-next-line react/jsx-key
            <tr
              {...headerGroup.getHeaderGroupProps()}
              className="text-md font-semibold tracking-wide text-left text-purple-800 bg-indigo-100 uppercase border-b border-gold-600"
            >
              {headerGroup.headers.map((column) => (
                // eslint-disable-next-line react/jsx-key
                <th
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  className="px-4 py-3 border border-purple-300"
                >
                  <div className="flex justify-between items-center">
                    {column.render('Header')}
                    <span className={iconColor}>
                      {column.isSorted ? column.isSortedDesc ? <FiArrowDown /> : <FiArrowUp /> : ''}
                    </span>
                  </div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()} className="bg-gold-50 text-purple-800">
          {page.map((row) => {
            prepareRow(row)
            return (
              // eslint-disable-next-line react/jsx-key
              <tr
                {...row.getRowProps()}
                onClick={() => onRowClick?.(row)}
                className={clsx(onRowClick && 'cursor-pointer')}
              >
                {row.cells.map((cell) => {
                  return (
                    // eslint-disable-next-line react/jsx-key
                    <td {...cell.getCellProps()} className="px-4 py-3 border border-purple-300">
                      {cell.render('Cell')}
                    </td>
                  )
                })}
              </tr>
            )
          })}
          <tr>
            {loading ? (
              <td className="px-4 py-3 border border-purple-300" colSpan={10000}>
                Betöltés...
              </td>
            ) : (
              <td className="px-4 py-3 border border-purple-300" colSpan={10000}>
                {dataCount === 0
                  ? 'Nem áll rendelkezésre adat'
                  : `${pageSize * pageIndex + 1} - ${pageSize * pageIndex + page.length} / ${
                      dataCount ? ` ${dataCount}` : ` ~${controlledPageCount * pageSize}`
                    }`}
              </td>
            )}
          </tr>
        </tbody>
      </table>

      <div className="text-purple-900 flex justify-between items-center h-16 px-2 bg-indigo-100 border border-purple-300 border-t-0">
        <div className="space-x-4">
          <button onClick={() => gotoPage(0)} className={iconColor} disabled={!canPreviousPage}>
            <FiChevronsLeft className={clsx(iconSize)} />
          </button>
          <button onClick={() => previousPage()} className={iconColor} disabled={!canPreviousPage}>
            <FiChevronLeft className={clsx(iconSize)} />
          </button>
          <button onClick={() => nextPage()} className={iconColor} disabled={!canNextPage}>
            <FiChevronRight className={clsx(iconSize)} />
          </button>
          <button
            onClick={() => gotoPage(pageCount - 1)}
            className={iconColor}
            disabled={!canNextPage}
          >
            <FiChevronsRight className={clsx(iconSize)} />
          </button>
        </div>

        <div className="flex space-x-4 items-center">
          <span>
            Oldal {pageIndex + 1}/{pageOptions.length}
          </span>
          <span>
            Ugrás
            <Input
              type="number"
              defaultValue={pageIndex + 1}
              onChange={(e) => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0
                gotoPage(page)
              }}
              className="w-16 m-2 inline-block"
            />
          </span>
          <div>
            <label>Sor laponként</label>
            <select
              value={pageSize}
              onChange={(e) => {
                setInnerPageSize(Number(e.target.value))
                setPageSize(Number(e.target.value) as PageSize)
              }}
              className="ml-2"
            >
              {perPageOptions.map((pageSize) => (
                <option key={pageSize} value={pageSize}>
                  {pageSize}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    </>
  )
}

export default Table
