import React, { useCallback } from 'react'
import { Column } from 'react-table'

import Table, { FetchParameters } from 'components/Table/Table'
import Link from 'components/ui/Link'
import UserIcon from 'components/UserIcon'
import {
  RecordingDetailsFragment,
  useChangeRecordingPublicityMutation,
  useSearchRecordingsQuery
} from 'generated/apollo-components'
import usePageSize from 'hooks/usePageSize'
import { transformToPrismaSort } from 'util/sort'

import { getDateColumn, getPublicityColumns } from '../util'

export default function ScoreTable() {
  const [pageSize, setPageSize] = usePageSize()
  const [changeRecordingPublicity] = useChangeRecordingPublicityMutation()
  const columns = React.useMemo<Column<RecordingDetailsFragment>[]>(
    () => [
      {
        Header: 'Ének',
        accessor: 'song',
        Cell: ({ row }) => {
          return (
            <Link
              href={`/song/${row.original.song.id}`}
              onClick={(e) => {
                e.stopPropagation()
              }}
            >
              {(row.values as RecordingDetailsFragment).song.name}
            </Link>
          )
        }
      },
      {
        Header: 'Felhasználó',
        accessor: 'user',
        Cell: ({ row }) => {
          return (
            <div>
              <UserIcon user={row.original.user} />
            </div>
          )
        }
      },
      {
        Header: 'Leírás',
        accessor: 'description'
      },
      getDateColumn<RecordingDetailsFragment>({ Header: 'Hozzáadva', accessor: 'createdAt' }),
      ...getPublicityColumns<RecordingDetailsFragment>(changeRecordingPublicity)
    ],
    [changeRecordingPublicity]
  )

  const { loading, data, refetch } = useSearchRecordingsQuery({
    variables: { take: 10, skip: 0 }
  })

  const refetchData = useCallback(
    ({ pageSize, pageIndex, sortBy }: FetchParameters<RecordingDetailsFragment>) => {
      refetch({
        skip: pageSize * pageIndex,
        take: pageSize,
        orderBy: sortBy.length
          ? sortBy.reduce((sorts, curr) => {
              if (curr.id === 'song') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              if (curr.id === 'user') {
                return { ...sorts, [curr.id]: { name: transformToPrismaSort(curr) } }
              }
              return { ...sorts, [curr.id]: transformToPrismaSort(curr) }
            }, {})
          : null
      })
    },
    [refetch]
  )

  return (
    <>
      <Table
        columns={columns}
        data={data?.recordings || []}
        fetchData={refetchData}
        loading={loading}
        pageCount={Math.ceil((data?.recordingsCount || 1) / pageSize)}
        pageSize={pageSize}
        setPageSize={setPageSize}
        dataCount={data?.recordingsCount}
        onRowClick={(row) => window.open(row.original.url)}
      />
    </>
  )
}
