import React from 'react'
import { useForm } from 'react-hook-form'
import Link from 'next/link'
import { useRouter } from 'next/router'

import Button from 'components/ui/Button'
import FormError from 'components/ui/FormError'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import { useLoginMutation } from 'generated/apollo-components'
import { loginUser } from 'util/auth'
import { MINIMUM_PASSWORD_LENGTH } from 'util/constants'

import styles from '../sharedStyles'

type FormData = {
  email: string
  password: string
}

export default function LoginForm() {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<FormData>()

  const router = useRouter()

  const [login, { error: apiError }] = useLoginMutation({
    onCompleted: (data) => {
      loginUser(data.login?.token)
      router.push('/song')
    }
  })

  const onSubmit = handleSubmit(async (data) => {
    login({
      variables: { password: data.password, emailOrName: data.email }
    })
  })

  return (
    <form onSubmit={onSubmit} className={styles.form}>
      <div className={styles.title}>Akkordo Belépés</div>

      <div className={styles.fields}>
        <div className={styles.field}>
          <Label>Email vagy felhasználónév</Label>
          <Input type="text" {...register('email', { required: true })} />
          <FormError>
            {errors.email?.type === 'required' &&
              'Azonosítsd magad az email címeddel vagy a felhasználó neveddel'}
          </FormError>
        </div>

        <div className={styles.field}>
          <Label>Jelszó</Label>
          <Input
            autoComplete="password"
            type="password"
            {...register('password', { required: true, minLength: MINIMUM_PASSWORD_LENGTH })}
          />
          <FormError>
            {errors.password?.type === 'required' && 'Kötelező jelszót is megadni a belépéshez'}
          </FormError>
          <FormError>
            {errors.password?.type === 'minLength' &&
              'A jelszavad ezen az oldalon ennél hosszabb lehet csak'}
          </FormError>
        </div>

        <div>{apiError && <FormError>{apiError.message}</FormError>}</div>

        <div className={styles.buttonContainer}>
          <Button color="indigo" className={styles.button} type="submit">
            Belépés
          </Button>
          <Link href="/forgot-password">
            <a className={styles.link}>Elfelejtetted a jelszavad?</a>
          </Link>
        </div>
      </div>
    </form>
  )
}
