import { Meta } from '@storybook/react'

import Component from './LoginForm'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'Authentication/LoginForm'
}
export default meta

const template = storyTemplate(Component)

export const LoginForm = template({})
