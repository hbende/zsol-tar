import React from 'react'
import { useForm } from 'react-hook-form'
import { useRouter } from 'next/router'

import Button from 'components/ui/Button'
import FormError from 'components/ui/FormError'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import Link from 'components/ui/Link'
import { useSignupMutation } from 'generated/apollo-components'
import { loginUser } from 'util/auth'
import { MINIMUM_PASSWORD_LENGTH } from 'util/constants'

import styles from '../sharedStyles'

type FormData = {
  email: string
  password: string
  passwordAgain: string
  userName: string
}

export default function RegisterForm() {
  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors }
  } = useForm<FormData>()

  const router = useRouter()

  const [signup, { error: apiError }] = useSignupMutation({
    onCompleted: (data) => {
      loginUser(data.signup?.token)
      router.push('/song')
    }
  })

  const onSubmit = handleSubmit(async (data) => {
    signup({
      variables: { email: data.email, password: data.password, userName: data.userName }
    })
  })

  return (
    <form onSubmit={onSubmit} className={styles.form}>
      <div className={styles.title}>Akkordo Regisztráció</div>

      <div className={styles.fields}>
        <div className={styles.field}>
          <Label>Email</Label>
          <Input type="email" {...register('email', { required: true })} />
          <FormError>
            {errors.email?.type === 'required' && 'Kötelező email címet megadnod'}
          </FormError>
        </div>

        <div className={styles.field}>
          <Label>Felhasználónév</Label>
          <Input type="text" {...register('userName', { required: true })} />
          <FormError>
            {errors.userName?.type === 'required' && 'Kötelező a felhasználónevet választanod'}
          </FormError>
        </div>

        <div className={styles.field}>
          <Label>Jelszó</Label>
          <Input
            type="password"
            {...register('password', { required: true, minLength: MINIMUM_PASSWORD_LENGTH })}
          />
          <FormError>
            {errors.password?.type === 'required' && 'Kötelező jelszót megadnod'}
          </FormError>
          <FormError>
            {errors.password?.type === 'minLength' &&
              `Legalább ${MINIMUM_PASSWORD_LENGTH} karakterből kell, hogy álljon a jelszavad`}
          </FormError>
        </div>

        <div className={styles.field}>
          <Label>Jelszó megerősítése</Label>
          <Input
            type="password"
            {...register('passwordAgain', {
              required: true,
              validate: (value) => value === getValues().password
            })}
          />
          <FormError>
            {errors.passwordAgain?.type === 'required' && 'Kötelező a jelszót megerősíteni'}
            {errors.passwordAgain?.type === 'validate' && 'A két jelszó mező nem egyezik'}
          </FormError>
        </div>

        <div>{apiError && <FormError>{apiError.message}</FormError>}</div>

        <div className={styles.buttonContainer}>
          <Button color="indigo" className={styles.button} type="submit">
            Regisztráció
          </Button>
          <Link href="/login">Van már fiókód?</Link>
        </div>
      </div>
    </form>
  )
}
