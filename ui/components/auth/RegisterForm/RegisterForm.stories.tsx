import { Meta } from '@storybook/react'

import Component from './RegisterForm'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'Authentication/RegisterForm'
}
export default meta

const template = storyTemplate(Component)

export const RegisterForm = template({})
