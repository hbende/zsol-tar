import { Meta } from '@storybook/react'

import Component from './HelperText'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'UI/HelperText'
}
export default meta

const template = storyTemplate(Component)

export const HelperText = template({ children: 'Ahhoz hogy megtudj többet olvass el' })
export const Long = template({
  children:
    'Ahhoz hogy megtudj többet olvass el több alkalommal egymás után nagyon odafigyelve minden egyes apró és nagyobb részletre amikről megtudhatsz ezt-azt'
})
