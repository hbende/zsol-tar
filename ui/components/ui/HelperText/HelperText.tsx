import React from 'react'
import { TiInfo } from 'react-icons/ti'

import clsx from 'clsx'

type HelperTextProps = {
  className?: string
  children?: React.ReactNode
}

export default function HelperText({ className, children }: HelperTextProps) {
  return (
    <span className={clsx('text-indigo-600 flex text-sm italic', className)}>
      <div className="">
        <TiInfo className="block mr-2 w-4 h-4" />
      </div>
      {children}
    </span>
  )
}
