import React, { useRef } from 'react'
import { GroupBase, Props as VeryBadSelectProps } from 'react-select'
import RSAsyncSelect from 'react-select/async'
import { AsyncAdditionalProps } from 'react-select/dist/declarations/src/useAsync'

import clsx from 'clsx'

import styles from './AsyncSelect.module.css'

export type AsnycSelectProps<
  Option = unknown,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>
> = VeryBadSelectProps<Option, IsMulti, Group> &
  AsyncAdditionalProps<Option, Group> & { required?: boolean }

export default function AsyncSelect<
  Option = unknown,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>
>({ required, className, ...props }: AsnycSelectProps<Option, IsMulti, Group>) {
  const selectRef = useRef<HTMLElement>(null)

  return (
    <>
      <RSAsyncSelect
        as
        RSAsyncSelect
        cacheOptions
        styles={{
          ...props.styles,
          control: (base, state) => {
            return {
              ...base,
              borderColor: state.isFocused
                ? 'rgba(133, 64, 197, var(--tw-border-opacity))'
                : 'rgba(197, 170, 237, var(--tw-border-opacity))'
            }
          },
          input: (base) => ({
            ...base,
            '::placeholder': {
              '--tw-placeholder-opacity': 0.3,
              color: 'rgba(40, 22, 68, var(--tw-placeholder-opacity))'
            }
          })
        }}
        {...props}
        className={clsx(styles['async-select'], className)}
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        //  @ts-ignore
        ref={selectRef as unknown}
        noOptionsMessage={() => 'Nincs találat'}
        classNamePrefix="async-select" /* If changed styles break */
      />
      {required && (
        <input
          tabIndex={-1}
          autoComplete="off"
          style={{
            opacity: 0,
            width: '100%',
            height: 0,
            position: 'absolute'
          }}
          value={props.value ? 'some-value' : ''}
          onFocus={() => selectRef.current?.focus()}
          required={true}
        />
      )}
    </>
  )
}
