import React from 'react'

import clsx from 'clsx'

type TextAreaProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLTextAreaElement>,
  HTMLTextAreaElement
>

const TextArea = React.forwardRef<HTMLTextAreaElement, TextAreaProps>(function Input(
  { className, ...props },
  ref
) {
  return (
    <textarea
      className={clsx(
        'resize-none w-full min-h-[14rem] border-purple-300 focus:border-purple-600 placeholder-purple-900 placeholder-opacity-30',
        className,
        props.disabled && 'border-gray-200'
      )}
      {...props}
      ref={ref}
    />
  )
})

export default TextArea
