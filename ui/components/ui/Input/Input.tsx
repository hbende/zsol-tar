import React from 'react'

import clsx from 'clsx'

export type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>

const Input = React.forwardRef<HTMLInputElement, InputProps>(function Input(
  { className, ...props },
  ref
) {
  return (
    <input
      className={clsx(
        'block rounded w-full border-purple-300 focus:border-purple-600 placeholder-purple-900 placeholder-opacity-30',
        className,
        props.disabled && 'border-gray-200'
      )}
      {...props}
      ref={ref}
    />
  )
})

export default Input
