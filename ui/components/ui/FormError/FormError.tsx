import React from 'react'

import clsx from 'clsx'

type FormErrorProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLSpanElement>,
  HTMLSpanElement
>

export default function FormError({ className, ...props }: FormErrorProps) {
  return <span className={clsx('block text-red-400', className)} {...props} />
}
