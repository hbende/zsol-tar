import React from 'react'
import { UseFormRegisterReturn } from 'react-hook-form'

import clsx from 'clsx'

type RadioGroupProps = {
  options: { value: string; label: string }[]
  className?: string
  register: UseFormRegisterReturn
}

function RadioGroup({ className, register, options }: RadioGroupProps) {
  return (
    <div className={clsx(className)}>
      {options.map((option) => {
        return (
          <label key={option.value} className="flex space-x-2 items-center">
            <input
              type="radio"
              className={clsx(
                'block rounded-full w-3 h-3 border-purple-300 focus:border-purple-600 placeholder-purple-900 '
              )}
              {...register}
              value={option.value}
            />
            <span>{option.label}</span>
          </label>
        )
      })}
    </div>
  )
}

export default RadioGroup
