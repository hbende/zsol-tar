import React from 'react'
import NextLink, { LinkProps as NextLinkProps } from 'next/link'

import clsx from 'clsx'

type LinkProps = NextLinkProps & { className?: string } & {
  children: React.ReactNode
  onClick?: (e: React.MouseEvent) => void
  disabled?: boolean
}

export default function Link({ className, children, onClick, disabled, ...props }: LinkProps) {
  return (
    <NextLink {...props}>
      <a
        className={clsx(
          'text-sm text-indigo-500 hover:text-indigo-800 ml-2',
          disabled && 'text-gray-400 hover:text-gray-400  pointer-events-none',
          className
        )}
        onClick={onClick}
      >
        {children}
      </a>
    </NextLink>
  )
}
