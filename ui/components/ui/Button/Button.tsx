import React from 'react'

import clsx from 'clsx'

type ButtonColor = 'indigo' | 'purple' | 'gold' | 'red'
type ButtonSize = 'small' | 'normal' | 'large'

export type ButtonProps = React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
> & { color: ButtonColor; size?: ButtonSize }

const style = clsx('rounded text-white inline-block shadow-lg')

const buttonColorsClasses: Record<ButtonColor, string> = {
  indigo: clsx('bg-indigo-500 hover:bg-indigo-600 focus:bg-indigo-700'),
  gold: clsx('bg-gold-500 hover:bg-gold-600 focus:bg-gold-700'),
  purple: clsx('bg-purple-500 hover:bg-purple-600 focus:bg-purple-700'),
  red: clsx('bg-red-500 hover:bg-red-600 focus:bg-red-700')
}

const buttonSizeClasses: Record<ButtonSize, string> = {
  small: clsx('h-8 px-4'),
  normal: clsx('h-10 px-5'),
  large: clsx('h-12 px-6')
}

export default function Button({ className, size = 'normal', color, ...props }: ButtonProps) {
  return (
    <button
      {...props}
      className={clsx(
        style,
        buttonColorsClasses[color],
        buttonSizeClasses[size],
        props.disabled && 'bg-gray-500 hover:bg-gray-600 focus:bg-gray-700',
        className
      )}
    />
  )
}
