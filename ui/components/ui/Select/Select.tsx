import React, { useRef } from 'react'
import VeryBadSelect, { GroupBase, Props as VeryBadSelectProps } from 'react-select'

import clsx from 'clsx'

type CustomSelectProps<
  Option = unknown,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>
> = VeryBadSelectProps<Option, IsMulti, Group> & { required?: boolean }

export default function Select<
  Option = unknown,
  IsMulti extends boolean = boolean,
  Group extends GroupBase<Option> = GroupBase<Option>
>({ required, className, ...props }: CustomSelectProps<Option, IsMulti, Group>) {
  const selectRef = useRef<HTMLElement>(null)

  return (
    <>
      <VeryBadSelect
        {...props}
        className={clsx(className)}
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        ref={selectRef as unknown}
        noOptionsMessage={() => 'Nincs találat'}
      />
      {required && (
        <input
          tabIndex={-1}
          autoComplete="off"
          style={{
            opacity: 0,
            width: '100%',
            height: 0,
            position: 'absolute'
          }}
          value={props.value ? 'some-value' : ''}
          onFocus={() => selectRef.current?.focus()}
          required={true}
        />
      )}
    </>
  )
}
