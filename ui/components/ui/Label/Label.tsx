import React from 'react'

import clsx from 'clsx'

type LabelProps = React.DetailedHTMLProps<
  React.LabelHTMLAttributes<HTMLLabelElement>,
  HTMLLabelElement
>

export default function Label({ className, ...props }: LabelProps) {
  return <label className={clsx('block', className)} {...props} />
}
