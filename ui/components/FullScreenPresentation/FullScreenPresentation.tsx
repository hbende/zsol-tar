import React, { useEffect } from 'react'
const { RevealJS } = {
  RevealJS: dynamic(() => import('@gregcello/revealjs-react').then((mod) => mod.RevealJS), {
    ssr: false
  })
}

import dynamic from 'next/dynamic'

import { Reveal } from '@gregcello/revealjs-react'
import { MightBeRevealPlugin } from '@gregcello/revealjs-react/types/reveal.js'
import Slides from 'components/SongOrderEditor/Slides'
import { SongOrderWithGroupsFragment } from 'generated/apollo-components'

import RevealJsStyles from 'components/SongOrderEditor/RevealJsStyles'

type FullScreenPresentationProps = {
  songOrder?: SongOrderWithGroupsFragment | null
}

export default function FullScreenPresentation({ songOrder }: FullScreenPresentationProps) {
  useEffect(() => {
    return () => {
      document.getElementsByTagName('html')?.[0].classList.remove('reveal-full-page')
      document.getElementsByTagName('body')?.[0].classList.remove('reveal-viewport')
    }
  }, [])

  return (
    <>
      <RevealJsStyles theme={songOrder?.presentationConfig?.theme || 'black'} print={false} />
      <RevealJS
        {...songOrder?.presentationConfig}
        embedded={false}
        center={true}
        autoSlide={0}
        onDeckReady={(deck: Reveal<MightBeRevealPlugin[]>) => {
          // timeout shouldn't be used
          setTimeout(() => deck.layout(), 200)
        }}
      >
        <Slides songOrder={songOrder} />
      </RevealJS>
    </>
  )
}
