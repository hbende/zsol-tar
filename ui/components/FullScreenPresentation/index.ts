import dynamic from 'next/dynamic'
export default dynamic(() => import('./FullScreenPresentation'), {
  ssr: false
})
