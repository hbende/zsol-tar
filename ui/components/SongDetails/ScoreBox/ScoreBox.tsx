import React from 'react'
import { TiNotes } from 'react-icons/ti'

import UserIcon from 'components/UserIcon'
import { ScoreDetailsFragment } from 'generated/apollo-components'

type ScoreBoxProps = {
  score: ScoreDetailsFragment
}

export default function ScoreBox({ score }: ScoreBoxProps) {
  return (
    <div className="group w-56 h-32 bg-indigo-200 flex flex-col justify-between border-indigo-500 border-2 rounded-xl transition hover:border-indigo-800 ">
      <a
        target="_blank"
        href={score.url}
        className="h-2/3 flex justify-center items-center"
        rel="noreferrer"
      >
        <TiNotes className="w-1/2 h-1/2 text-indigo-600 group-hover:text-indigo-800 transition group-hover:w-3/4 group-hover:h-3/4" />
      </a>
      <div className="text-indigo-900 h-1/3 bg-indigo-300 rounded-b-xl flex justify-start items-center p-1 truncate w-full">
        <UserIcon small user={score.user} />
        {score.description ? ` - ${score.description}` : ''}
      </div>
    </div>
  )
}
