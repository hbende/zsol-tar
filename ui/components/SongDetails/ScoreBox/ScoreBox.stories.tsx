import { Meta } from '@storybook/react'
import { mockScore } from 'components/SongDetails/mocks'

import Component from './ScoreBox'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongDetails/ScoreBox'
}
export default meta

const template = storyTemplate(Component)

export const ScoreBox = template({ score: mockScore })
