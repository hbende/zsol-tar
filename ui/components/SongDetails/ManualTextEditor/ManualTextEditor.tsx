import React from 'react'
import { useForm } from 'react-hook-form'

import { LocalSlideGroup } from 'components/SongOrderEditor/PresentationEditor/PresentationEditor'
import Button from 'components/ui/Button'
import HelperText from 'components/ui/HelperText'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import TextArea from 'components/ui/TextArea'

type ManualTextEditorProps = {
  onSubmit: (data: ManualSlideGroupFormData) => void
  slideGroup: LocalSlideGroup
}

type ManualSlideGroupFormData = {
  text: string
  title?: string
}

export default function ManualTextEditor({ onSubmit, slideGroup }: ManualTextEditorProps) {
  const { handleSubmit, register } = useForm<ManualSlideGroupFormData>({
    defaultValues: {
      text: slideGroup.slides.map((slide) => slide.content).join('\r\n\r\n'),
      title: slideGroup.title || ''
    }
  })

  const submit = handleSubmit(async (data) => {
    onSubmit(data)
  })

  return (
    <form onSubmit={submit}>
      <div>
        <Label>Cím</Label>
        <Input type="text" {...register('title', { required: true })} />
      </div>
      <div>
        <Label>Szöveg</Label>
        <TextArea className="min-h-[14rem]" {...register('text', { required: true })} />
        <HelperText>
          Két sortörés segítségével lehetséges több diára felbontani a szöveget
        </HelperText>
      </div>
      <Button type="submit" color="indigo">
        Mentés
      </Button>
    </form>
  )
}
