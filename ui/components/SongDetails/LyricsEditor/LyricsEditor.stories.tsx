import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'
import { VerseType } from 'generated/apollo-components'

import Component from './LyricsEditor'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongDetails/LyricsEditor'
}
export default meta

const template = storyTemplate(Component)

export const Empty = template({
  onSubmit: action('onSubmit')
})

export const Populated = template({
  lyrics: {
    creator: { id: 'userId', email: 'teszt@teszt.teszt', name: 'felhasználó' },
    id: 'lyricsId',
    verses: [
      { id: 'verse1', text: 'The first verse', order: 0, type: VerseType.Verse },
      { id: 'refrain', text: 'This is the refrain', type: VerseType.Refrain },
      { id: 'verse2', text: 'The second verse', order: 1, type: VerseType.Verse }
    ]
  },
  onSubmit: action('onSubmit')
})
