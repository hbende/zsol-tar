import React from 'react'
import { Controller, NestedValue, useFieldArray, useForm } from 'react-hook-form'
import { TiDelete, TiPlus } from 'react-icons/ti'

import Button from 'components/ui/Button'
import HelperText from 'components/ui/HelperText'
import Input from 'components/ui/Input'
import Select from 'components/ui/Select'
import TextArea from 'components/ui/TextArea'
import Tooltip from 'components/ui/Tooltip'
import { LyricsFragment, VerseFragment, VerseType } from 'generated/apollo-components'
import capitalizeFirstLetter from 'util/capitalize'
import { PartialBy } from 'util/utilTypes'

type LyricsEditorProps = {
  lyrics?: LyricsFragment
  onSubmit?: (verses: PartialBy<VerseFragment, 'id'>[]) => void
}

export type VerseTypeHungarianLabel = 'bridge' | 'chorus' | 'refrén' | 'versszak'

export const verseTypeLabels: Record<VerseType, VerseTypeHungarianLabel> = {
  Bridge: 'bridge',
  Chorus: 'chorus',
  Refrain: 'refrén',
  Verse: 'versszak'
}

const verseTypes: VerseType[] = [
  VerseType.Verse,
  VerseType.Refrain,
  VerseType.Chorus,
  VerseType.Bridge
]

type LocalVerseType = {
  value: VerseType
  label: string
}

type LocalVerse = PartialBy<Omit<VerseFragment, 'type'>, 'id'> & { type?: LocalVerseType }

export function getReferenceLabel(type?: LocalVerseType, order?: number | null): string {
  return `${(type?.label || '?').substr(0, 1)}${typeof order === 'number' ? order : ''}`
}

function setOrderOnVerses(verses: LocalVerse[]): LocalVerse[] {
  const countsAndVerses = verses.reduce<{
    counts: Record<VerseType, number>
    verses: LocalVerse[]
  }>(
    (acc, curr) => {
      curr.order = curr.type ? acc.counts[curr.type.value] : null
      acc.verses.push(curr)
      if (curr.type) {
        acc.counts[curr.type.value]++
      }
      return { ...acc }
    },
    {
      counts: {
        Bridge: 1,
        Chorus: 1,
        Refrain: 1,
        Verse: 1
      },
      verses: []
    }
  )

  return countsAndVerses.verses
}

export default function LyricsEditor({ lyrics, onSubmit = () => {} }: LyricsEditorProps) {
  const { control, handleSubmit, register, setValue, getValues } = useForm<{
    verses: NestedValue<LocalVerse[]>
  }>({
    defaultValues: {
      verses: lyrics
        ? setOrderOnVerses(
            lyrics.verses.map((verse) => ({
              ...verse,
              type: verse.type
                ? { value: verse.type, label: capitalizeFirstLetter(verseTypeLabels[verse.type]) }
                : undefined
            }))
          )
        : [{ text: '' }]
    }
  })
  const { fields, append, remove } = useFieldArray({
    control,
    name: `verses`
  })

  const onFormSubmit = handleSubmit((data) =>
    onSubmit(data.verses.map((verse) => ({ ...verse, type: verse.type?.value })))
  )

  function handleUpdateOrdersOfVerses() {
    const values = getValues()
    setValue('verses', setOrderOnVerses(values.verses))
  }

  return (
    <form onSubmit={onFormSubmit}>
      <HelperText className="max-w-xl mb-2">
        Válassz egy típust a verszakokhoz, majd gépeld be az általad ismert szöveget. A későbbiekben
        a szöveg mellett megjelenő módon tudsz hivatkozni rá.
      </HelperText>
      <HelperText className="max-w-xl mb-2">
        Egy versszakot elég egyszer feltöltened (pl. egy ismétlődő refrént csak egyszer gépelj be).
        A feltöltött részek sorrendjét a veítés készítésekor választhatod ki.
      </HelperText>
      {fields.map((verse, index) => {
        return (
          <div key={verse.id} className="flex items-center">
            <div className="w-2/3">
              <Controller
                name={`verses.${index}.type`}
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    required
                    isSearchable={false}
                    options={verseTypes.map((type) => ({
                      label: capitalizeFirstLetter(verseTypeLabels[type]),
                      value: type
                    }))}
                    onChange={(newValue) => {
                      field.onChange(newValue)
                      handleUpdateOrdersOfVerses()
                    }}
                    className="w-full"
                  />
                )}
              />
              <TextArea {...register(`verses.${index}.text`)} className="" />
            </div>
            <div className="w-1/3 h-full">
              <div className="pl-2 flex items-center h-full space-x-2">
                <Input
                  type="text"
                  disabled
                  value={getReferenceLabel(verse.type, verse.order)}
                  className="w-12"
                />
                <Tooltip label="Törlés">
                  <Button
                    color="indigo"
                    type="button"
                    size="small"
                    onClick={() => {
                      remove(index)
                      handleUpdateOrdersOfVerses()
                    }}
                  >
                    <TiDelete />
                  </Button>
                </Tooltip>
              </div>
            </div>
          </div>
        )
      })}
      <div className="space-y-2">
        <div className="w-2/3 flex justify-center">
          <Tooltip label="Hozzáadás">
            <Button
              color="indigo"
              type="button"
              className="flex items-center"
              onClick={() => {
                append([
                  {
                    text: ''
                  }
                ])
              }}
            >
              <TiPlus />
            </Button>
          </Tooltip>
        </div>
        <div>
          <Button color="indigo" type="submit">
            Mentés
          </Button>
        </div>
      </div>
    </form>
  )
}
