import { VerseType } from 'generated/apollo-components'

import { getReferenceLabel } from './LyricsEditor'

describe('getReferenceLabel', () => {
  it('should get correct reference for the first verse', () => {
    expect(getReferenceLabel({ value: VerseType.Verse, label: 'Verse' }, 0)).toBe('V0')
  })

  it('should increment the order in the referencelabel by one', () => {
    expect(getReferenceLabel({ value: VerseType.Bridge, label: 'Bridge' }, 123)).toBe('B123')
  })

  it('should get correct reference for a chorus verse', () => {
    expect(getReferenceLabel({ value: VerseType.Chorus, label: 'Chorus' }, 2)).toBe('C2')
  })
})
