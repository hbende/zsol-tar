import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'
import { mockManyTags, mockTags } from 'components/SongDetails/mocks'

import Component from './SongTags'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongDetails/Tags'
}
export default meta

const template = storyTemplate(Component)

export const Tags = template({ tags: mockTags, onDeleteTag: action('delete') })
export const ManyTags = template({
  tags: mockManyTags,
  onDeleteTag: action('delete')
})
