import { mockUser } from 'components/Profile/mocks'
import {
  LyricsFragment,
  RecordingDetailsFragment,
  RecordingType,
  ScoreDetailsFragment,
  SongDetailsFragment,
  TagFragment,
  VerseType
} from 'generated/apollo-components'

export const mockSong: SongDetailsFragment = {
  id: 'cktnht6f3000773h5x9k9rp9x',
  name: 'Teszt ének',
  createdAt: new Date('2021-09-16T22:16:46.527Z'),
  updatedAt: new Date('2021-09-16T22:16:46.527Z'),
  writer: 'Nagy Béla',
  translator: null,
  creator: {
    ...mockUser
  },
  userTags: [],
  comments: [],
  lyrics: [
    {
      id: 'cktnht6f3000873h59cb5cr1c',
      creator: {
        ...mockUser
      },
      verses: []
    }
  ],
  recordings: [],
  scores: []
}

export const mockTags: TagFragment[] = [
  {
    id: '1',
    text: 'Tag#1'
  },
  {
    id: '2',
    text: 'Tag#2'
  }
]
export const mockManyTags: TagFragment[] = new Array(20)
  .fill(null)
  .map((_, i) => ({ id: i.toString(), text: `Tag#${i}` }))

export const mockScore: ScoreDetailsFragment = {
  id: '1',
  url: 'http://facebook.com',
  user: { email: 'teszt@teszt.teszt', id: '1', name: 'Nagy Dezső' },
  description: 'Hegedűre',
  createdAt: new Date(),
  song: {
    id: '2',
    name: 'Valami ének'
  }
}

export const mockRecording: RecordingDetailsFragment = {
  id: '1',
  url: 'http://facebook.com',
  user: { email: 'teszt@teszt.teszt', id: '1', name: 'Nagy Dezső' },
  description: 'Hegedű - legjobb',
  type: RecordingType.Raw,
  createdAt: new Date(),
  song: {
    id: '2',
    name: 'Valami ének'
  }
}

export const mockTextPreview: LyricsFragment = {
  id: 'cktnht6f3000873h59cb5cr1c',
  creator: {
    id: 'cktnht6el000073h5273opdvb',
    name: 'Admin',
    email: 'admin@zsoltar.hu'
  },
  verses: [
    {
      id: 'cktnht6f3000973h5wv18ouq0',
      order: 0,
      type: VerseType.Verse,
      text: 'Mária, Hozzád száll imánk! Tiszta virága az égnek!\n            Mária, gyönyörű liliomszál! Szűzanyánk, kérünk Téged!'
    },
    {
      id: 'cktnht6f3001073h56mhx3zqn',
      order: null,
      type: VerseType.Refrain,
      text: 'Könyörögj értünk Jézusnál! Könyörögj értünk, Szűzanyánk!\n                Követni vágyunk Szent Fiad! Vezess az úton tovább!'
    },
    {
      id: 'cktnht6f3001173h55snh0tz5',
      order: 1,
      type: VerseType.Verse,
      text: 'Mária, ragyogó ékes ág! Egyetlen igen a földön.\n                Egyedül tiszta és egyedül jó, Istenhez egyedül méltó.'
    }
  ]
}
