import React from 'react'
import { useForm } from 'react-hook-form'
import { TiPlus } from 'react-icons/ti'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import Tooltip from 'components/ui/Tooltip'
import { noop } from 'lodash'

type NewTagProps = {
  onAdd?: (text: string) => void
  className?: string
}

export default function NewTag({ className, onAdd = noop }: NewTagProps) {
  const { register, handleSubmit, reset } = useForm<{ text: string }>({
    defaultValues: { text: '' }
  })

  const onSubmit = handleSubmit((data) => {
    onAdd(data.text)
    reset()
  })
  return (
    <form
      className={clsx('flex space-x-2 items-center relative', className)}
      onSubmit={(e) => {
        e.preventDefault()
        onSubmit(e)
      }}
    >
      <Label className="sr-only">Cimke</Label>
      <Input
        type="text"
        placeholder="Gépeld be a cimke nevét"
        {...register('text')}
        className="h-8 w-52 inline-block"
      />
      <Tooltip label="Hozzáadás">
        <Button color="indigo" className="inline-block" type="submit">
          <span className="sr-only">Hozzáadás</span>
          <TiPlus />
        </Button>
      </Tooltip>
    </form>
  )
}
