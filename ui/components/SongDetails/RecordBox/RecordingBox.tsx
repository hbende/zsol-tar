import React from 'react'
import { TiMediaPlay } from 'react-icons/ti'

import UserIcon from 'components/UserIcon'
import { RecordingDetailsFragment } from 'generated/apollo-components'

type RecordingBoxProps = {
  recording: RecordingDetailsFragment
}

// todo think about different types of recordings
export default function RecordingBox({ recording }: RecordingBoxProps) {
  return (
    <div className="group w-56 h-32 bg-indigo-200 flex flex-col justify-between border-indigo-500 border-2 rounded-xl transition hover:border-indigo-800 ">
      <a
        target="_blank"
        href={recording.url}
        className="h-2/3 flex justify-center items-center"
        rel="noreferrer"
      >
        <TiMediaPlay className="w-1/2 h-1/2 text-indigo-600 group-hover:text-indigo-800 transition group-hover:w-3/4 group-hover:h-3/4" />
      </a>
      <div className="text-indigo-900 h-1/3 bg-indigo-300 rounded-b-xl flex justify-start items-center p-1 truncate w-full">
        <UserIcon small user={recording.user} />
        {recording.description ? ` - ${recording.description}` : ''}
      </div>
    </div>
  )
}
