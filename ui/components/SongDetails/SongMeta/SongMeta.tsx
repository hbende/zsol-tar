import React from 'react'

import { SongDetailsFragment } from 'generated/apollo-components'

type SongMetaProps = {
  song: Pick<SongDetailsFragment, 'writer' | 'translator' | 'name'>
}

export default function SongMeta({ song }: SongMetaProps) {
  return (
    <div>
      <h1 className="text-2xl font-bold">{song.name}</h1>
      <ul>
        <li>Szerző: {song.writer || 'Ismeretlen'}</li>
        <li>Fordítás: {song.translator || 'Ismeretlen'}</li>
      </ul>
      {/* todo meta fields */}
    </div>
  )
}
