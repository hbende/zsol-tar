import { Meta } from '@storybook/react'
import { mockSong } from 'components/SongDetails/mocks'

import Component from './SongMeta'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'SongDetails/Meta'
}
export default meta

const template = storyTemplate(Component)

export const MetaData = template({ song: mockSong })
