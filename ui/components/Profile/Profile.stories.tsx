import { action } from '@storybook/addon-actions'
import { Meta } from '@storybook/react'

import { mockUser } from './mocks'
import Component from './Profile'
import { storyTemplate } from '.storybook/storyTemplate'

const meta: Meta = {
  title: 'Profile'
}
export default meta

const template = storyTemplate(Component)

export const Profile = template({
  user: mockUser,
  onSubmit: action('submit')
})
