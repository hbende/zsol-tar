import { UserFragment } from 'generated/apollo-components'
export const mockUser: UserFragment = {
  email: 'test@email.com',
  id: '2',
  name: 'a',
  profileImage: 'https://picsum.photos/200'
}
