import React from 'react'
import { Controller, useForm } from 'react-hook-form'

import clsx from 'clsx'
import FileDropper from 'components/FileDropper'
import Button from 'components/ui/Button'
import FormError from 'components/ui/FormError'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import UserIcon from 'components/UserIcon'
import { UserFragment } from 'generated/apollo-components'
import { noop } from 'lodash'
import { MINIMUM_PASSWORD_LENGTH } from 'util/constants'

type ProfileProps = {
  user: UserFragment
  className?: string
  onSubmit?: (user: FormData) => void
}

type FormData = {
  password?: string
  passwordAgain?: string
  userName: string
  profileImage: string | File | null
}

export default function Profile({ user, className, onSubmit = noop }: ProfileProps) {
  const {
    handleSubmit,
    register,
    getValues,
    control,
    formState: { errors }
  } = useForm<FormData>({
    defaultValues: {
      password: undefined,
      passwordAgain: undefined,
      profileImage: user.profileImage,
      userName: user.name || ''
    }
  })

  const onSubmitCallback = handleSubmit(async (data) => {
    if (!data.password) {
      data.password = undefined
    }
    onSubmit(data)
  })

  return (
    <form onSubmit={onSubmitCallback} className={clsx('space-y-4', className)}>
      <div className="flex justify-between">
        <div>
          <Label>Felhasználónév</Label>
          <Input className="w-80" type="text" {...register('userName')} required />
          <FormError>
            {errors.userName?.type === 'required' && 'Kötelező a felhasználónév'}
          </FormError>
        </div>
        <div className="space-y-2">
          <div>
            <Label>Jelszó</Label>
            <Input
              className="w-80"
              type="password"
              {...register('password')}
              minLength={MINIMUM_PASSWORD_LENGTH}
            />
            <FormError>
              {errors.password?.type === 'minLength' &&
                `Legalább ${MINIMUM_PASSWORD_LENGTH} karakterből kell, hogy álljon a jelszavad`}
            </FormError>
          </div>
          <div>
            <Label>Jelszó ismét</Label>
            <Input
              className="w-80"
              type="password"
              {...register('passwordAgain', {
                validate: (value) => value === getValues().password
              })}
            />
            <FormError>
              {errors.passwordAgain?.type === 'validate' && 'A két jelszó mező nem egyezik'}
            </FormError>
          </div>
        </div>
      </div>
      <div>
        <Label>Profilkép</Label>
        <Controller
          render={({ field: { onChange } }) => (
            <FileDropper
              multiple={false}
              onChange={(e) => onChange(e.target.files[0])}
              accept="image/*"
            />
          )}
          name="profileImage"
          control={control}
        />
        <UserIcon
          className="mt-2"
          user={{
            email: user.email,
            id: user.id,
            name: getValues().userName,
            profileImage:
              typeof getValues().profileImage === 'string'
                ? (getValues().profileImage as string)
                : undefined
          }}
        />
      </div>
      <div className="flex justify-center items-center">
        <Button color="indigo">Mentés</Button>
      </div>
    </form>
  )
}
