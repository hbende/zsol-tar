import React from 'react'
import { useForm } from 'react-hook-form'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import HelperText from 'components/ui/HelperText'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import { TemplateFragment } from 'generated/apollo-components'
import { noop } from 'lodash'

type FormData = {
  title: string
}

type TemplateFormProps = {
  className?: string
  onSubmit?: (song: FormData) => void
  template?: TemplateFragment | null
}

export default function TemplateForm({ className, onSubmit = noop, template }: TemplateFormProps) {
  const { register, handleSubmit } = useForm<FormData>({
    defaultValues: { title: template?.title }
  })

  const onSubmitCallback = handleSubmit(async (data) => {
    onSubmit(data)
  })

  return (
    <form className={clsx('space-y-2', className)} onSubmit={onSubmitCallback}>
      <h2 className="text-2xl font-bold">Sablon szerkesztése</h2>
      <div className="p-2">
        <Label>Név</Label>
        <Input type="text" {...register('title')} required />
        <HelperText>
          A későbbiekben ugyan ezekkel a címekkel tudsz egy új vetítést létrehozni.
        </HelperText>
      </div>
      <div className="flex justify-center">
        <Button color="indigo" type="submit">
          Mentés
        </Button>
      </div>
    </form>
  )
}
