import React, { ReactElement } from 'react'
import { useForm } from 'react-hook-form'

import clsx from 'clsx'
import Button from 'components/ui/Button'
import HelperText from 'components/ui/HelperText'
import TextArea from 'components/ui/TextArea'
import { noop } from 'lodash'

interface AddCommentProps {
  className?: string
  onSubmit?: (text: string) => void
}

export default function AddComment({ className, onSubmit = noop }: AddCommentProps): ReactElement {
  const { register, handleSubmit, reset } = useForm<{ text: string }>({
    defaultValues: { text: '' }
  })

  const onSubmitCallback = handleSubmit((data) => {
    onSubmit(data.text)
    reset()
  })

  return (
    <form className={clsx('max-w-md space-y-2 p-2', className)} onSubmit={onSubmitCallback}>
      <TextArea {...register('text')} className="min-h-[3rem]" placeholder="Írd le a véleményed" />

      <div className="flex space-x-2">
        <Button color="indigo" type="submit">
          Hozzászólás
        </Button>
        <HelperText>
          A hozzászólásod csak saját magad részére lesz látható, amíg jóváhagyásra nem kerül
        </HelperText>
      </div>
    </form>
  )
}
