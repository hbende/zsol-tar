import React from 'react'
import { TiUser } from 'react-icons/ti'
import Image from 'next/image'

import clsx from 'clsx'
import { UserFragment } from 'generated/apollo-components'

type UserIconProps = {
  className?: string
  user: UserFragment
  small?: boolean
}

export default function UserIcon({ className, user, small }: UserIconProps) {
  return (
    <div className={clsx('flex items-center space-x-2', className)}>
      <div
        className={clsx(
          'w-10 h-10 rounded-full relative border border-gold-300',
          small && 'w-6 h-6'
        )}
      >
        {user.profileImage ? (
          <Image
            src={user.profileImage}
            alt=""
            layout="fill"
            className="w-full h-full block rounded-full"
          />
        ) : (
          <TiUser className="w-full h-full rounded-full text-gold-500" />
        )}
      </div>
      <div className="flex items-center">
        <p className={clsx('text-lg', small && 'text-base')}>{user.name}</p>
      </div>
    </div>
  )
}
