import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useRouter } from 'next/router'

import clsx from 'clsx'
import FileDropper from 'components/FileDropper'
import Button from 'components/ui/Button'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import { SongDocument, useUploadScoreMutation } from 'generated/apollo-components'
import { noop } from 'lodash'

type ScoreUploadProps = {
  onSucces?: () => void
  className?: string
}

export default function ScoreUpload({ onSucces = noop, className }: ScoreUploadProps) {
  const { register, handleSubmit, control, watch } =
    useForm<{ scoreFile?: File; scoreUrl?: string; description?: string }>()

  const router = useRouter()

  const [uploadScoreMutation] = useUploadScoreMutation({
    refetchQueries: [{ query: SongDocument, variables: { where: { id: router.query.songId } } }],
    onCompleted: onSucces
  })

  const onSubmit = handleSubmit(async (data) => {
    if (!router.query.songId) {
      return
    }

    uploadScoreMutation({
      variables: {
        data: {
          song: { connect: { id: router.query.songId as string } },
          url: data.scoreFile || data.scoreUrl,
          description: data.description
        }
      }
    })
  })

  return (
    <form onSubmit={onSubmit} className={clsx('space-y-4', className)}>
      <h2 className="text-xl">Kotta feltöltés</h2>
      <div className="flex justify-evenly items-center">
        <div className="w-5/12">
          <Controller
            render={({ field: { onChange } }) => (
              <FileDropper
                disabled={!!watch().scoreUrl}
                multiple={false}
                onChange={(e) => onChange(e.target.files[0])}
                accept=".pdf,.doc,image/*"
              />
            )}
            name="scoreFile"
            control={control}
          />
        </div>

        <div className="w-5/12">
          <Label>Hivatkozás</Label>
          <Input
            type="url"
            {...register('scoreUrl')}
            disabled={!!watch().scoreFile}
            placeholder="Illessz be egy hivatkozást"
          />
        </div>
      </div>
      <div>
        <Label className="block">Leírás</Label>
        <Input className="block" type="text" {...register('description', { required: false })} />
      </div>
      <div className="flex justify-center">
        <Button className="block" type="submit" color="indigo">
          Mentés
        </Button>
      </div>
    </form>
  )
}
