import React from 'react'
import { useRouter } from 'next/router'

import clsx from 'clsx'
import CommonHead from 'components/CommonHead'
import Header from 'components/Header'
import { logoutUser } from 'util/auth'

const routesWithoutHeader = ['/presentation/[songOrderId]']

export default function Layout({
  children,
  loggedIn,
  isAdmin
}: {
  children: React.ReactNode
  loggedIn: boolean
  isAdmin?: boolean
}) {
  const router = useRouter()

  const shouldHideHeader = routesWithoutHeader.includes(router.pathname)
  return (
    <div className="flex flex-col h-full overflow-x-hidden">
      <CommonHead />
      {!shouldHideHeader && (
        <Header onLogout={logoutUser} loggedIn={loggedIn} isAdmin={isAdmin} className="shrink-0" />
      )}
      <div className={clsx('flex-1', !shouldHideHeader && 'pt-4 pb-4')}>{children}</div>
    </div>
  )
}
