import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useRouter } from 'next/router'

import clsx from 'clsx'
import FileDropper from 'components/FileDropper'
import Button from 'components/ui/Button'
import Input from 'components/ui/Input'
import Label from 'components/ui/Label'
import { SongDocument, useUploadRecordingMutation } from 'generated/apollo-components'
import { noop } from 'lodash'

type AddRecordingProps = {
  onSucces?: () => void
  className?: string
}

export default function AddRecording({ onSucces = noop, className }: AddRecordingProps) {
  const { register, handleSubmit, control, watch } =
    useForm<{ recordingFile?: File; recordingUrl?: string; description?: string }>()

  const router = useRouter()

  const [uploadRecording] = useUploadRecordingMutation({
    refetchQueries: [{ query: SongDocument, variables: { where: { id: router.query.songId } } }],
    onCompleted: onSucces
  })

  const onSubmit = handleSubmit(async (data) => {
    if (!router.query.songId) {
      return
    }

    uploadRecording({
      variables: {
        data: {
          // todo songId should be a prop instead
          song: { connect: { id: router.query.songId as string } },
          url: data.recordingFile || data.recordingUrl,
          description: data.description
        }
      }
    })
  })

  return (
    <form onSubmit={onSubmit} className={clsx('space-y-4', className)}>
      <h2 className="text-xl">Felvétel feltöltés</h2>
      <div className="flex justify-evenly items-center">
        <div className="w-5/12">
          <Controller
            render={({ field: { onChange } }) => (
              <FileDropper
                disabled={!!watch().recordingUrl}
                multiple={false}
                onChange={(e) => onChange(e.target.files[0])}
                accept="audio/*,video/*"
              />
            )}
            name="recordingFile"
            control={control}
          />
        </div>

        <div className="w-5/12">
          <Label>Hivatkozás</Label>
          <Input
            type="url"
            {...register('recordingUrl')}
            disabled={!!watch().recordingFile}
            placeholder="Illessz be egy hivatkozást"
          />
        </div>
      </div>
      <div>
        <Label>Leírás</Label>
        <Input type="text" {...register('description', { required: false })} />
      </div>
      <div className="flex justify-center">
        <Button className="block" type="submit" color="indigo">
          Mentés
        </Button>
      </div>
    </form>
  )
}
