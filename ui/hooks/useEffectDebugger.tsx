import { DependencyList, EffectCallback, useEffect } from 'react'

import usePrevious from 'hooks/usePrevious'

/**
 * Can be used to see what dependencies caused a rerun of the effect.
 *
 * Source: https://stackoverflow.com/a/59843241/8384179
 */
export default function useEffectDebugger(
  effectHook: EffectCallback,
  dependencies: DependencyList,
  dependencyNames: string[] = []
) {
  const previousDeps = usePrevious(dependencies)

  const changedDeps = dependencies.reduce((accum, dependency, index) => {
    if (dependency !== previousDeps?.[index]) {
      const keyName = dependencyNames[index] || index
      return {
        ...accum,
        [keyName]: {
          before: previousDeps?.[index],
          after: dependency
        }
      }
    }

    return accum
  }, {})

  if (Object.keys(changedDeps).length) {
    console.log(changedDeps)
  }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(effectHook, dependencies)
}
