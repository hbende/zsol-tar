import { useState } from 'react'

export default function useArrayIndex<T>(arr: T[] = []) {
  const [index, setIndex] = useState(0)
  function next() {
    if (index === arr.length - 1) {
      setIndex(0)
      return
    }
    setIndex((x) => x + 1)
  }
  function previous() {
    if (index === 0) {
      setIndex(arr.length - 1)
      return
    }
    setIndex((x) => x - 1)
  }
  return { selectedItem: arr[index] || null, next, previous, index }
}
