import { useState } from 'react'

import { PageSize } from 'components/Table'

export default function usePageSize() {
  const perPage = useState<PageSize>(10)

  return perPage
}
