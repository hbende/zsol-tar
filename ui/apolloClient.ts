import { useEffect, useState } from 'react'
import {
  GetServerSidePropsContext,
  GetServerSidePropsResult,
  GetStaticPropsResult,
  NextPageContext
} from 'next'

import {
  ApolloCache,
  ApolloClient,
  ApolloLink,
  InMemoryCache,
  NormalizedCacheObject
} from '@apollo/client'
import { onError } from '@apollo/client/link/error'
import { RetryLink } from '@apollo/client/link/retry'
import { setContext } from 'apollo-link-context'
import { createUploadLink } from 'apollo-upload-client'
import { CachePersistor, LocalStorageWrapper } from 'apollo3-cache-persist'
import merge from 'deepmerge'
import isDeepEqual from 'fast-deep-equal/react'
import usePrevious from 'hooks/usePrevious'
import { getJWT } from 'util/jwt'

// based on https://github.com/kellenmace/apollo-client-cache-rehydration-in-next-js and https://github.com/vercel/next.js/pull/29718/files

export const PERSISTOR_CACHE_KEY = '__APOLLO-CACHE_PERSIST_KEY__'
export const APOLLO_STATE_PROP_NAME = '__APOLLO_STATE__'

const SCHEMA_VERSION = '1' // Must be a string.
const SCHEMA_VERSION_KEY = 'apollo-schema-version'

const isServer = (): boolean => typeof window === 'undefined'

let apolloCache: ApolloCache<NormalizedCacheObject> | null = null

function createCache() {
  const _cache = apolloCache ?? new InMemoryCache()

  if (!apolloCache) {
    apolloCache = _cache
  }

  return _cache
}

let apolloClient: ApolloClient<NormalizedCacheObject> | null = null

const apiURL =
  process.env.NODE_ENV === 'production'
    ? process.env.NEXT_PUBLIC_API_URL
    : 'http://localhost:3001/gql'

function createApolloClient(ctx?: NextPageContext | GetServerSidePropsContext) {
  const retryLink = new RetryLink({
    attempts: { max: 5 }
  })

  const httpLink = createUploadLink({
    uri: apiURL,
    headers: {
      'apollo-require-preflight': true
    }
  })

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.forEach(({ message, locations, path }) =>
        console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
      )
    }
    if (networkError) {
      console.log(`[Network error]: ${networkError}`)
    }
  })

  const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = getJWT(ctx)
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
  })

  const link = ApolloLink.from([authLink as unknown as ApolloLink, errorLink, retryLink, httpLink])

  return new ApolloClient({
    ssrMode: isServer(),
    link: link,
    cache: createCache(),
    connectToDevTools: process.env.NODE_ENV === 'development',
    defaultOptions: {
      watchQuery: { fetchPolicy: 'cache-and-network' }
    }
  })
}

function mergeCache(cache1: NormalizedCacheObject, cache2: NormalizedCacheObject) {
  return merge(cache1, cache2, {
    // Combine arrays using object equality (like in sets)
    arrayMerge: (destinationArray, sourceArray) => [
      ...sourceArray,
      ...destinationArray.filter((d) => sourceArray.every((s) => !isDeepEqual(d, s)))
    ]
  })
}

export function initializeApollo(ctx?: NextPageContext | GetServerSidePropsContext) {
  const _apolloClient = apolloClient ?? createApolloClient(ctx)

  // For SSG and SSR always create a new Apollo Client
  if (isServer()) return _apolloClient

  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient

  return _apolloClient
}

function mergeAndRestoreCache(
  client: ApolloClient<NormalizedCacheObject>,
  state?: NormalizedCacheObject
) {
  if (!state) {
    return
  }
  // Get existing cache, loaded during client side data fetching
  const existingCache = client.extract()
  // Merge the existing cache into data passed from getStaticProps/getServerSideProps
  const data = mergeCache(state, existingCache)
  // Restore the cache with the merged data
  client.cache.restore(data)
}

export function addApolloState<
  P extends
    | GetServerSidePropsResult<Record<string, unknown>>
    | GetStaticPropsResult<Record<string, unknown>>
>(
  client: ApolloClient<NormalizedCacheObject>,
  pageProps: P,
  existingCache?: NormalizedCacheObject
) {
  if (pageProps && 'props' in pageProps) {
    const props = pageProps.props

    if (existingCache) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      props[APOLLO_STATE_PROP_NAME] = mergeCache(client.cache.extract(), existingCache)
    } else {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      props[APOLLO_STATE_PROP_NAME] = client.cache.extract()
    }
  }

  return pageProps
}

let cachePersistor: CachePersistor<NormalizedCacheObject> | null = null

export function createCachePersistor() {
  const _cachePersistor =
    cachePersistor ??
    new CachePersistor({
      cache: createCache(),
      storage: new LocalStorageWrapper(window.localStorage),
      debug: process.env.NODE_ENV === 'development',
      key: PERSISTOR_CACHE_KEY
    })

  if (!cachePersistor) {
    cachePersistor = _cachePersistor
  }

  return _cachePersistor
}

export function useApollo(pageProps: Record<string, unknown>, ctx?: NextPageContext) {
  const state = pageProps[APOLLO_STATE_PROP_NAME] as NormalizedCacheObject | undefined
  const previousState = usePrevious(state)

  const [client, setClient] = useState<ApolloClient<NormalizedCacheObject>>()
  const [cachePersistor, setCachePersistor] = useState<CachePersistor<NormalizedCacheObject>>()

  useEffect(() => {
    async function initializeCache() {
      const cachePersistor = createCachePersistor()

      // Restore client side persisted data before letting the application to
      // run any queries

      const currentVersion = localStorage.getItem(SCHEMA_VERSION_KEY)

      if (currentVersion === SCHEMA_VERSION) {
        // If the current version matches the latest version,
        // we're good to go and can restore the cache.
        await cachePersistor.restore()
      } else {
        // Otherwise, we'll want to purge the outdated persisted cache
        // and mark ourselves as having updated to the latest version.
        await cachePersistor.purge()
        await localStorage.setItem(SCHEMA_VERSION_KEY, SCHEMA_VERSION)
      }

      setCachePersistor(cachePersistor)
    }

    initializeCache()
  }, [])

  useEffect(() => {
    async function initClient() {
      const client = initializeApollo(ctx)

      // already have some data from SSR
      if (state) {
        mergeAndRestoreCache(client, state)
        // Trigger persist to persist data from SSR
        cachePersistor?.persist()
      }

      setClient(client)
    }

    initClient()
  }, [])

  useEffect(() => {
    // If your page has Next.js data fetching methods that use Apollo Client, the initial state
    // gets hydrated here during page transitions
    if (client && state && previousState && !isDeepEqual(state, previousState)) {
      mergeAndRestoreCache(client, state)

      if (cachePersistor) {
        // Trigger persist to persist data from SSR
        cachePersistor.persist()
      }
    }
  }, [state, previousState, client, cachePersistor])

  return { client, cachePersistor }
}

export default apolloClient
