module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        gray: {
          50: '#f9fafb',
          100: '#f0f1f3',
          200: '#d9dbdf',
          300: '#b7bbc2',
          400: '#8f959f',
          500: '#6e7582',
          600: '#555e6e',
          700: '#3e4859',
          800: '#283242',
          900: '#131f30'
        },
        indigo: {
          50: '#f8fafb',
          100: '#e9f1fc',
          200: '#d0d9f9',
          300: '#abb4f0',
          400: '#8b8be5',
          500: '#7266dc',
          600: '#5c49cb',
          700: '#4637a8',
          800: '#302579',
          900: '#1a174a'
        },
        purple: {
          50: '#f9fafb',
          100: '#efeefb',
          200: '#e0d1f8',
          300: '#c5aaed',
          400: '#b580e1',
          500: '#9f5cd7',
          600: '#8540c5',
          700: '#6530a2',
          800: '#462273',
          900: '#281644'
        },
        gold: {
          50: '#fcfaf6',
          100: '#f9f0ca',
          200: '#f3dc94',
          300: '#dfb65f',
          400: '#c78a36',
          500: '#aa691c',
          600: '#8c4f12',
          700: '#6a3b10',
          800: '#48280d',
          900: '#2e1809'
        }
      }
    }
  },
  plugins: [require('@tailwindcss/forms')]
}
