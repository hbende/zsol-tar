// helper function so that stories are well typed, without using rarely used syntax
// source: https://github.com/storybookjs/storybook/issues/11916#issuecomment-743077612

import * as React from "react";

import { Story } from "@storybook/react";
import { StoryFnReactReturnType } from "@storybook/react/dist/ts3.9/client/preview/types";

export const storyTemplate =
  <P,>(Component: (props: P) => StoryFnReactReturnType | null) =>
  (props: P): Story<P> => {
    const ComponentStory: Story<P> = (args) => {
      // @ts-expect-error todo figure out
      return <Component {...args} />;
    };
    const story = ComponentStory.bind({});
    story.args = props;
    return story;
  };
