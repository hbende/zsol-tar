import Head from 'next/head'

export default function Home() {
  return (
    <div className="flex flex-col h-full">
      <Head>
        <title>Akkordo</title>
      </Head>

      <main className="container mx-auto flex flex-col justify-center items-center flex-1">
        <h1 className="text-indigo-800 font-bold text-4xl ">Üdv az Akkordo-n!</h1>
      </main>

      <footer className="w-full flex justify-center items-center h-24 mt-auto">
        Készíti Hajnal Bendegúz - {process.env.NEXT_PUBLIC_MAINTAINER_EMAIL}
      </footer>
    </div>
  )
}
