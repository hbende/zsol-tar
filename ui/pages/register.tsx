import React from 'react'

import clsx from 'clsx'
import RegisterForm from 'components/auth/RegisterForm'
import CommonHead from 'components/CommonHead'

export default function RegisterPage() {
  return (
    <div className={clsx('flex justify-center items-center h-screen')}>
      <CommonHead pageName="Regisztráció" />
      <RegisterForm />
    </div>
  )
}
