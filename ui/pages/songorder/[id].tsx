import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import Modal from 'components/Modal'
import SongOrderEditor from 'components/SongOrderEditor'
import { LocalSongOrderItem } from 'components/SongOrderEditor/SongOrderEditor/SongOrderEditor'
import TemplateForm from 'components/TemplateForm'
import {
  useAddTemplateMutation,
  useSongOrderQuery,
  useUpdateSongOrderMutation
} from 'generated/apollo-components'

const SongOrder = () => {
  const router = useRouter()
  const { data } = useSongOrderQuery({
    variables: { where: { id: router.query.id as string } },
    skip: !router.isReady
  })

  useEffect(() => {
    if (router.isReady && router.query.id) {
      router.replace(`/songorder/${router.query.id}`, undefined, { shallow: true })
    }
  }, [router.query['print-pdf']])

  const [updateSongOrder] = useUpdateSongOrderMutation()
  const [addTemplate] = useAddTemplateMutation({
    onCompleted: () => {
      setSaveAsTemplateModalOpen(false)
    }
  })
  const [saveAsTemplateModalOpen, setSaveAsTemplateModalOpen] = useState(false)
  const [templateItems, setTemplateItems] = useState<Pick<LocalSongOrderItem, 'title' | 'order'>[]>(
    []
  )

  return (
    <div className="h-full">
      <CommonHead pageName="Énekrend" />
      <SongOrderEditor
        initialMenu="slides"
        songOrder={data?.songOrder}
        onSaveAsTemplate={(items) => {
          setTemplateItems(items)
          setSaveAsTemplateModalOpen(true)
        }}
        onSetTheme={(theme) => {
          updateSongOrder({
            variables: {
              where: { id: router.query.id as string },
              data: { presentationConfig: { theme } }
            }
          })
        }}
        onSongOrderChange={async (items, title) => {
          if (items.length < Math.max((data?.songOrder?.groups.length || 0, 1))) {
            const songsToDelete =
              data?.songOrder?.groups.slice(
                data?.songOrder?.groups.length - Math.max(items.length, 1)
              ) || []
            await updateSongOrder({
              variables: {
                where: { id: router.query.id as string },
                data: {
                  groups: {
                    delete: songsToDelete.map((song) => ({ id: song.id }))
                  }
                }
              }
            })
          }
          updateSongOrder({
            variables: {
              where: { id: router.query.id as string },
              data: {
                title: { set: title },
                groups: {
                  upsert: items.map((item, i) => ({
                    where: { id: item.id || 'undefined' },
                    create: {
                      order: i,
                      song: item.song?.id ? { connect: { id: item.song?.id } } : {},
                      title: item.title,
                      isSongOrderItem: item.isSongOrderItem
                    },
                    update: {
                      order: { set: i },
                      song: item.song?.id ? { connect: { id: item.song?.id } } : {},
                      title: { set: item.title },
                      isSongOrderItem: { set: item.isSongOrderItem }
                    }
                  }))
                }
              }
            }
          })
        }}
      />
      <Modal isOpen={saveAsTemplateModalOpen} onClose={() => setSaveAsTemplateModalOpen(false)}>
        <TemplateForm
          onSubmit={({ title }) => {
            addTemplate({
              variables: { data: { items: { createMany: { data: templateItems } }, title } }
            })
          }}
        />
      </Modal>
    </div>
  )
}

export default SongOrder
