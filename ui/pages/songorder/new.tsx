import { useState } from 'react'
import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import Modal from 'components/Modal'
import SongOrderEditor from 'components/SongOrderEditor'
import { LocalSongOrderItem } from 'components/SongOrderEditor/SongOrderEditor/SongOrderEditor'
import TemplateForm from 'components/TemplateForm'
import { useAddSongOrderMutation, useAddTemplateMutation } from 'generated/apollo-components'

const NewSongOrder = () => {
  const router = useRouter()
  const [addSongOrder] = useAddSongOrderMutation({
    onCompleted: (data) => {
      router.push(`/songorder/${data.createOneSongOrder.id}`)
    }
  })
  const [saveAsTemplateModalOpen, setSaveAsTemplateModalOpen] = useState(false)
  const [templateItems, setTemplateItems] = useState<Pick<LocalSongOrderItem, 'title' | 'order'>[]>(
    []
  )
  const [addTemplate] = useAddTemplateMutation({
    onCompleted: () => {
      setSaveAsTemplateModalOpen(false)
    }
  })
  return (
    <div className="h-full">
      <CommonHead pageName="Új énekrend" />
      <SongOrderEditor
        onSongOrderChange={async (items, title) => {
          await addSongOrder({
            variables: {
              data: {
                title,
                groups: {
                  createMany: {
                    data: items.map((item) => ({
                      order: item.order,
                      songId: item.song?.id,
                      title: item.title,
                      isSongOrderItem: item.isSongOrderItem
                    }))
                  }
                }
              }
            }
          })
        }}
        onSaveAsTemplate={(items) => {
          setTemplateItems(items)
          setSaveAsTemplateModalOpen(true)
        }}
      />
      <Modal isOpen={saveAsTemplateModalOpen} onClose={() => setSaveAsTemplateModalOpen(false)}>
        <TemplateForm
          onSubmit={({ title }) => {
            addTemplate({
              variables: { data: { items: { createMany: { data: templateItems } }, title } }
            })
          }}
        />
      </Modal>
    </div>
  )
}

export default NewSongOrder
