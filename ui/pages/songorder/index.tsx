import Link from 'next/link'

import CommonHead from 'components/CommonHead'
import SongOrderTable from 'components/Table/SongOrderTable'
import Button from 'components/ui/Button'

const SongOrders = () => {
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Énekrendek" />
      <SongOrderTable />
      <Link href="/songorder/new" passHref={false}>
        <Button color="gold" className="mt-4">
          Új énekrend
        </Button>
      </Link>
    </div>
  )
}

export default SongOrders
