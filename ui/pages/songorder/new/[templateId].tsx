import { useState } from 'react'
import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import Modal from 'components/Modal'
import SongOrderEditor from 'components/SongOrderEditor'
import { LocalSongOrderItem } from 'components/SongOrderEditor/SongOrderEditor/SongOrderEditor'
import TemplateForm from 'components/TemplateForm'
import {
  useAddSongOrderMutation,
  useAddTemplateMutation,
  useTemplateQuery
} from 'generated/apollo-components'

const NewSongOrderFromTemplate = () => {
  const router = useRouter()
  const { data: templateData, loading: templateLoading } = useTemplateQuery({
    variables: { where: { id: router.query.templateId as string } },
    skip: !router.isReady
  })
  const [addSongOrder] = useAddSongOrderMutation({
    onCompleted: (data) => {
      setSaveAsTemplateModalOpen(false)
      router.push(`/songorder/${data.createOneSongOrder.id}`)
    }
  })
  const [saveAsTemplateModalOpen, setSaveAsTemplateModalOpen] = useState(false)
  const [templateItems, setTemplateItems] = useState<Pick<LocalSongOrderItem, 'title' | 'order'>[]>(
    []
  )
  const [addTemplate] = useAddTemplateMutation({
    onCompleted: () => {
      setSaveAsTemplateModalOpen(false)
    }
  })

  if (!templateData?.songOrderTemplate || templateLoading) {
    return null
  }

  return (
    <div className="h-full">
      <CommonHead pageName="Új énekrend" />

      <SongOrderEditor
        songOrder={{
          id: undefined as unknown as string,
          presentationConfig: {},
          groups: templateData.songOrderTemplate.items.map((item, i) => ({
            title: item.title,
            order: i + 1,
            slides: [],
            id: undefined as unknown as string,
            isSongOrderItem: true
          }))
        }}
        onSongOrderChange={async (items, title) => {
          await addSongOrder({
            variables: {
              data: {
                title,
                groups: {
                  createMany: {
                    data: items.map((item) => ({
                      order: item.order,
                      song: item.song?.id ? { connect: { id: item.song.id } } : undefined,
                      title: item.title,
                      isSongOrderItem: item.isSongOrderItem
                    }))
                  }
                }
              }
            }
          })
        }}
        onSaveAsTemplate={(items) => {
          setTemplateItems(items)
          setSaveAsTemplateModalOpen(true)
        }}
      />
      <Modal isOpen={saveAsTemplateModalOpen} onClose={() => setSaveAsTemplateModalOpen(false)}>
        <TemplateForm
          onSubmit={({ title }) => {
            addTemplate({
              variables: { data: { items: { createMany: { data: templateItems } }, title } }
            })
          }}
        />
      </Modal>
    </div>
  )
}

export default NewSongOrderFromTemplate
