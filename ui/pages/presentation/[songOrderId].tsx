import React, { useEffect } from 'react'
import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import FullScreenPresentation from 'components/FullScreenPresentation'
import { useSongOrderQuery } from 'generated/apollo-components'

export default function Presentation() {
  const router = useRouter()
  const { data, loading, refetch } = useSongOrderQuery({
    variables: {
      where: { id: router.query.songOrderId as string }
    },
    skip: !router.query.songOrderId
  })

  useEffect(() => {
    if (!router.query.songOrderId) {
      return
    }
    refetch({ where: { id: router.query.songOrderId as string } })
  }, [refetch, router.query.songOrderId])

  if (loading || !router.query.songOrderId) {
    return null
  }

  return (
    <>
      <CommonHead pageName="Prezentáció" />
      <FullScreenPresentation songOrder={data?.songOrder} />
    </>
  )
}
