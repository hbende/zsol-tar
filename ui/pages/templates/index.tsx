import CommonHead from 'components/CommonHead'
import TemplateTable from 'components/Table/TemplateTable'

const Templates = () => {
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Sablonok" />
      <TemplateTable />
    </div>
  )
}

export default Templates
