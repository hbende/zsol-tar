import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import SongOrderTemplate from 'components/SongOrderTemplate'
import { useTemplateQuery } from 'generated/apollo-components'

const Template = () => {
  const router = useRouter()
  const { data, loading } = useTemplateQuery({
    variables: { where: { id: router.query.id as string } }
  })

  if (!data?.songOrderTemplate || loading) {
    return null
  }

  return (
    <div className="container mx-auto">
      <CommonHead pageName={data.songOrderTemplate.title} />
      <SongOrderTemplate template={data?.songOrderTemplate} />
    </div>
  )
}

export default Template
