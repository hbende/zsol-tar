import CommonHead from 'components/CommonHead'
import LyricsTable from 'components/Table/LyricsTable'

const Lyrics = () => {
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Szövegek" />
      <LyricsTable />
    </div>
  )
}

export default Lyrics
