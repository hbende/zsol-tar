import CommonHead from 'components/CommonHead'
import ProfileForm from 'components/Profile'
import { useGetUserQuery, useUpdateUserMutation } from 'generated/apollo-components'
import useUser from 'hooks/useUser'

const Profile = () => {
  const user = useUser()
  const { data, loading } = useGetUserQuery({ variables: { where: { id: user?.id } } })
  const [updateUser] = useUpdateUserMutation()

  if (!data?.user || loading) {
    return null
  }

  return (
    <div className="container mx-auto">
      <CommonHead pageName="Profil" />
      <ProfileForm
        user={data.user}
        onSubmit={(newUser) => {
          updateUser({
            variables: {
              where: { id: user?.id },
              data: {
                name: { set: newUser.userName },
                profileImage:
                  typeof newUser.profileImage === 'string' || !newUser.profileImage
                    ? undefined
                    : newUser.profileImage,
                ...(newUser.password ? { password: { set: newUser.password } } : {})
              }
            }
          })
        }}
      />
    </div>
  )
}

export default Profile
