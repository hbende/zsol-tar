import CommonHead from 'components/CommonHead'
import RecordingTable from 'components/Table/RecordingTable'

const Recordings = () => {
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Felvételek" />
      <RecordingTable />
    </div>
  )
}

export default Recordings
