import { useRouter } from 'next/router'

import { useApolloClient } from '@apollo/client'
import CommonHead from 'components/CommonHead'
import LyricsEditor from 'components/SongDetails/LyricsEditor/LyricsEditor'
import { useAddLyricsMutation, useSongQuery } from 'generated/apollo-components'

const NewLyrics = () => {
  const router = useRouter()
  const client = useApolloClient()
  const { data, loading } = useSongQuery({
    variables: { where: { id: router.query.songId as string } }
  })
  const [addLyrics] = useAddLyricsMutation({
    onCompleted: () => {
      client.refetchQueries({ include: ['song'] })
      router.back()
    }
  })
  if (loading || !data || !data.song) {
    return null
  }

  return (
    <div className="container mx-auto">
      <CommonHead pageName="Új szöveg" />
      <h2 className="text-xl text-center p-5">{data.song.name}</h2>
      <LyricsEditor
        onSubmit={(verses) =>
          addLyrics({ variables: { songId: data.song?.id as string, verses: { data: verses } } })
        }
      />
    </div>
  )
}

export default NewLyrics
