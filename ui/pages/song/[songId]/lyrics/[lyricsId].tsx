import { TiArrowLeft } from 'react-icons/ti'
import { useRouter } from 'next/router'

import CommonHead from 'components/CommonHead'
import SongTextPreview from 'components/SongDetails/SongTextPreview'
import Link from 'components/ui/Link'
import { useLyricsQuery } from 'generated/apollo-components'

const LyricsPage = () => {
  const router = useRouter()
  const { data, loading } = useLyricsQuery({
    variables: { id: router.query.lyricsId as string },
    skip: !router.isReady
  })
  if (loading || !data?.lyrics) {
    return null
  }
  return (
    <div className="container mx-auto">
      <CommonHead pageName="Szöveg" />

      <Link href={`/song/${data.lyrics.song?.id}`} className="flex items-center mb-2">
        <TiArrowLeft className="h-8 w-8 " />
        <span> Ugrás az énekhez</span>
      </Link>
      <SongTextPreview lyrics={data.lyrics} />
    </div>
  )
}

export default LyricsPage
