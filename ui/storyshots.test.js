import initStoryshots from '@storybook/addon-storyshots'
import { render } from '@testing-library/react'
import path from 'path'

initStoryshots({
  renderer: render,
  framework: 'react',
  configPath: path.join(__dirname, '.storybook')
})
