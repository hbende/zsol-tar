import { useRouter } from 'next/router'

import { deserializeToken, getJWT } from 'util/jwt'

const useAuthRedirect = () => {
  // todo redirect should be handled on server
  const router = useRouter()
  const accessToken = getJWT()
  const user = deserializeToken(accessToken)

  // If there is no access token we redirect to "/" page.
  if (typeof window !== 'undefined' && (!accessToken || !user?.isLoggedIn)) {
    return router.replace('/login')
  }
}

export default useAuthRedirect
