import { GetServerSidePropsContext, NextPageContext } from 'next'

import { decode } from 'jsonwebtoken'
import Cookies from 'universal-cookie'

export const cookieManager = new Cookies()

export function getJWT(ctx?: NextPageContext | GetServerSidePropsContext<any>): string | undefined {
  let cookies
  if (ctx && ctx.req) {
    cookies = new Cookies(ctx.req.headers.cookie)
  } else {
    cookies = new Cookies()
  }

  return cookies.get('token')
}

export function setJWT(token: string) {
  cookieManager.set('token', token, { path: '/' })
}

export function deleteJWT() {
  cookieManager.remove('token', { path: '/' })
}

export type JWTPayload = {
  id: string
  isAdmin?: boolean
  isLoggedIn?: boolean
  token?: string
}

export function deserializeToken(token: string | undefined = getJWT()): JWTPayload {
  if (!token) {
    return {
      isLoggedIn: false,
      isAdmin: false,
      id: '',
      token: ''
    }
  }

  const { id, isAdmin } = decode(token) as JWTPayload

  return {
    isLoggedIn: true,
    isAdmin,
    id,
    token
  }
}
