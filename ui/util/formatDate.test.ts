import './dayJsSetup'

import { transformDate } from './formatDate'

describe('formatDate', () => {
  it('doesnt throw an error when supplied date', () => {
    expect(() => transformDate(new Date())).not.toThrow()
  })

  it('returns the date in correct format', () => {
    expect(transformDate(new Date(2020, 5, 6, 1, 32, 34))).toBe('2020. 06. 06. 01:32')
  })
})
