import dayjs from 'dayjs'

export function transformDate(date: string | Date): string {
  return dayjs(date).format('YYYY. MM. DD. HH:mm')
}
