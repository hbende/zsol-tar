import { GetServerSideProps, GetServerSidePropsContext } from 'next'

import { deserializeToken, getJWT } from 'util/jwt'

export function withServerSideAuthRedirect<
  P extends { [key: string]: any } = { [key: string]: any }
>(gssp: GetServerSideProps<P>) {
  return async function redirectIfNoJWT(context: GetServerSidePropsContext<any>) {
    const accessToken = getJWT(context)
    const user = deserializeToken(accessToken)

    // If there is no access token we redirect to "/" page.
    if (!accessToken || !user?.isLoggedIn) {
      return {
        redirect: { destination: '/login', permanent: false }
      }
    }

    return await gssp(context) // Continue on to call `getServerSideProps` logic
  }
}
