import capitalizeFirstLetter from './capitalize'

describe('capitalize', () => {
  it('should capitalize string', () => {
    expect(capitalizeFirstLetter('myString')).toBe('MyString')
  })
})
