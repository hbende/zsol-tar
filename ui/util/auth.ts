import { GetServerSidePropsContext, NextPageContext } from 'next'

import { createCachePersistor, initializeApollo } from 'apolloClient'
import { Maybe } from 'graphql/jsutils/Maybe'
import { deleteJWT, deserializeToken, getJWT, setJWT } from 'util/jwt'

// todo this type shouldn't be used here
export const loginUser = (token?: Maybe<string>) => {
  if (token) {
    setJWT(token)
  }
}

export const logoutUser = async () => {
  deleteJWT()
  const apolloClient = initializeApollo()
  const cachePersistor = createCachePersistor()
  await cachePersistor.purge()
  await apolloClient.cache.reset()
  await apolloClient.clearStore()
}

export function getUser(ctx?: NextPageContext | GetServerSidePropsContext<any>) {
  const accessToken = getJWT(ctx)
  const user = deserializeToken(accessToken)
  return user
}

export function isUserLoggedIn(ctx?: NextPageContext | GetServerSidePropsContext<any>) {
  const user = getUser(ctx)
  return user.isLoggedIn
}
