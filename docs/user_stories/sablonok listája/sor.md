Elmentett sablonról megjelenő információk

---

Ezek az adatok jelennek meg a sablonokról a kilistázáskól

---

**Mint egy** belépett felhasználó
**Amikor** a sablonok között böngészek
**Akkor** a következő adatokat és gombokat látom: cím, létrehozás dátuma, feltöltő neve, énekrend készítése gomb

---

prioritás:
