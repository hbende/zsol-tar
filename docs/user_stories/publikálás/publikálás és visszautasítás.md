## Publikussá tétel

A publikussággal rendelkező tartalmak publikálhatóak vagy visszautasíthatóak adminisztrátorként

---

**Mint egy** belépett adminisztrátor felhasználó
**Amikor** a különböző táblázatokban böngészek rányomok a publikálás vagy visszautasítás gombokra
**Akkor** rendre vagy publikus lesz a tartalom, vagy visszautasításra kerül

---

prioritás:
