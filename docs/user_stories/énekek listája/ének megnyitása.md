Az ének részleteinek elérése

---

Az egyes énekek részletes adatait is el lehet érni

---

**Mint egy** felhasználó
**Amikor** az énekek listájában egy sorra kattintok
**Akkor** annak a részleteinek az oldalára kerülök

---

prioritás: 10