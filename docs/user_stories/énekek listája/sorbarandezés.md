Énekek sorbarendezése

---

Az énekeket sorba lehet rendezni
 
---

**Mint egy** felhasználó
**Amikor** az énekek listájában sorbarendezek a kedvencek száma szerint
**Akkor** azok az énekek kerülnek kilistázásra elől, amelyeknek a legmagasabb az értékelése

---

prioritás: 6