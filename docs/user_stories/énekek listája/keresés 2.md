Énekek keresés

---

Szűrés az énekek listájában

---

**Mint egy** felhasználó
**Amikor** az énekek listájában szűrök a cím alapján
**Akkor** azok az énekek kerülnek kilistázásra amelyek címe részben tartalmazza az általam beírt szöveget

---

prioritás: 6
