Üres eredmény lista

---

Szűrés esetén előfordulhat, hogy nincs a szűrőknek megfelelő elem
 
---

**Mint egy** felhasználó
**Amikor** az énekek listájában olyan szűrőt állítok be, amihez nincs eredmény
**Akkor** egy szöveg figyelmeztet, hogy próbáljak más szűrőket alkalmazni

---

prioritás: 