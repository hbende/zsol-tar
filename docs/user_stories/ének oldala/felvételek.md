Felvételek listája

---

Különböző felvételeket lehet egy énekhez rendelni
 
---

**Mint egy** felhasználó
**Amikor** az ének részletei oldalon a felvételek listáját nézem
**Akkor** azok közül x db jelenik meg alapértelmezetten, a sorbarendezés alapja a kedvencek száma és a frissesség. A következő felvételek lehetségesek: videó fájl, hang fájl, spotify link(?), youtube embed

---

prioritás: 

[[kották]]