Ének cimkéinek megjelenítése az ének részletei oldalon

---

A felhasználók által hozzárendelt cimkék megjelennek egy horizotálisan görgethető listában
 
---

**Mint egy** felhasználó
**Amikor** az énekhez rendelt cimkéket nézem, de azok nem férnek el egy sorban
**Akkor** a lista horizontálisan görgethetővé válik

---

prioritás: 