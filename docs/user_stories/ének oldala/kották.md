Kották elérése

---

Az ének részletei oldalon megjelennek a feltöltött és megosztott kották
 
---

**Mint egy** felhasználó
**Amikor** az ének részletei oldalon a kották listáját szeretném megnézni
**Akkor** a következő sorrendben jelennek meg: kedvencek száma, feltöltés ideje (legfrisebb elől). Alapértelmezetten x db jelenik meg, az elnevezésükkel együtt

---

prioritás: 