Egyedi dia hozzáadása

---

Lehetőség van egyedi tartalom beillesztésére a vetítésembe

---

**Mint egy** belépett felhasználó
**Amikor** a diák közötti hozzáadás gombra kattintok
**Akkor** egyedi, egyszeri szöveges tartalmat hozzáadhatok a vetítésemhez

---

prioritás:
