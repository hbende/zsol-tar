Vetítésben szereplő ének szövegezésének megválasztása

---

Lehetőség van a vetítésben szereplő ének más - a platformra feltöltött - szövegezései közül választani és felhasználni.

pl. x énekből a vetítésbe a következő versszakok kerüljenek bele: Refrén, 1. vsz, Refrén, a Nagy Dezső által feltöltött szövegezés szerint

---

**Mint egy** belépett felhasználó
**Amikor** a vetítésben szereplő ének meletti szövegezés szerkesztésre kattintok
**Akkor** egy felületen lehetőségem van az énekhez elérhető különböző szövegezések között böngészni, majd egyet kiválasztva abból a versszakokat és azok sorrendjeit kiválasztani.

---

prioritás:
