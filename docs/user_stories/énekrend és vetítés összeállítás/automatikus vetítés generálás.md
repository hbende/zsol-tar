Automatikus vetítés készítése

---

Az énekrend szerkesztése közben egy vetítés is képződik ahhoz

---

**Mint egy** belépett felhasználó
**Amikor** szerkesztem az énekrendet
**Akkor** a hozzá tartozó vetítést is látom

---

prioritás:
