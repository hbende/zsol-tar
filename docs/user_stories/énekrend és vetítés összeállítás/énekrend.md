Az énekek rendjének felvétele

---

Az énekrend énekek egy rögzített sorrendje, az egyes elemekhez cím rendelhető hozzá opcionálisan

---

**Mint egy** belépett felhasználó
**Amikor** az énekrend összeállítás felületen vagyok
**Akkor** látom kilistázva az énekrendet a címükkel és a hozzá rendelt énekekkel. Lehetőségem van a sorrendet módosítani, elemeket törölni és új elemeket hozzáadni.

---

prioritás:
