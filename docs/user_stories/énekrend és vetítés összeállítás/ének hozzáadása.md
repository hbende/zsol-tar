Ének hozzáadása az énekrendhez

---

Az énkerend összeállítás oldalán lehetőség van új dalt hozzáadni az énekrendhez (és a vetítéshez)

---

**Mint egy** belépett felhasználó
**Amikor** az énekrend szerkesztési oldalon az új ének hozzáadása gombra kattintok
**Akkor** az énekek listájából kereséssel kiválaszthatok egy éneket amit hozzzáadok az énekrendhez. Opcionálisan címet is adhatok az új elemnek.

---

prioritás:
