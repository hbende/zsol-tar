Énekrend sablon mentése

---

Lehetőség van a felvett énekrend címei és sorrendje alapján egy sablon elmentésére. pl. katolikus Szentmise részei: Bevonulás, Kyrie, Gloria stb.

---

**Mint egy** belépett felhasználó
**Amikor** az énekrend szerkesztése közben rányomok a sablon mentése gombra
**Akkor** egy felületen megadhatok egy címet a sablonnak, ezt követően pedig mentésre kerül a saját sablonjaim közé a sorrend a címekkel együtt

---

prioritás:
