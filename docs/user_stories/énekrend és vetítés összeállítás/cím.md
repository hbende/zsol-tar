Énekrendhez cím hozzáadása

---

Az énekrendehez (és vetítéshez) címet kell hozzárendelni a könyebb kereshetőség érdekében.

---

**Mint egy** belépett felhasználó
**Amikor** az énekrend összeállítása oldalon kitöltöm a vetítés elnevezése mezőt, majd a 'Mentés' gombra kattintok
**Akkor** az énekrendem címe elmentődik

---

prioritás:
