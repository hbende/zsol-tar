Vetítés exportálása

---

Lehetőség van az elkészült vetítést különböző formátumokban elmenteni a saját gépünkre

---

**Mint egy** belépett felhasználó
**Amikor** az exportálás gombra kattintok
**Akkor** lehetőségem van elmenteni az elkészült vetítést .pdf fájlként

---

prioritás:
