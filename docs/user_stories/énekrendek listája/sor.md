Énekrendről megjelenített információk

---

Az énekrendekről ezek az adatok jelennek meg a listanézetben

---

**Mint egy** belépett felhasználó
**Amikor** az énekrendjeim listájában böngészek
**Akkor** ezeket az adatokat látom: Cím, utolsó szerkesztés időpontja, vetítés gomb

---

prioritás:
