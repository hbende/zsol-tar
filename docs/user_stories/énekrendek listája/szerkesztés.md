Szerkesztés gomb

---

Lehetőség van az énekrend / vetítés szerkesztésére, ez a felület egy gomb segítségével érhető el
 
---

**Mint egy** belépett felhasználó
**Amikor** egy korábban létrehozott énekrendemen a szerkesztés gombra kattintok
**Akkor** az énekrend / vetítés szerkesztő oldalra kerülök

---

prioritás: 