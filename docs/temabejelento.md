A cél egy PWA (Progresszív Webes Alkalmazás) előállítása amely különböző énekrendek és a hozzájuk tartozó vetítés készítését majd
prezentálását segíti. A célfelhasználók különböző felekezetek zenei szolgálattevői. A felhasználók regisztráció és belépés után énekeket
tölthetnek fel. Megadhatják az ének szerzőjét, címét, az általuk ismert szövegét, annak fordítóját, feltölthetnek kottákat és felvételeket. A
dalok cimkékkel is elláthatóak a könnyebb kereshetőség érdekében. Ezeket az adatokat - felvételeket, kottákat - ha publikussá teszik,
akkor bárki elérheti őket az alkalmazáson keresztül.

Az egyes énekekhez más regisztrált felhasználó is feltölthet általa ismert kottákat, szövegeket, hangfájlokat. A felhasználók
megjegyzéseket is hagyhatnak az egyes énekek alatt.

A belépett felhasználó énekrendeket vehet föl. Egy énekrend valójában énekek egy sorrendje. Az énekrend előálíltása közben is van
lehetőség új éneket feltölteni az oldalra. Az énekrend összeállításában a felhasználó segítségére vannak a cimkék és a mások által az
aznapra előállított más énekrendek. Egy énekrendhez vetítés készíthető. Kiválaszthatja a felhasználó, hogy melyik ének melyik szövegezés
szerinti melyik versszakait szeretné betenni és azokat milyen sorrendben. További HTML tartalommal is bővíthetőek a vetítések, így akár
a felhasználó teljes mértékben szeméylre szabhatja a vetítést.

Sablonokat lehet felvenni az énekrendekhez. Erre azért van szükség, mert sorrend szempontjából az egyes hasonló eseménykre készített
diák gyakran megegyeznek.

Az offline használat segítsége érdekében a vetítések PDF és esetleg PPT formátumban is kigenerálhatóak. Illetve mivel PWA az applikáció,
a böngészőből is vezérelhető a vetítés, ha előtte az be lett töltve.