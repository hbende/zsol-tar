## next.js PWA example

https://github.com/vercel/next.js/tree/canary/examples/progressive-web-app

## presentations

https://revealjs.com/

It has multi-plex built in!
https://github.com/reveal/multiplex

PDF generation built in
https://revealjs.com/pdf-export/

ppt2 to reveal ?
https://github.com/ignatandrei/ppt2reveal/tree/master

reveal to ppt ?
https://github.com/hakimel/reveal.js/issues/1702

### tools : may have to look for some awesome things

https://github.com/hakimel/reveal.js/wiki/Plugins,-Tools-and-Hardware#tools
